\documentclass{ansarticle}

\newtheorem{corollary}{Corollary}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{theorem}{Theorem}

\title{dune-composites v1.0 - solving large-scale anisotropic elasticity problems}
\author[1]{Tim Dodwell}
\author[2]{Anne Reinarz}
\affil[1]{College of Engineering, Mathematics and Physical Sciences, University of Exeter}
\affil[2]{Department of Mathematical Sciences, University of Bath}


%------------------------------------------------------------------------------
\begin{document}

\maketitle

\begin{abstract}
We present the module \textit{dune-composites}, which provides a tool 
to efficiently solve large-scale anisotropic elasticity problems.
\end{abstract}

%------------------------------------------------------------------------------
\section{Introduction}

Composite materials constitute over 50\% of recent aircraft construction.
Thus, their simulation is of considerable interest. This is challenging due to the
nature of these materials. They are made up of thin layers of carbon-fibres (typically around $10^{-3}$ m thick),
which are held together with even thinner layers of resin material (typically around $10^{-4}$ m thick),
with the entire component part being meters long. Further, sheets of carbon fibres are very strong in the direction
of the fibres, but as weak as the resin in the other two directions. To account for this they are stacked
on top of each other at various angles, commonly these are $-45^\circ, 45^\circ, 90^\circ, 0^\circ$, referred to as
a stacking sequence. These large jumps in material properties between the layers as well as the resultant non-grid aligned
anisotropy make their simulation extremely challenging.

We have developed a new module, \textit{dune-composites}, which
solves the linear elasticity equations with general, anisotropic
stiffness tensor, applicable for modelling
 composite structures within the HPC framework DUNE (Distributed and 
 Unified Numerics Environment), see \cite{dune1,dune2a,dune2b,dune3}.  This module provides an interface to handle composite
 applications, which includes stacking sequences, complex part geometries and
 complex boundary conditions such as multi-point constraints, or periodic boundary conditions.
Further, we have implemented a new $20$-node 3D serendipity element
(with full integration) within \textit{dune-pdelab},
 which is not prone to shear locking. This element has degrees of freedom
 at the $8$ nodes of the element as well as on each of the $12$ edges.
 
The main challenge when developing the module, however, was the design of an efficient solver
for the resultant linear systems. Commerical packages often use direct solvers, which are robust
with regards to the anisotropy and heterogeneity, but have high memory demands and poor parallel scalability [{\color{green}Duf86}].
For the  very large finite element calculations needed to model entire component parts iterative solvers are the only viable option.
However, our systems are very badly conditioned.
A system of equations $\textbf{K}{\bf u} = {\bf f}$ is ill-conditioned if ${\bf u}$ is sensitive to (very) small changes in ${\bf f}$.
For such cases iterative solvers converge very slowly, worsening as the problem size increases. The remedy is to `precondition' the system,
this is to develop an operation $\textbf{P}^{-1}$ which is computationally cheap to construct (in parallel) and so that
$\textbf{P}^{-1}\textbf{K}{\bf u} = \textbf{P}^{-1}{\bf f}$ is well-conditioned and so iterative solvers converges quickly. 
A good preconditioner is application specific. Since the implementation of customised preconditioners is not available in commercial software,
their parallel efficiency is significantly limited. Consequently, there are huge untapped research opportunities by exploiting parallel 
preconditioners based on domain decomposition and multigrid methods [{\color{green}Vas08}] widely studied in other applications and by numerical theorists. 

An efficient iterative solver has been implemented alongside \textit{dune-composites} in the DUNE module \textit{dune-geneo}. This module
implements the GenEO coarse space  \cite{geneo1,geneo2} and has been shown to scale well in composite applications up to thousands of computer
cores \cite{dunecomp}.

For supercomputers with large core counts and complex memory hierarchies a good preconditioned solver is not enough. To attain peak performance, 
data must be managed efficiently through the use of cach-friendly algorithms. Increasingly researchers realise that to attain computationally tractable solutions 
for massive models high-performance computer implementations must balance the considerations of the physical model, numerical methods and computer architecture. 





%------------------------------------------------------------------------------
\section{DUNE overview and dependencies}
The module \textit{dune-composites} has been implemented within the HPC framework DUNE (Distributed and 
 Unified Numerics Environment), see \cite{dune1,dune1-2.5}.
 DUNE is a modular C++ library for the grid-based solution of partial differential equations. 
 
 The general
 structure of DUNE is shown in Figure \ref{fig-dunestruc}. The main functionality of DUNE is provided by the
 core modules. These modules provide support for different grids, e.g. structured grids such as YaspGrid and
 unstructured grids such as UGgrid \cite{dune2a,dune2b}.
 The module \textit{dune-istl} provides various linear solvers,
 including iterative solver, such as algebraic muligrid (AMG) and interfaces with 
 the direct solvers UMFPack and SuperLU \cite{dune3}.
 Further, DUNE provides two discretisation modules. We use the module \textit{dune-pdelab}, which
 provides an easy interface for implementing various 
 discretisation schemes quickly and efficiently.
 On top of these modules sits our application module \textit{dune-composites}, which provides
 support specific to composite applications.
 
\begin{figure}[tb]
\centering
 \includegraphics[scale=.6]{Images/dune.png}
 \caption{DUNE module structure.}
 \label{fig-dunestruc}
\end{figure}

\subsection{Installation}
The module \textit{dune-composites} is available at
\url{https://gitlab.dune-project.org/anne.reinarz/dune-composites}.
It requires the core DUNE modules and the discretisation module \textit{dune-pdelab} in the version 2.5. 
For parallel efficiency it also requires a sparse direct solver
such as UMFPack \cite{umfpack}, the eigensolver Arpack \cite{arpack} and the module \textit{dune-geneo}, which can
be found at \url{https://gitlab.dune-project.org/linus.seelinger/dune-geneo}. 

The module can be built using DUNEs standard cmake build system, by placing all the required DUNE modules into one directory
and running

\verb>./dune-common/bin/dunecontrol --opts=release.opts all>

Any required options can be set in the \verb=release.opts= file. For further details on installing DUNE see 
\url{https://www.dune-project.org/}

After installing the 8 problems described in Section \ref{sec-examples}
can be run. These test cases are set up in module \verb=./src= folder and can be run without changes.
For many minor changes to the tests, the config files given in the folder \verb=config/= can be used.
These allow changes, for example, to the number of elements
of structured grids, the order of the finite element space, vtk plotting and defect parameters.
Larger changes to the tests may require changing some files in the \verb=dune/composites/= folder and require
recompiling the module.


\section{Mathematical Problem}

\subsection{Anisotropic Elasticity Equations}

\begin{equation}
\sigma_{ij,j} + f_i = 0
\end{equation}

\begin{equation}\label{eq-hooke}
\sigma_{ij} = C_{ijkl}\epsilon_{kl}
\end{equation}

\begin{equation}
\epsilon_{ij} = \frac{1}{2}\left(u_{i,j} + u_{j,i}\right)
\end{equation}

Define function space for each $u_i$ 
\begin{equation}\label{eq-fctspace}
V_{h,i} := \{v \in H^1(\Omega) : v_i({\bf x}) = h_i \; {\bf x} \in \partial \Omega_i\} 
\end{equation}

Find ${\bf u} \in V_h := V_1 \otimes V_2 \otimes V_3$ such that

\begin{equation}\label{eq-fe}
\int_\Omega \sigma_{ij}\epsilon_{ij} \; dx + \int_\Omega f_iv_i\;dx = \int_{\partial\Omega_n} \sigma_{ij}n_jv_i\;ds \quad \forall v_k \in V_k, \quad \mbox{for} \quad k = 1, 2, 3. 
\end{equation}


Can be defined for a nonlinear problem, give example for test case. Will be useful later for cohesive zone elements

\subsection{Anisotropic Stiffness Tensors}

The elastic response of the material is described by the material tensor $C$. In the simplest case of an
isotropic material only two parameters are needed, the Lam\'e parameters $\mu$ and $\lambda$. In this case
equation (\ref{eq-hooke}) can be written simply as
\begin{equation}
 \sigma_{ij}\lambda \epsilon_{kk}\delta_{ij}+2\mu\epsilon_{ij}.
\end{equation}
In the general anisotropic case needed to model composite materials the tensor $C$ is needed.
This tensor initially has $81$ components, however due to symmetry the number of independent
variables is mch lower. In particular,
\begin{equation}
 \begin{split}
  &C_{ijkl}=C_{klij},~~~~~~~~~~~~~~~~~~~~~~~~\forall i,j,k,l,~~~\text{(major symmetries)}\\
  \text{and }&C_{ijkl}=C_{jikl}=C_{ijlk}=C_{ijlk},~\forall i,j,k,l,~~~\text{(minor symmetries)}.
 \end{split}
\end{equation}

In order to simplify the calculations  we write stress and strain in Voigt notation \cite{ting}, i.e.
\begin{equation}
 \sigma = (\sigma_{11},\sigma_{22},\sigma_{33},\sigma_{23},\sigma_{13},\sigma_{12})\text{ and } 
 \epsilon = (\epsilon_{11},\epsilon_{22},\epsilon_{33},\epsilon_{23},\epsilon_{13},\epsilon_{12}).
\end{equation}
Then the stiffness tensor $C$ can be expressed as a $6\times 6$ matrix and (\ref{eq-hooke}) becomes a
simple matrix-vector multiplication $\sigma=C\cdot \epsilon$.

\subsection{Efficient Iterative Solvers}

In the large finite element calculations needed to model composite components the vast majority of the run time lies in the 
solution of the linear system (\ref{eq-linear}). However, due to the huge aspect ratio between the resin layer thickness and
the size of the modeled structures, the non-grid aligned anisotropy of the material properties and the large contrast
between fibrous and resin layers the linear system is extremely badly conditioned.

Direct solvers  are robust with regards to the ill-conditioning, but they have high memory demands and show poor
parallel scalability [{\color{green}Duf86}], making them ill-suited to large computations.
Iterative solvers pose an obvious alternative, 
however, their runtime depends strongly on the condition number of the linear system. The remedy is to `precondition' the system,
that is to develop an operation $\textbf{P}^{-1}$ so that the preconditioned system
$\textbf{P}^{-1}\textbf{K}{\bf u} = \textbf{P}^{-1}{\bf f}$ is well-conditioned.
It is important that the computation of $\textbf{P}^{-1}$ is cheap and can be performed in parallel.
Domain decomposition 
methods, such as the overlapping additive Schwarz method, provide one possible technique for acquiring such a parallel preconditioner,
see e.g. \cite{EfGa, domaindecomp}. We have chosen to use 
the module \textit{dune-geneo} which implements one such method, the GenEO preconditioner from \cite{geneo1,geneo2}. This preconditioner
has been shown  in \cite{dunecomp} to scale well for composite applications up to thousands of cores.

\subsubsection{1-level additive Schwarz preconditioner}
The underlying idea of the overlapping additive Schwarz method is to split the computational domain $\Omega$
into multiple overlapping  subdomains. We generate these overlapping subdomains by starting from a non-overlapping
subdivision $\left\{ \Omega_j^{'} \right\}_{j = 1}^N$ where each $\Omega_j^{'}$ is the union
of mesh elements from $\tau \in \mathcal{T}_h$. Then, an arbitrary number of layers of elements can be added to each
$\Omega_j^{'}$, resulting in overlapping subdomains $\Omega_j$. A locally restricted problem is solved on each of these subdomains
in parallel. Next, results from each
subdomain are added on the overlaps. The method then starts over solving the local problems,
taking into account the new information received from each of the neighboring subdomains until convergence.


For each subdomain $1 \leqslant j \leqslant N$, we denote the restriction of
  $V_h$ to $\Omega_j$ by
  \[ V_h ( \Omega_j) := \{ v |_{\Omega_j} : v \in V_h \} . \]
  Similarly, the space of functions with support in $\Omega_j$ can be defined
  as
  \[ V_{h, 0} ( \Omega_j) := \{ v |_{\Omega_j} : v \in V_h,
     \text{supp}\, ( v) \subset \Omega_j \} . \]

Elements are mapped from $V_{h, 0} ( \Omega_j)$ to $V_h$  by
extending with zero. We define the prolongation operator $R_j^T : V_{h, 0} ( \Omega_j) \rightarrow V_h$ by
\begin{displaymath}
   R_j^T v(x) = \begin{cases}v(x), & x\in  \Omega_j \\  0,  & x\in \Omega\backslash \Omega_j\end{cases}.
\end{displaymath}
The corresponding restriction operator $R_j$ is defined as
\begin{displaymath}
    \langle R_j g, v \rangle = \langle g, R_j^T v \rangle, ~~~~~
    \forall v \in V_{h, 0} ( \Omega_j) ,~g \in V_h' .
\end{displaymath}

  We denote the matrix form of these restriction operators $R_j$ by
$\textbf{R}_j$ and as before, the system matrix by $\textbf{K}$.
  
  Then we can define the problems restricted to the subdomains by
  \begin{eqnarray*}
    \textbf{K}_j & := \textbf{R}_j \textbf{K}\textbf{R}_j^T &
    \forall j = 1, \ldots, N.
  \end{eqnarray*}

\begin{definition}
  The one-level Schwarz preconditioner is given by
  \[ \textbf{M}_{\text{AS}, 1}^{- 1} := \sum_{j = 1}^N \textbf{R}_j^T \textbf{K}_j^{- 1} \textbf{R}_j . \]
\end{definition}

Due to the purely local exchange of information, the number of iterations required 
with the 1-level preconditioner tends to increase strongly
with the number of subdomains involved and depends on problem parameters. This can be
overcome by additionally solving a suitable coarse problem in each iteration and adding its contribution
to each subdomain.  Thus, the next step is to find a suitable coarse problem.

\subsubsection{2-level additive Schwarz preconditioner}
  For the coarse space $V_H$, denote the natural embedding by $R_H^T : V_H
  \rightarrow V_h$ and its adjoint by $R_H$.
  Further, we define the problems restricted to the coarse space:
  \begin{eqnarray*}
    \textbf{K}_H & := \textbf{R}_H \textbf{K}\textbf{R}_H^T.
  \end{eqnarray*}
  
  \begin{definition}
  The two-level Schwarz preconditioner is given by
  \[ \textbf{M}_{\text{AS}, 2}^{- 1} := \textbf{R}_H^T
     \textbf{K}_H^{- 1} \textbf{R}_H + \sum_{j = 1}^N \textbf{R}_j^T
     \textbf{K}_j^{- 1} \textbf{R}_j . \]
\end{definition}
  
In this definition of the two-level preconditioner the choice of coarse space $V_H$ has not been made explicit.  
Before we can do so we require a partition of unity, which
allows us to stitch together the local bases on each subdomain to form
a suitable global basis.

  Given a family of weights $\{ \mu_{j, k} : k \in \text{dof} ( \Omega_j), 1
  \leqslant j \leqslant N \}$ fulfilling $\mu_{j, k} \geqslant 1$ and 
  \begin{displaymath}
   \sum_{k \in \text{dof} ( \Omega_j)}  \frac{1}{\mu_{j, k}} = 1, ~~~~\forall 1 \leqslant j \leqslant N,
  \end{displaymath}
the associated partition of unity operator $\Xi_j : V_h ( \Omega_j)
  \rightarrow V_{h, 0} ( \Omega_j)$ is defined by
  \begin{displaymath}
    \Xi_j ( v) := \sum_{k \in \text{dof} ( \Omega_j)}  \frac{1}{\mu_{j,
     k}} v_k \phi_k |_{\Omega_j}, ~~~~\forall v \in V_h ( \Omega_j)  .
  \end{displaymath}


Using this partition of unity we define the generalised eigenproblems that provie
the basis of te GenEO coarse space.
  For each subdomain $j = 1, \ldots, N$, we define the generalized
  eigenproblem
  \begin{equation}\label{eq-geneo}
   a_{\Omega_j} ( p, v) = \lambda a_{\Omega_j^o} ( \Xi_j ( p), \Xi_j ( v)),~~~~~ \forall v \in V_h ( \Omega_j),
  \end{equation}
  where $a_{\Omega_j}$ and $a_{\Omega_j^o}$ is the bilinear form associated to the PDE on the subdomain and overlap
  respectively.

These generalised eigenproblems are local to their respective subdomain
$\Omega_j$ and can be computed in parallel. To use
the eigenvectors as a global basis, they still need to be extended to the entire domain
by the partition of unity operators.

\begin{definition}\label{GenEOSpace}(GenEO coarse space)
  For each subdomain let $( p_k^j)^{m_j}_{k = 1}$ be the
  eigenfunctions from  (\ref{eq-geneo})
  corresponding to the $m_j$ smallest eigenvalues. Then the GenEO coarse space
  is defined as
  \[ V_H := \text{span} \{ R_j^T \Xi_j ( p_k^j) ~:~ k = 1, \ldots, m_j,\, j = 1, \ldots, N \} . \]
\end{definition}

The only parameter remaining to be chosen for this coarse space is the numbers
$m_j$ of eigenmodes on each subdomain to be included. 

  For all $1 \leqslant j \leqslant N$, let
  \[ m_j := \min \left\{ m : \lambda_{m + 1}^j > \frac{\delta_j}{H_j}
     \right\}, \]
  where $\delta_j$ is a measure of the width of the overlap $\Omega_j^o$ and
  $H_j = \text{diam} ( \Omega_j)$.
  
  This choice of coarse space and of eigenvalue threshold has been shown in \cite{geneo1}
  to lead to a well-conditioned system independent of the number of subdomains and of the
  variations in coefficients of the PDE.
  
\begin{theorem} (Condition number bound)
  Assume that the bilinear form can be decomposed into per-element bilinear forms {\cite[Assumption 2.1]{geneo1}}
  and that the coercivity requirements from {\cite[Assumptions 3.12, 3.13]{geneo1}} are met.

  Then we can bound the condition number of the preconitioned system as follows.
  \[ \kappa ( M^{- 1}_{\text{AS}, 2} A) \leqslant ( 1 + k_0) \left[ 2 + k_0 (
     2 k_0 + 1) \max_{1 \leqslant j \leqslant N} \left( 1 +
     \frac{H_j}{\delta_j} \right) \right] . \]
\end{theorem}

The improved conditioning of the system is gained at the price of a potentially large number of coarse
basis vectors. Still, as the numerical results in Section \ref{sec-examples} indicate, this advantage
is well worth the price in anisotropic problems.
The choice of threshold by which the eigenvectors are selected is
not unique and can be tuned to the problem. More details on the implementation of this
preconditioner can be found in \cite{Seel}.



%------------------------------------------------------------------------------

\section{\texttt{dune-composites}: overview, capabitilies and dependencies}
The structure of the module \textit{dune-composites} is shown in Figure \ref{fig-flow}.
Essentially the module
solves the linear elasticity equations with general, anisotropic
stiffness tensor, applicable for modelling
 composite structures.  This includes an interface to handle 
 stacking sequences and complex part geometries in the \verb=Geometry= folder, as well as
  complex boundary conditions such as multi-point constraints, 
 or periodic boundary conditions in the \verb=Setup= folder.
 



\begin{figure}[ht]
 \centering
\includegraphics{Images/flowchart.pdf}
\caption{Overview of the module \textit{dune-composites}.}
\label{fig-flow}
\end{figure}

%------------------------------------------------------------------------------

\subsection{Complex Geometries for \tt{Dune::Yaspgrid}}
DUNE already provides general tensor-product grids through the wrapper class {\tt GeometryGrid}. This class allows us to map
a cube geometry to more general geometries simply by providing a mapping from one to the other. The new geometry will
still have a tensor-product mesh. Examples of grids that can be created this way are provided in Section \ref{sec-examples}.

\subsection{Application specific loading and constraints}
A multipoint constraint connects a set of nodes by restricitng them so that their displacements
are identical. In \textit{dune-composites} this constraint is approximated by applying a thin layer
of isotropic, extremely stiff material along the nodes covered by the constraint.

The module \textit{dune-composites} also provides support for periodic boundary conditions. It should
be noted that this only works in parallel.


\subsection{Special Elements for Composite Analysis}

The equations (\ref{eq-fe})  are prone to shear locking.
This is a result of using piecewise linear basis functions and few elements through thickness, in this
case the bending cannot be sufficiently resolved and the structure reacts "too stiffly". 
One approach to avoiding shear locking is the use of higher order elements as these allow
bending. The serendipity family of elements provides higher order elements with a reduced number
of degrees of freedom \cite{serendipity}. These basis functions are quadratic in one direction only.
They have only $20$ degrees of freedom as opposed to the $27$ degrees of freedom of the standard
quadratic element. They do not have the $6$ face and $1$ interior degree of freedom, leaving only the $8$
nodal and $12$ edge degrees of freedom.


\subsection{Special Quadrature Rules}

Another approach to minimise the effects of shear locking is the use of under-integration. When using
linear basis functions, only one quadrature point is used so that the element can move more freely.
In some loadings this leads to the excitation of spurious zero energy modes. These can be eliminated
while still keeping the advantages of the under-integration by adding a small number to the eigenvalues
which have become $0$ only due to the inexact quadrature rule \cite{hourglass}. 

\subsection{Thermal Loading}

A temperature change induces a thermal strain 
\begin{equation}\label{eq-thermalstrain}
 \epsilon_{ij}^\text{thermal} = \alpha_{ij} \Delta T,
\end{equation}
where $\Delta T$ is the temperature change and $\alpha_{ij}$ are the thermal strain coefficients.
This thermal strain causes a contraction (or expansion) of the material 
and can cause considerable stress, particularly in the vicinity of the defect.


\subsection{Stress calculation}
%post-processing, failure criteria

In an engineering context the main interest is often in the resultant stresses not the displacements
themselves.
If we have solved the stiffness equations
\begin{equation}\label{eq-linear}
 \textbf{K} u = f,
\end{equation}
for the displacements $u$, we can calculate the stresses $\sigma$ directly from the 
strains $\epsilon$ via (\ref{eq-hooke}). To calculate the strains $\epsilon$ we 
derive the displacements $u$. This means that it is generally reasonable to represent the
stresses with a basis of one order lower than that of the displacement calculation.
We provide both a piecewise
constant and a linear output option for stresses. 
Generally the piecewise constant option
should be used when using a linear basis for the displacements and the linear option should be used when the displacements
are represented using quadratic elements. 
The piecewise constant stresses are
evaluated at Gauss-points, while the linear stresses are calculated at the nodes.

Note: Should we mention patch recovery methods here? \cite{patchrecovery}

Failure criteria map the stress data to separate "failed" states from
"unfailed" states. Various different failure criteria are used in the engineering community,
we provide the quadratic criterion \cite{camanho}. The user can input further failure criteria.


\subsection{Plotting}
We use the standard plotting tools provided by the DUNE library and optionally provide output
of displacements, stresses and failure criteria in the form of \verb=.vtk= files.


\subsection{Integration with \tt{dune-muq} for UQ}



  
%------------------------------------------------------------------------------
\section{Applications}\label{sec-examples}
In this section we introduce the functionalities of \textit{dune-composites} using a series of examples of increasing
complexity. 


\subsection{Flat Composite Laminate Plates}

\textbf{Test Case 1:} Pristine Flat Plate

\begin{figure}[ht]
\centering
\includegraphics[scale=0.3]{Images/problem1.png}
\caption{The deformed flat laminate plate set up in test case 1.}
 \label{figure-problem1}
\end{figure}


As an initial test case we examine a flat laminate plate, consisting of 12 composite layers (and 11 isotropic resin
interface layers). For this small test case we use a width of $W=5$ cm and a length of $L=22$ cm. The overall thickness
is $T=2.98$ cm where the composite plies
are $23$ mm thick and the resin layers are $2$ mm thick. 
The deformed geometry is shown in Figure \ref{figure-problem1}. The stacking sequence we have used is:
\begin{displaymath}
 \left[\pm 45 /90/0/\mp 45/0/90/\pm45\right].
\end{displaymath}

The module \textit{dune-composites} allows a very simple interface to enter these stacking sequence.
The user can write a stacking sequence to a .csv file, the number of layers
and stacking sequence is then read out automatically. It is possible to change the stacking sequence
along the length of the laminate using a "macro" grid, for details see test case 6.

The flat plate is placed under axial strain by clamping one limb and applying a displacement to the other limb.
More precisely,
\begin{align}\label{eq-bc-problem1}
 u &= 0, ~~~&& \text{ if } x = 0,\\
 u &= 1,    && \text{ if } x= L,\\
 \sigma_{ij}\cdot n_j &= 0, &&\text{else.}
\end{align}
These boundary conditions are generated using
\begin{c++}
template<typename I>
bool isDirichlet(const I & ig, const typename Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const {
      Dune::FieldVector<double,3> xg = ig.geometry().global(x);
      if(xg[0] < 1e-6 || xg[0] > model.limb-1e-6)
           return true;
      return false;
}
inline void evaluateGlobal(const DomainType & x, RangeType & u) const {
      u = 0.0;
      if(x[0] > model.limb-1e-6 && i_dim==2)
           u = -1;
}
\end{c++}


As this is a small test case, it can be run sequentially. For sequential cases the DUNE AMG preconditioner can be
used.


\textbf{Test Case 2:} Defected Flat Plate

In this second example, we introduce defects in the laminate. The geometry and stacking sequence used are identical
to those in the first example. However, we introduce a wrinkle in the form of a perturbation in $z$, which depends
on all three coordinates. The perturbed
coordinates $(\hat x, \hat y, \hat z)$ 
are given by
\begin{equation}\label{eq-wrinkle}
\hat x = x, \quad \hat y = y \quad \text{and} \quad \hat z = z+f(x,y,z).
\end{equation}
The user can choose the wrinkle by defining the function $f$ which defines the wrinkle.
In this test example we have chosen a cosine wave in $x$,
which decays in all coordinate directions as $\text{sech}(\cdot)^2$.
\begin{c++}
 y[2] += defect_size * cos(M_PI * frequency * x[0]) 
           * pow(cosh(damping[0]*M_PI * (defect_loc[0] - x[0])/model.limb),-2.)
           * pow(cosh(damping[1]*M_PI * (defect_loc[1] - x[1])/model.Y),-2.)
           * pow(cosh(damping[2]*M_PI * (defect_loc[2] - x[2])/model.thickness),-2.);
\end{c++}


The mechanical loading in this example consists of a pressure applied to the top of the defected flat plate.
More precisely,
\begin{align}\label{eq-bc-problem2}
  u &= 0, ~~~&& \text{ if } x = 0,\\
 \frac{\partial u}{\partial x} &= -1,    && \text{ if } z = T,\\
 \frac{\partial u}{\partial x} &= 0, &&\text{else.}
\end{align}


\goodbreak


\textbf{Test Case 3:} Thermal Loading

The third example introduces a non-zero right hand side to the problem.
In this example the geometry of the previous example is used. Homogeneous Dirichlet boundary conditions
are applied to the end of one limb to ensure well-posedness. We then apply a temperature induced strain. 
This thermal strain causes a contraction (or expansion) of the material 
and can cause considerable stress, particularly in the vicinity of the defect.

\subsection{Curved Laminates}

\textbf{Test Case 4:}  Pristine Curved Laminate

Next we introduce a geometry transformation. The rectangular grid is transformed into a curved laminate.

\begin{c++}
//No transformation necessary for first limb
if(x[0] >= model.limb && x[0] <= model.limb+model.radius){           //Model the curve
     theta = model.angle*M_PI/180*(ydef[0]-model.limb)/model.radius; //Angle
     double r = model.radius + model.thickness - ydef[2];            //Radius
     y[0] = model.limb + r*sin(theta);
     y[1] = ydef[1];
     y[2] = model.radius + model.thickness - r*cos(theta);
}
if(x[0] > model.limb+model.radius){     //Model the second limb
     theta = model.angle*M_PI/180;      //Angle
     double y0 = ydef[0] - model.limb - model.radius;
     double y2 = ydef[2];
     y[0] = model.limb + model.radius + model.thickness + cos(theta)*y0 - sin(theta)*y2;
     y[1] = ydef[1];
     y[2] = model.radius + model.thickness + sin(theta)*y0 + cos(theta)*y2;
}
\end{c++}


Then one limb is held in place, while a multi-point constraint and a moment are applied to the other limb.
This opens up the corner, causing complex stresses at the apex of the curve. A thermal load is also applied.



\textbf{Test Case 5:} Defected, Curved Laminate

Then the defect can be reintroduced. The defect is defined in the flat geometry as in test case 2, which is
then transformed into the curved geometry. This test case is examined in more detail in \cite{dunecomp}.

This case is too large to be run sequentially using AMG, a parallel preconditioner is required. We use
the module \textit{dune-geneo} which implements the GenEO preconditoner from \cite{geneo1,geneo2}.


\subsection{Wingbox}

\textbf{Test Case 6:} Pristine Wing Box

\begin{figure}[ht]
\centering
 \includegraphics[scale=0.2]{Images/problem6_annotated.png}
 \includegraphics[scale=0.2]{Images/problem6_2_annotated.png}
 \caption{Left: Geometry of the wing box. Right: Stacking sequence around the corner of the wingbox.}
 \label{fig-problem6}
\end{figure}


This test case is too large to run sequentially. It is set up to be run using the GenEO preconditioner.

The wingbox has a width of $W=1$ m, a height of $H=300$ cm and a variable length $L$. The overall thickness
is $T=9.93$ cm, where the composite plies are $23$ mm thick and the resin layers are $2$ mm thick.
This geometry is shown on the left in Figure \ref{fig-problem6}.

This test case uses a stacking sequence that changes around the part. This change of stacking sequence is easy for the
user to specify. In the \verb=.csv= file specifying the sequence, the first line contains a "macro" corresponding to 
a location around the geometry, different stacking sequences can be input for each section. In this
test case the stacking sequence for the radii, the straight top and bottom segments and the left and right segments
is different. There is a short segment on which one stacking sequence merges into the next as seen on the right in
Figure \ref{fig-problem6}.

In order to create the closed curve of this wingbox using a tensor product grid, we have introduced a periodic boundary
condition where the curve closes.

The loading chosen for this test case includes both mechanical and thermal. There is a pressure to the top of the wingbox, 
as well as a pressure from the inside of the wingbox. One side is clamped and the other has a multi-point constraint
applied.


The experiment in Figure \ref{fig-weakscaling} was carried out on the 
University of Bath HPC cluster {\tt Balena}. This consists of 
192 nodes each with two 8-core Intel Xeon E5-2650v2 Ivybridge
processors, each running at 2.6 GHz and giving a total of 3072 available cores. 

We carry out a weak scaling
experiment, that is, we increase the problem size proportionally to
the number of cores used, we expect that
the computational time should remain roughly constant in this experiment. 
As the number of cores $N_\text{cores}$
grows, we increase the length $L$ of the wingbox.  
In Figure \ref{fig-weakscaling}, we see that after a slight initial growth the 
scaling of the iterative solver in DUNE with GenEO preconditioner
is indeed almost optimal to at least $1920$ cores.

\begin{figure}[ht]
 \centering
 \includegraphics[scale=1]{Images/time-geneo.pdf}
 \caption{Scaling of {\em dune-composites} using the GenEO
   preconditioner on up to $1920$ cores. The problem size varies from about $3.5$ million degrees of freedom on
192 cores to about $310$ million degrees of freedom on 1920 cores.}
 \label{fig-weakscaling}
\end{figure}


\subsection{MUQ}

\textbf{Test Case 7:} Test Case 2 linked with 'dune-muq'

\subsection{Unstructured Grids}

\textbf{Test Case 8:} Isotropic Plate with a hole

\begin{figure}[ht]
 \centering
 \includegraphics[scale=0.3]{Images/problem8.png}
 \caption{Von-Mises failure criterion plotted for test case 8.}
\end{figure}

It is also possible to import unstructured grids into \textit{dune-composites} using either \textit{dune-uggrid} or
\textit{dune-alugrid}. However, when using unstructured grids it is no longer possible to use the parallel preconditioner
GenEO and thus to run very large anisotropic problems.

When running using an unstructured mesh, the mesh file simply needs to be placed into the \verb=grids/= folder and linked into
the problem file. It is important in this case to define the physical groups corresponding to the different material
types in the mesh file. If the geometry requires new material types these can be added in 
the file \verb=Geometry/defineMatProp.hh=.

For this test case we examine a flat isotropic plate with a small hole. The plate has a width and length of $1$ cm and a thickness
of $10$ mm. The hole at the center of the plate has a radius $10$ mm.

The plate is held clamped on  one end while the other is pulled in $x$-direction.

In this test case we demonstrate how to use a different failure criterion. The standard failure criterion used by the
module is the Camanho criterion. This failure criterion is used to model failure by delamination in the resin regions
of laminates. However, for this isotropic test case we use the von-Mises failure criterion instead.
Any criterion which uses local stress information can be easily added into the folder \verb=PostProcessing/= and
linked from the corresponding problem file.
\begin{c++}
template<typename PROBLEM, typename EG>
double failureCriterion(const PROBLEM & problem,const EG& eg, std::vector<double> xlc){
    //allowables for aluminium
    double s11 = 250.;
    double maxF = pow(xlc[1]/s11,2);
    return sqrt(maxF);
}
\end{c++}



%------------------------------------------------------------------------------
\section{Conclusions}


\bibliographystyle{abbrvnat}
\bibliography{ElastPaper}
\end{document}
