// Define Scalar Dirichlet Boundary Conditions
template<typename GV, typename MODEL, typename RF>
class Scalar_BC :
    public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
    Scalar_BC<GV,MODEL,RF> >,
    public Dune::PDELab::InstationaryFunctionDefaults
{
    public:

        typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
        typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, Scalar_BC<GV,MODEL,RF>> BaseT;

        typedef typename Traits::DomainType DomainType;
        typedef typename Traits::RangeType RangeType;

        // Constructor
        Scalar_BC(const GV & gv, MODEL& model_, int i_dim_) :
            BaseT(gv), model(model_), i_dim(i_dim_){ }

        template<typename I>
            bool isDirichlet(const I & ig, const typename Dune::FieldVector<typename I::ctype,2> & x) const
            {
                Dune::FieldVector<double,3> xg = ig.geometry().global( x );
                return model.isDirichlet(xg,i_dim);
            }

        inline void evaluateGlobal(const DomainType & x, RangeType & u) const
        {
           /* u = 0.0;
            if(x[0] > model.limb-1e-6 && i_dim==2)
                u = -1;*/
            u = model.evaluateDirichlet(x,i_dim);

        } // end inline function evaluateGlobal

    private:
        MODEL& model;
        int i_dim;
};

