#ifndef REGION__HH
#define REGION__HH

namespace Dune{

namespace Composites{


class Region{

public:

	Region(){}

	int Type;
	std::vector<double> matprop;
	double density; 

	void inline setType(int t){
		Type = t;
		if (t == 0){ matprop.resize(2);	}
		else if(t == 1){ matprop.resize(9);}
		else{ matprop.resize(21); }
	}

	void inline setProp(int i, double val){
		assert(i < matprop.size());
		matprop[i] = val;
	}

	void inline setDensity(double val){density = val;}


private:


};