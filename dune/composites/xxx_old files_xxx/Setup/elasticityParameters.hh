#ifndef _ELASTICITY_PARAMETERS_hh
#define _ELASTICITY_PARAMETERS_hh

#include "../Driver/Cijkl.hh"
#include "../Geometry/RandomFields/defectGenerator.hh"

//Yaspgrid version
template<typename GV, typename GV2, typename RF, typename PGMap, typename MAT_PROP, typename MODEL>
class ElasticityProblem :
	public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,GV::dimension 
          * GV::dimension,Dune::FieldMatrix<RF,GV::dimension,GV::dimension> >, ElasticityProblem<GV,GV2,RF,PGMap,MAT_PROP,MODEL> >
{
    public :
    typedef RF RFType;
    typedef Dune::PDELab::GridFunctionTraits<GV,RF,
    GV::dimension*GV::dimension,Dune::FieldMatrix<RF,GV::dimension,GV::dimension> > Traits;
    typedef Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,
    GV::dimension*GV::dimension,Dune::FieldMatrix<RF,GV::dimension,GV::dimension> >,
    ElasticityProblem<GV,GV2,RF,PGMap,MAT_PROP, MODEL> > BaseT;
    

    // ================================================================================
    // 			Constructor for ElasticityTensor
    // ================================================================================
    
    ElasticityProblem(const GV& gv_, const GV2& gv2_, MODEL& model_)
    : gv(gv_), gv2(gv2_), is(gv.indexSet()), Cijkl(is.size(0)), //Cijkl_local(is.size(0)),
      Cijkl_strain(is.size(0)), density(is.size(0)), model(model_), theta(is.size(0)), thetaFct(is.size(0)), lay_id(is.size(0)), materialType(is.size(0)), geometryType(is.size(0))
    {
        typedef typename GV::Traits::template Codim<0>::Iterator ElementIterator;
        // ================================================================================
        // 	<< a >>		Compute Elasticity Tensors for Isotropic & Orthropic Cases
        // ================================================================================
        auto my_rank = gv.comm().rank();

       /* CalCijkl<MAT_PROP> field(mat_prop, my_rank);*/
        // ================================================================================
        // 	<< b >>		Calculate Cijkl[id] in each element
        // ================================================================================
        auto it2 = gv2.template begin<0>();
        
        for (ElementIterator it = gv.template begin<0>(); it!=gv.template end<0>(); ++it)
        { // loop through each element
            
            int id = is.index(*it); // Get element id

            Cijkl[id] = model.getElasticTensor(id);

            Dune::FieldVector<double,3> x = it2->geometry().center();

            double dt = model.returnTheta(x);

            dt = (180.0/M_PI) * dt; // radians

            it2++;

            /*
            // Physical Groups are (at least) 3 digit numbers which define both region and layer id
            // abc - a is the region number and bc - is the layer id
            // Currently there is no maximum on the number regions (i.e 12323 defines region 123 and layer 23)
            // There is at the moment a maximum number of layers = 1000
            int region_id = floor(pg / 1000) - 1;
            int layer_id  = pg - (1 + region_id) * 1000;
            //std::cout << pg <<" " <<region_id<< std::endl;

            int mat_id = mat_prop.lay_mat[region_id][layer_id];

            GeometryFct<Dune::FieldVector<double,3>,Dune::FieldVector<double,3>,MODEL> g(model,myDefect);
            Dune::FieldVector<double,3> x = it2->geometry().center();

            //Get stacking sequence at this point
            int loc = model.macroGrid.size()-1;
            for(int i = 1; i < model.macroGrid.size(); i++){
                if(x[0] < model.macroGrid[i] && x[0]){
                    loc = i-1;
                    break;
                }
            }

            //Calculate angle
            double dt = g.returnTheta(x);
            dt = 180.0/M_PI*dt;                 //transform from radians to degrees
            thetaFct[id] = [=](Dune::FieldVector<double,3> xh){return g.returnTheta(x);};
            theta[id] = {mat_prop.region_data[region_id][0], dt, mat_prop.region_data[region_id][2] + model.stack_seq[layer_id][loc]};
            //std::cout << "Dt "  << theta[id][0] << " " << theta[id][1] << " " << theta[id][2] << std::endl;
            lay_id[id] = layer_id;
            geometryType[id] = mat_prop.region_info[region_id];

            materialType[id] = mat_prop.lay_mat[region_id][layer_id];
            //layerRotation[id] = mat_prop.lay_rot[region_id][layer_id];

            field.eval(Cijkl[id],              theta[id],mat_id,mat_prop.materialType[mat_id],region_id);
            //field.eval_local(Cijkl_local[id],  theta[id],mat_id,mat_prop.materialType[mat_id],region_id);
            field.eval_strain(Cijkl_strain[id],theta[id],mat_id,mat_prop.materialType[mat_id],region_id);

            density[id] = mat_prop.matprop[mat_id][21];
            it2++;*/
        } // End for-loop of each element */
    }; // End Constructor

    void setParallel(bool isPar){ parallel = isPar; }

   
 
    private :

    const GV& gv;
    const GV2& gv2;
    const typename GV::IndexSet& is;
    std::vector<Dune::FieldMatrix<RF,6,6>> Cijkl;
    std::vector<Dune::FieldMatrix<RF,6,6>> Cijkl_strain;
    //std::vector<Dune::FieldMatrix<RF,6,6>> Cijkl_local;
    std::vector<RF> density;
    MODEL& model;
    std::vector<std::vector<double>> theta;
    std::vector<std::function<double(Dune::FieldVector<double,3>)>> thetaFct;
    std::vector<int> lay_id;
    std::vector<int> geometryType;
    std::vector<int> materialType;
    std::vector<Dune::FieldVector<RF,6>> eps;
    int solver_type;
    int maxIt;
    int verbosity;
    int dofel;
    int intorder;
    bool parallel;
    double tolerance;
   

};


#endif
