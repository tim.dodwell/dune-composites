#ifndef _problem_definition_hh
#define _problem_definition_hh

#include "../Driver/Cijkl.hh"
#include "../Geometry/RandomFields/defectGenerator.hh"

//Yaspgrid version
template<typename GV, typename GV2, typename RF, typename PGMap, typename MAT_PROP, typename MODEL>
class ElasticityProblem :
	public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,GV::dimension 
          * GV::dimension,Dune::FieldMatrix<RF,GV::dimension,GV::dimension> >, ElasticityProblem<GV,GV2,RF,PGMap,MAT_PROP,MODEL> >
{
    public :
    typedef RF RFType;
    typedef Dune::PDELab::GridFunctionTraits<GV,RF,
    GV::dimension*GV::dimension,Dune::FieldMatrix<RF,GV::dimension,GV::dimension> > Traits;
    typedef Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,
    GV::dimension*GV::dimension,Dune::FieldMatrix<RF,GV::dimension,GV::dimension> >,
    ElasticityProblem<GV,GV2,RF,PGMap,MAT_PROP, MODEL> > BaseT;
    // ================================================================================
    // 			Constructor for ElasticityTensor
    // ================================================================================
    ElasticityProblem(const GV& gv_, const GV2& gv2_, const PGMap& elemIndx2PG, MAT_PROP mat_prop, MODEL model_,COEFF myDefect_)
    : gv(gv_), gv2(gv2_), is(gv.indexSet()), Cijkl(is.size(0)), //Cijkl_local(is.size(0)),
      Cijkl_strain(is.size(0)), density(is.size(0)), model(model_), theta(is.size(0)), thetaFct(is.size(0)), lay_id(is.size(0)), materialType(is.size(0)), geometryType(is.size(0)), myDefect(myDefect_)
    {
        typedef typename GV::Traits::template Codim<0>::Iterator ElementIterator;
        // ================================================================================
        // 	<< a >>		Compute Elasticity Tensors for Isotropic & Orthropic Cases
        // ================================================================================
        auto my_rank = gv.comm().rank();
        CalCijkl<MAT_PROP> field(mat_prop, my_rank);
        // ================================================================================
        // 	<< b >>		Calculate Cijkl[id] in each element
        // ================================================================================
        auto it2 = gv2.template begin<0>();
        for (ElementIterator it = gv.template begin<0>(); it!=gv.template end<0>(); ++it)
        { // loop through each element
            int id = is.index(*it); // Get element id
            int pg = elemIndx2PG[id]; // Map from element to physical group
            // Physical Groups are (at least) 3 digit numbers which define both region and layer id
            // abc - a is the region number and bc - is the layer id
            // Currently there is no maximum on the number regions (i.e 12323 defines region 123 and layer 23)
            // There is at the moment a maximum number of layers = 1000
            int region_id = floor(pg / 1000) - 1;
            int layer_id  = pg - (1 + region_id) * 1000;
            //std::cout << pg <<" " <<region_id<< std::endl;

            int mat_id = mat_prop.lay_mat[region_id][layer_id];

            GeometryFct<Dune::FieldVector<double,3>,Dune::FieldVector<double,3>,MODEL> g(model,myDefect);
            Dune::FieldVector<double,3> x = it2->geometry().center();

            //Get stacking sequence at this point
            int loc = model.macroGrid.size()-1;
            for(int i = 1; i < model.macroGrid.size(); i++){
                if(x[0] < model.macroGrid[i] && x[0]){
                    loc = i-1;
                    break;
                }
            }

            //Calculate angle
            double dt = g.returnTheta(x);
            dt = 180.0/M_PI*dt;                 //transform from radians to degrees
            thetaFct[id] = [=](Dune::FieldVector<double,3> xh){return g.returnTheta(x);};
            theta[id] = {mat_prop.region_data[region_id][0], dt, mat_prop.region_data[region_id][2] + model.stack_seq[layer_id][loc]};
            //std::cout << "Dt "  << theta[id][0] << " " << theta[id][1] << " " << theta[id][2] << std::endl;
            lay_id[id] = layer_id;
            geometryType[id] = mat_prop.region_info[region_id];

            materialType[id] = mat_prop.lay_mat[region_id][layer_id];
            //layerRotation[id] = mat_prop.lay_rot[region_id][layer_id];

            field.eval(Cijkl[id],              theta[id],mat_id,mat_prop.materialType[mat_id],region_id);
            //field.eval_local(Cijkl_local[id],  theta[id],mat_id,mat_prop.materialType[mat_id],region_id);
            field.eval_strain(Cijkl_strain[id],theta[id],mat_id,mat_prop.materialType[mat_id],region_id);

            density[id] = mat_prop.matprop[mat_id][21];
            it2++;
        } // End for-loop of each element
    }; // End Constructor

    inline void evaluate(const typename Traits::ElementType& e,
                         Dune::FieldMatrix<double,6,6>& y) const{
        y = Cijkl[is.index(e)];
    }

    inline void evaluate(int id, Dune::FieldMatrix<double,6,6>& y) const{
        y = Cijkl[id];
    }
    /*inline void evaluate_local(int id, Dune::FieldMatrix<double,6,6>& y) const{
      y = Cijkl_local[id];
    }
    inline void evaluate_local(const typename Traits::ElementType& e,
                         Dune::FieldMatrix<double,6,6>& y) const{
        y = Cijkl_local[is.index(e)];
    }*/
    
    inline void evaluate_strain(int id, Dune::FieldMatrix<double,6,6>& y) const{
      y = Cijkl_strain[id];
    }
    inline void evaluate_strain(const typename Traits::ElementType& e,
                         Dune::FieldMatrix<double,6,6>& y) const{
        y = Cijkl_strain[is.index(e)];
    }


    void setDofel(int number){
      dofel = number;    
    }
    inline int getDofel(){
      return dofel;      
    }

    void setIntOrder(int order){
      intorder = order;   
    }    
    inline const int getIntOrder() const{
      return intorder;
    }

    inline  std::vector<double> getTheta(int id) const{
      return theta[id];
    }
    inline  std::vector<double> getTheta(const typename Traits::ElementType& e) const{
      return theta[is.index(e)];    
    }
    inline  std::vector<double> getThetaFct(const typename Traits::ElementType& e, Dune::FieldVector<double,3> x) const{
        std::vector<double> angle = theta[is.index(e)];
        return angle;
    }

    inline  int getLayID(int id) const{
      return lay_id[id];    
    }
    inline  int getLayID(const typename Traits::ElementType& e) const{
      return lay_id[is.index(e)];    
    }
    inline  int getMatID(int id) const{
      return geometryType[id];    
    }
    inline  int getMatID(const typename Traits::ElementType& e) const{
      return geometryType[is.index(e)];
    }
    inline  int getMaterialType(int id) const{
      return materialType[id];    
    }
    inline  int getMaterialType(const typename Traits::ElementType& e) const{
      return materialType[is.index(e)];
    }

    void setSolver(int solver){
        solver_type = solver;
    }
    inline int getSolver(){
        return solver_type;
    }

    void setParallel(bool answer) {
        parallel = answer;
    }

    inline bool isParallel() const{
        return parallel;
    }

    void setMaxIt(int N){
        maxIt = N;
    }

    inline int getMaxIt(){
        return maxIt;
    }

    void setVerbosity(int N){
        verbosity = N;
    }

    inline int getVerbosity(){
        return verbosity;
    }

    void setTol(double tol)
    {
        tolerance = tol;
    }

    inline double getTol(){
        return tolerance;
    }

    inline void evaluateDensity(const typename Traits::ElementType& e, Dune::FieldVector<double,3>& f) const{
        f[0] = 0.0;
        f[1] = 0.0;
        f[2] = 0.0;
        if(config.get<int>("geneo",0)==1)
            f[2] = -10.0; // 9.80665 * density[is.index(e)];
    }

    inline void evaluateHeat(Dune::FieldVector<double,6>& f ,bool resin) const{
        f = 0.0;
        if(config.get<bool>("residualHeat",false)){
            double deltaT = -160.; //temperature difference
            double alpha11 = config.get<double>("alpha11", -.342)*1e-6;
            double alpha22 = config.get<double>("alpha22", 25.8)*1e-6;
            double alpha33 = config.get<double>("alpha33", 25.8)*1e-6;
            double alpharesin = config.get<double>("alpharesin",25.8)*1e-6;
            if(resin){
                f[0] = alpharesin*deltaT;
                f[1] = alpharesin*deltaT;
                f[2] = alpharesin*deltaT;
            }
            else{
                f[0] = alpha11*deltaT;
                f[1] = alpha22*deltaT;
                f[2] = alpha33*deltaT;
            }
        }
    }

 inline void evaluateNeumann(const Dune::FieldVector<double,3> &x,Dune::FieldVector<double,3>& h,const  Dune::FieldVector<double,3>& normal) const{
        // s_ij n_j = h_i
        h = 0;
        double p_m = config.get<double>("moment",0); // Initialise pressure & traction values
        double p_p = config.get<double>("pressure",0); // Initialise pressure & traction values

        //handle special cases
        if(config.get<int>("geneo",0)==1) p_m = 0.;

        double xval = model.radius;
        if(model.num_limbs == 2) xval += model.limb;

        double resin_treatment = 0;
        if(config.get<int>("edge_treatment",0) == 1) resin_treatment = config.get<double>("edge_width");

        //If model of a cube, apply a pressure to the top face
        if(model.radius == 0){
            if(x[0] > xval && x[2] > model.thickness-1e-06){
                for (int i = 0; i < 3; i++)
                    h[i] = p_p * normal[i];
            }
            return;
        }
        if(config.get<std::string>("grid_name","cube") == "wingbox"){
            //BC for wingbox example
            //1. Pressure bending down one end of the wingbox
            if(x[2] > (model.thickness+model.radius)*2+model.limb_b -1e-06){
             for(int i = 0; i < 3; i++)
                h[i] = p_p*normal[i];
		}
            //2. Fuel pressure from the inside
            if(
                    (x[0] > model.radius+model.thickness-1e-6 && x[0] < model.radius+model.thickness+model.limb_a -1e-6 && fabs(x[2]-model.thickness)<1e-6)
                    || (x[0] >model.radius+model.thickness-1e-6 && x[0] < model.radius+model.thickness+model.limb_a -1e-6 && fabs(x[2]-(model.thickness+model.limb_b+model.radius*2))<1e-6)
                    || (x[2] >model.radius+model.thickness-1e-6 && x[2] < model.radius+model.thickness+model.limb_b -1e-6 && fabs(x[0]-model.thickness)<1e-6)
                    || (x[2] >model.radius+model.thickness-1e-6 && x[2] < model.radius+model.thickness+model.limb_b -1e-6 && fabs(x[0]-(model.thickness+model.limb_a+model.radius*2))<1e-6)
                    ){
             for(int i = 0; i < 3; i++)
                h[i] = p_m*normal[i];
            }
        }
        //lshape
        //First handle special case 85 ply problem
        if(config.get<std::string>("grid_name","cube").compare("lshape_85")==0){
            double theta = model.angle*M_PI/180.;
            auto y0hat = [=](double y2){return model.limb+model.radius+model.thickness + cos(theta)*model.limb - sin(theta)*y2;};
            auto y2hat = [=](double y2){return            model.radius+model.thickness + sin(theta)*model.limb + cos(theta)*y2;};

            //Ensure we are on the upper face of the limb section
            if(x[0] <= y0hat(0) && x[0] >= y0hat(model.thickness) && x[2] <= y2hat(0) && x[2] >= y2hat(model.thickness)){
                Dune::FieldMatrix<double,3,3> R = { { cos(46*M_PI/180),0, sin(46*M_PI/180)}, {0,1,0}, {-sin(46*M_PI/180),0, cos(46*M_PI/180)}};
                auto rot_normal = normal;
                R.mv(normal,rot_normal);
                //First apply pressure of 40 kN/Area to limb
                double Area = model.Y*model.thickness;
                for(int i = 0; i < 3; i++)
                    h[i] = -p_p*1000/Area * rot_normal[i];
            }
            //Then, apply a moment of p*82.4mm to the same limb
            double Area = model.thickness*model.Y;
            double running_moment = 84.88*p_p*1000/Area;
            //TODO choose upper and lower bounds correctly
            if(x[0] <= y0hat(model.thickness*0.9) && x[0] >= y0hat(model.thickness) && x[2] <= y2hat(model.thickness*0.9) && x[2] >= y2hat(model.thickness)){
                for(int i = 0; i < 3; i++)
                    h[i] = -running_moment*normal[i];
            }
            if(x[0] <= y0hat(0) && x[0] >= y0hat(model.thickness*.1) && x[2] <= y2hat(0) && x[2] >= y2hat(model.thickness*.1)){
                for(int i = 0; i < 3; i++)
                    h[i] = running_moment*normal[i];
            }
            return;
        }
        //apply a pressure if there is no multi point constraint on the limb
        if(config.get<int>("mpc",0)==0){
            if( fabs(x[0] - xval) < 1e-06 && x[2] > model.radius+model.thickness - 1e-06){
                for (int i = 0; i < 3; i++)
                    h[i] = p_p * normal[i];
            }
            return;
        }
        //or otherwise apply a moment to the limb
        if(x[2] > model.radius+model.thickness+model.limb - 1e-6){
            //std::cout << "applying moment" <<std::endl;
            if(x[1] > resin_treatment + 1e-06  && x[1] < model.Y-resin_treatment-1e-06){
                if(fabs(x[0] - xval) < model.thickness*0.1){
                    for(int i = 0; i < 3; i++)
                        h[i] = -p_m*normal[i];
                }
                if(fabs(x[0] - (xval+model.thickness)) < model.thickness*0.1){
                    for(int i = 0; i < 3; i++)
                        h[i] = p_m*normal[i];
                }
            }
            return;
        }
    }
 
    private :

    const GV& gv;
    const GV2& gv2;
    const typename GV::IndexSet& is;
    std::vector<Dune::FieldMatrix<RF,6,6>> Cijkl;
    std::vector<Dune::FieldMatrix<RF,6,6>> Cijkl_strain;
    //std::vector<Dune::FieldMatrix<RF,6,6>> Cijkl_local;
    std::vector<RF> density;
    MODEL model;
    std::vector<std::vector<double>> theta;
    std::vector<std::function<double(Dune::FieldVector<double,3>)>> thetaFct;
    std::vector<int> lay_id;
    std::vector<int> geometryType;
    std::vector<int> materialType;
    std::vector<Dune::FieldVector<RF,6>> eps;
    int solver_type;
    int maxIt;
    int verbosity;
    int dofel;
    int intorder;
    bool parallel;
    double tolerance;
    COEFF myDefect;

};


#endif
