#ifndef Cal_Cijkl_hh
#define Cal_Cijkl_hh

// Cijkl is a class which computes Elasticity Tensor on each element and contains all required functions

#include "../MathFunctions/transpose.hh"


Dune::FieldMatrix<double,6,6> Cijkl(Dune::Composites::Material& material){


    Dune::FieldMatrix<double,6,6> C(0.0); // Initialise Tensor as Zeros


    int type = material.Type;

    std::cout << "Material Type = " << type << std::endl;

    if (type == 0){ // Isotropic

        double E = material.matprop[0];
        double nu = material.matprop[1];

        double tmp = E / ( (1.0 + nu) * ( 1.0 - 2.0 * nu ) );
        double G = E / (2.0 * (1 + nu));

         C[0][0] = ( 1.0 - nu ) * tmp;    C[0][1] = nu * tmp; C[0][2] = nu * tmp;
         C[1][0] = nu * tmp; C[1][1] = ( 1.0 - nu ) * tmp; C[1][2] = nu * tmp;
         C[2][0] = nu * tmp; C[2][1] = nu * tmp; C[2][2] = ( 1.0 - nu ) * tmp;
         C[3][3] = G;
         C[4][4] = G;
         C[5][5] = G;

    }

    if (type == 1){ // Composite

        double E1 = material.matprop[0];
        double E2 = material.matprop[1];
        double E3 = material.matprop[2];
        double nu12 = material.matprop[3];
        double nu21 = nu12 * (E2 / E1);
        double nu13 = material.matprop[4];
        double nu31 = nu13 * (E3 / E1);
        double nu23 = material.matprop[5];
        double nu32 = nu23 * (E3 / E2);
        double G12 = material.matprop[6];
        double G13 = material.matprop[7];
        double G23 = material.matprop[9];

        Dune::FieldMatrix<double,3,3> Sinv = {{1./E1,-nu21/E2,-nu31/E3},{-nu12/E1,1./E2,-nu32/E3},{-nu13/E1,-nu23/E2,1./E3}}; 
        Sinv.invert();

        C[0][0] = Sinv[0][0];
        C[1][1] = Sinv[1][1];
        C[2][2] = Sinv[2][2];
        C[0][1] = Sinv[0][1]; C[1][0] = Sinv[1][0];
        C[0][2] = Sinv[0][2]; C[2][0] = Sinv[2][0];
        C[1][2] = Sinv[1][2]; C[2][1] = Sinv[2][1];
        C[3][3] = G23;
        C[4][4] = G13;
        C[5][5] = G12;

    }
    
    return C;

}

Dune::FieldMatrix<double,6,6> getMatrix(Dune::FieldMatrix<double,3,3> A) {

        Dune::FieldMatrix<double,6,6> R;

        R[0][0] = A[0][0]*A[0][0]; R[0][1] = A[0][1]*A[0][1]; R[0][2] = A[0][2]*A[0][2];
        R[1][0] = A[1][0]*A[1][0]; R[1][1] = A[1][1]*A[1][1]; R[1][2] = A[1][2]*A[1][2];
        R[2][0] = A[2][0]*A[2][0]; R[2][1] = A[2][1]*A[2][1]; R[2][2] = A[2][2]*A[2][2];

        R[0][3] = 2.0*A[0][1]*A[0][2]; R[0][4] = 2.0*A[0][0]*A[0][2]; R[0][5] = 2.0*A[0][0]*A[0][1];
        R[1][3] = 2.0*A[1][1]*A[1][2]; R[1][4] = 2.0*A[1][0]*A[1][2]; R[1][5] = 2.0*A[1][0]*A[1][1];
        R[2][3] = 2.0*A[2][1]*A[2][2]; R[2][4] = 2.0*A[2][0]*A[2][2]; R[2][5] = 2.0*A[2][0]*A[2][1];

        R[3][0] = A[1][0]*A[2][0]; R[3][1] = A[1][1]*A[2][1]; R[3][2] = A[1][2]*A[2][2];
        R[4][0] = A[0][0]*A[2][0]; R[4][1] = A[0][1]*A[2][1]; R[4][2] = A[0][2]*A[2][2];
        R[5][0] = A[0][0]*A[1][0]; R[5][1] = A[0][1]*A[1][1]; R[5][2] = A[0][2]*A[1][2];
    
        R[3][3] = A[1][1]*A[2][2]+A[1][2]*A[2][1]; R[3][4] = A[1][0]*A[2][2]+A[1][2]*A[2][0]; R[3][5] = A[1][0]*A[2][1]+A[1][1]*A[2][0];
        R[4][3] = A[0][1]*A[2][2]+A[2][1]*A[0][2]; R[4][4] = A[0][0]*A[2][2]+A[2][0]*A[0][2]; R[4][5] = A[2][0]*A[0][1]+A[2][1]*A[0][0];
        R[5][3] = A[0][1]*A[1][2]+A[0][2]*A[1][1]; R[5][4] = A[0][0]*A[1][2]+A[0][2]*A[1][0]; R[5][5] = A[0][0]*A[1][1]+A[0][1]*A[1][0];

        return R;

}




#endif
