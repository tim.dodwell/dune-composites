// main driver function for FEM Calculations
#ifndef ELASTICITY_DRIVER_HH
#define ELASTICITY_DRIVER_HH

#include "localOperator/linearelasticity.hh"
#include "../MathFunctions/L2norm.hh"
#include "../PostProcessing/calculateStresses.hh"

template <typename PROBLEM,typename MODEL, typename GV1, typename GFS, typename GRID, typename IF, int dofel, typename FEM, typename CON>
void driver(FEM &fem, const GFS &gfs, PROBLEM& problem, const GV1& gv1, const GRID& grid, const IF& initial_solution, CON& con, Dune::MPIHelper& helper,MODEL& model, double cells, std::vector<double>& times)
{
    std::cout << "Starting Driver!" << std::endl;
    Dune::Timer timer;

	double overlap = config.get<int>("overlap",1);
    typedef double RF;

	using GV = Dune::PDELab::AllEntitySet<GV1>;
	auto gv = GV(gv1);

    typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::none,1> Scalar_VectorBackend;

	// Make constraints map and initialize it from a function
	typedef typename GFS::template ConstraintsContainer<RF>::Type C;
	C cg, cg_ext;
	cg.clear(); cg_ext.clear();

	typedef Scalar_BC<GV,MODEL,RF> BC;
	BC U1_cc(gv,model,0), U2_cc(gv,model,1), U3_cc(gv,model,2);
	U1_cc.setDof(1);
	U2_cc.setDof(2);
	U3_cc.setDof(3);

	typedef Dune::PDELab::CompositeConstraintsParameters<BC,BC,BC> Constraints;
	Constraints constraints(U1_cc,U2_cc,U3_cc);

	Dune::PDELab::constraints(constraints,gfs,cg);

	//file << "Time to initialise driver " << timer.elapsed() << "\n";
	times.push_back(timer.elapsed());
    timer.reset();

	//	Construct Linear Operator on FEM Space
	typedef Dune::PDELab::linearelasticity<GV, PROBLEM,dofel> LOP;
	LOP lop(gv, problem, problem.getIntOrder(),config.get<double>("eps",100));

	typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
	typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,C,C> GO;
	int non_zeros = 81;
    if(dofel == 8*3) non_zeros = 27;
    if(dofel == 27*3) non_zeros = 125;
    MBE mbe(non_zeros); // Maximal number of nonzeroes per row can be cross-checked using buildpattern below
	GO go(gfs,cg,gfs,cg,lop,mbe);

    //using Dune::PDELab::Backend::native;
    //typedef typename GO::Traits::Jacobian J;
    //J jac(go);
    //auto ss =  mbe.buildPattern<GO,J>(go,jac);
    //std::cout << ss[0];

	//file << "Time to set up matrix " << timer.elapsed() << "\n";
	times.push_back(timer.elapsed());
	timer.reset();

    // Make coefficent vector and initialize it from a function
	typedef Dune::PDELab::Backend::Vector<GFS,double> V;
	V x0(gfs,0.0);
	Dune::PDELab::interpolate(initial_solution,gfs,x0);

    // Set non constrained dofs to zero
    Dune::PDELab::set_shifted_dofs(cg,0.0,x0);

	//file << "Time to finish interpolation:" << timer.elapsed() << "\n";
	times.push_back(timer.elapsed());
	timer.reset();

    Dune::Timer timer_solver;
    if (problem.isParallel())
	{
        if(config.get<bool>("yaspgrid",true)){ //overlapping solvers can only be used for yaspgrid
            if (problem.getSolver() == 1){ //1-level Schwartz
                typedef Dune::PDELab::ISTLBackend_CG_AMG_SSOR<GO> PAR_CG_AMG_SSOR;
                PAR_CG_AMG_SSOR ls(gfs,problem.getMaxIt(),problem.getVerbosity());
                Dune::PDELab::StationaryLinearProblemSolver<GO,PAR_CG_AMG_SSOR,V> slp(go,ls,x0,problem.getTol());
                slp.apply();
            }
            if (problem.getSolver() == 2){ //1-level Schwartz
#if HAVE_SUITESPARSE
                typedef Dune::PDELab::ISTLBackend_OVLP_CG_UMFPack<GFS, C> PAR_UMF;  //CG with UMFPack
                PAR_UMF ls(gfs,cg,problem.getMaxIt(),problem.getVerbosity());
                Dune::PDELab::StationaryLinearProblemSolver<GO,PAR_UMF,V> slp(go,ls,x0,problem.getTol());
                slp.apply();
#else
                std::cout << "Solver not available" << std::endl;
                return;
#endif
            }
            if (problem.getSolver() == 3){ //1-level Schwartz
#if HAVE_SUPERLU
                typedef Dune::PDELab::ISTLBackend_OVLP_CG_SuperLU<GFS, C> PAR_LU;  //CG with superLU
                PAR_LU ls(gfs,cg,problem.getMaxIt(),problem.getVerbosity());
                Dune::PDELab::StationaryLinearProblemSolver<GO,PAR_LU,V> slp(go,ls,x0,problem.getTol());
                slp.apply();
#else
                std::cout << "Solver not available" << std::endl;
                return;
#endif
            }
            if (problem.getSolver() == 4) {
                typedef Dune::PDELab::ISTLBackend_OVLP_GMRES_ILU0<GFS,C> OVLP_GMRES_LS;
                OVLP_GMRES_LS ls(gfs,cg,problem.getMaxIt(),problem.getVerbosity());
                Dune::PDELab::StationaryLinearProblemSolver<GO,OVLP_GMRES_LS,V> slp(go,ls,x0,problem.getTol());
                slp.apply();
            }
        }
        else{ //For ALUgrid/UGgrid using non-overlapping solvers
            if(problem.getSolver() == 1){
                typedef Dune::PDELab::ISTLBackend_NOVLP_CG_AMG_SSOR<GO> NOVLP_AMG;
                NOVLP_AMG ls(go,problem.getMaxIt(),problem.getVerbosity());
                Dune::PDELab::StationaryLinearProblemSolver<GO,NOVLP_AMG,V> slp(go,ls,x0,problem.getTol());
                slp.apply();
            }
            else    std::cout << "Not implemented for UG/ALU Grid" << std::endl;
        }
    }
	else
	{
		if (problem.getSolver() == 1) {
			typedef Dune::PDELab::ISTLBackend_SEQ_CG_AMG_SSOR<GO> SEQ_CG_AMG_SSOR;
			SEQ_CG_AMG_SSOR ls(problem.getMaxIt(),problem.getVerbosity());
			Dune::PDELab::StationaryLinearProblemSolver<GO,SEQ_CG_AMG_SSOR,V> slp(go,ls,x0,problem.getTol());
			slp.apply();
		}
		if (problem.getSolver() == 2){
#if HAVE_SUITESPARSE
			typedef Dune::PDELab::ISTLBackend_SEQ_UMFPack SEQ_UMF;
			SEQ_UMF ls(problem.getVerbosity()); //Direct solver
			Dune::PDELab::StationaryLinearProblemSolver<GO, SEQ_UMF,V> slp(go,ls,x0,problem.getTol());
			slp.apply();
#else
        std::cout << "Solver not available" << std::endl;
        return;
#endif
		}
        if (problem.getSolver() == 3){
#if HAVE_SUPERLU
            typedef Dune::PDELab::ISTLBackend_SEQ_SuperLU SEQ_LU;
            SEQ_LU ls(problem.getVerbosity()); //Direct solver
            Dune::PDELab::StationaryLinearProblemSolver<GO, SEQ_LU,V> slp(go,ls,x0,problem.getTol());
            slp.apply();
#else
            std::cout << "Solver not available" << std::endl;
            return;
#endif
        }
        if (problem.getSolver() == 4){
            typedef Dune::PDELab::ISTLBackend_SEQ_LOOP_Jac SEQ_LOOP_JAC_LS;
            SEQ_LOOP_JAC_LS ls(problem.getMaxIt(),problem.getVerbosity());
            Dune::PDELab::StationaryLinearProblemSolver<GO,SEQ_LOOP_JAC_LS,V> slp(go,ls,x0,problem.getTol());
            slp.apply();
        }
    }
    if (problem.getSolver() == 5){ //2-level Schwartz
        assert(config.get<bool>("yaspgrid",true));
#if HAVE_SUITESPARSE
        //Neumann boudary conditions between processors
        if (configuration.get<bool>("EVboundariesFromProblem", ""))
            Dune::PDELab::constraints_exterior(constraints,gfs,cg_ext);
        auto cc_bnd_neu_int_dir = C();
        Dune::PDELab::PureNeumannBoundaryCondition pnbc;
        cc_bnd_neu_int_dir.clear();
        Dune::PDELab::constraints(pnbc,gfs,cc_bnd_neu_int_dir);

        //Set up Grid operator
        typedef typename GO::Jacobian J;

        //Matrix with "correct" boundary conditions
        GO go(gfs,cg,gfs,cg,lop,mbe);
        J AF(go);
        AF = 0.0;
        go.jacobian(x0,AF);

        //Matrix with pure neumann boundary conditions
        GO go_neu(gfs,cg_ext,gfs,cg_ext,lop,mbe);
        J AF_neu(go_neu);
        AF_neu = 0.0;
        go_neu.jacobian(x0,AF_neu);

        //Create local operator on overlap
        typedef Dune::PDELab::LocalOperatorOvlpRegion<LOP, GFS> LOP_ovlp;
        LOP_ovlp lop_ovlp(lop,gfs);
        typedef Dune::PDELab::GridOperator<GFS,GFS,LOP_ovlp,MBE,RF,RF,RF,C,C> GO_ovlp;
        GO_ovlp go_ovlp(gfs,cg_ext,gfs,cg_ext,lop_ovlp,mbe);
        typedef typename GO_ovlp::Jacobian J2;
        J2 AF_ovlp(go_ovlp);
        AF_ovlp = 0.0;
        go_ovlp.jacobian(x0,AF_ovlp);

        //Set up solution vector and some necessary operators
        typedef Dune::PDELab::OverlappingOperator<C,J,V,V> POP;
        POP popf(cg,AF);
        typedef Dune::PDELab::istl::ParallelHelper<GFS> PIH;
        PIH pihf(gfs);
        typedef Dune::PDELab::OverlappingScalarProduct<GFS,V> OSP;
        OSP ospf(gfs,pihf);

        //Preconditioner
        double eigenvalue_threshold = configuration.get<double>("threshhold",0.2)*(double)overlap/(cells+overlap); //eigenvalue threshhold
        if(helper.rank() == 0) std::cout << "Eigenvalue threshhold: " << eigenvalue_threshold << std::endl;
        int verb = 0;
        if (gfs.gridView().comm().rank()==0) verb=problem.getVerbosity();
        typedef Dune::PDELab::LocalFunctionSpace<GFS, Dune::PDELab::AnySpaceTag> LFSU;
        typedef typename LFSU::template Child<0>::Type LFS;
        LFSU lfsu(gfs);
        LFS lfs = lfsu.template child<0>();

        //file << "Time for geneo setup:" << timer.elapsed() << "\n";
        times.push_back(timer.elapsed());
        timer.reset();

        std::shared_ptr<V> part_unity;
        if (configuration.get<bool>("widlund_part_unity",false))
            part_unity = sarkisPartitionOfUnity<3,V>(gfs, lfs, cc_bnd_neu_int_dir);
        else
            part_unity = standardPartitionOfUnity<3,V>(gfs, lfs, cc_bnd_neu_int_dir);


        //file << "Time to setup partition of unity:" << timer.elapsed() << "\n";
        times.push_back(timer.elapsed());
        timer.reset();

        V dummy(gfs, 1);
        double norm = dummy.two_norm();
        Dune::PDELab::set_constrained_dofs(cg_ext,0.0,dummy); // Zero on subdomain boundary
        bool symmetric = (norm == dummy.two_norm());

        int nev = configuration.get<int>("eigenvectors",1);
        int nev_arpack = configuration.get<int>("eigenvectors_compute",-1);

        std::shared_ptr<SubdomainBasis<V> > subdomain_basis;
        SubdomainBasis<V> subdomain_basis2;
        if (nev > 0)
            subdomain_basis = std::make_shared<GenEOBasis<GFS,J,V,V,3> >(gfs, AF_neu, AF_ovlp, eigenvalue_threshold, *part_unity, nev, nev_arpack, configuration.get<double>("sigma",0.01),subdomain_basis2);
        else if (nev == 0){
            subdomain_basis = std::make_shared<ZEMBasis<GFS,LFS,V,3,3> >(gfs, lfs, *part_unity);
            subdomain_basis2 = *subdomain_basis;
        }
        else{
            subdomain_basis = std::make_shared<PartitionOfUnityBasis<V> >(*part_unity);
            subdomain_basis2 = *subdomain_basis;
        }


        //file << "Time to setup multiscale space:" << timer.elapsed() << "\n";
        times.push_back(timer.elapsed());
        timer.reset();

        auto partunityspace = std::make_shared<SubdomainProjectedCoarseSpace<GFS,J,V,V,3> >(gfs, AF_neu, subdomain_basis, verb);
        auto partunityspace2 = std::make_shared<SubdomainProjectedCoarseSpace<GFS,J,V,V,3> >(gfs, AF_neu, std::make_shared<SubdomainBasis<V>>(subdomain_basis2), verb);
        typedef TwoLevelOverlappingAdditiveSchwarz<GFS,J,V,V> PREC;
        auto prec = std::make_shared<PREC>(gfs, AF, partunityspace, partunityspace2);


        //file << "Time to create preconditioner:" << timer.elapsed() << "\n";
        times.push_back(timer.elapsed());
        timer.reset();

        // set up and assemble right hand side w.r.t. l(v)-a(u_g,v)
        V d(gfs,0.0);
        go.residual(x0,d);

        // now solve defect equation A*v = d
        V v(gfs,0.0);
        //Solve using CG
        if(!config.get<bool>("CoarseExclusiveSolve",false)){
            if (configuration.get<std::string>("criterion","error") == "error") {
                V ref_soln = x0;
                V v_ref = v;
                V d_ref = d;


                auto solver_ref = std::make_shared<Dune::CGSolverFork<V> >(popf,ospf,*prec,1E-14,5000,verb,true);
                Dune::InverseOperatorResult result;
                solver_ref->apply(v_ref,d_ref,result);

                ref_soln -= v_ref;

                auto solver = std::make_shared<Dune::CGSolverForkInfNorm<V> >(popf,ospf,*prec,problem.getTol(),problem.getMaxIt(),verb);
                solver->apply(v,d,v_ref,ref_soln,result);
            }
            else{
                auto solver = std::make_shared<Dune::CGSolverFork<V> >(popf,ospf,*prec,problem.getTol(),problem.getMaxIt(),verb,true);
                //Dune::RestartedGMResSolver<V> solver(popf,ospf,*prec,problem.getTol(),500, problem.getMaxIt(), verb);
                Dune::InverseOperatorResult result;
                solver->apply(v,d,result);
                //file << "Time to solve " << timer.elapsed() <<  "\n";
                times.push_back(timer.elapsed());
                timer.reset();
            }
            x0 -= v;
        }
        else{  //GENEO coarse space as solution
            //Calculate solution in coarse space
            if(helper.rank()==0) std::cout << "Coarse Space only" << std::endl;
            //Step 1: Calculate a reference solution
            V ref_soln = x0;
            V v_ref = v;
            V d_ref = d;

            auto solver_ref = std::make_shared<Dune::CGSolverFork<V> >(popf,ospf,*prec,problem.getTol(),problem.getMaxIt(),verb,true);
            Dune::InverseOperatorResult result;
            solver_ref->apply(v_ref,d_ref,result);

            ref_soln -= v_ref;

            if(helper.rank() == helper.size()-1){
                std::cout << "Reference L_infty Norm : " << ref_soln.infinity_norm() << std::endl;
            }

            //Step 2: Calculate the coarse solution
            typedef typename PREC::COARSE_V COARSE_V;
            typedef typename PREC::COARSE_M COARSE_M;
            std::shared_ptr<COARSE_V> coarse_defect = partunityspace->restrict_defect (d);
            COARSE_V v0(partunityspace->basis_size(),partunityspace->basis_size());

            Dune::UMFPack<COARSE_M> coarse_solver (*partunityspace->get_coarse_system());
            coarse_solver.apply(v0,*coarse_defect,result);

            auto fine_correction = partunityspace->prolongate_defect (v0);

            Dune::PDELab::AddDataHandle<GFS,V> result_addh(gfs,*fine_correction);
            gfs.gridView().communicate(result_addh,Dune::All_All_Interface,Dune::ForwardCommunication);

            x0 -= *fine_correction;

            // Calculate error norm
            auto err = x0;
            err -= ref_soln;

            //file << "Time to solve with multiscale space:" << timer.elapsed() << "\n";
            times.push_back(timer.elapsed());
            timer.reset();


            if(config.get<bool>("vtkoutput",false)){
                Dune::SubsamplingVTKWriter<GV1> vtkwriter(gv1,0);
                Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,ref_soln);
                vtkwriter.write("multiout",Dune::VTK::appendedraw);
            }
            if(config.get<bool>("vtkoutput",false)){
                Dune::SubsamplingVTKWriter<GV1> vtkwriter(gv1,0);
                Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,err);
                vtkwriter.write("error",Dune::VTK::appendedraw);
            }

            double error_norm_squared = L2norm<GFS,GV,V>(gfs,gv,err);
            double ref_norm_squared = L2norm<GFS,GV,V>(gfs,gv,ref_soln);

            double ref_norm_squared_global = gfs.gridView().comm().sum(ref_norm_squared);
            double error_norm_squared_global = gfs.gridView().comm().sum(error_norm_squared);
            double error_norm = std::sqrt(error_norm_squared_global) / std::sqrt(ref_norm_squared_global);

            MPI_Barrier(gfs.gridView().comm());
            if(helper.rank() == helper.size()-1){
                std::cout << "Error L2 Norm : " << error_norm << std::endl;
            }
        }
#else
        std::cout << "Solver not available" << std::endl;
        return;
#endif
    }
    // Apply linear solver
    //file << "Total time to Solve:" << timer_solver.elapsed() << "\n",vtk_output;
	times.push_back(timer_solver.elapsed());


    calculateStresses<V,GV1,GFS,MBE, PROBLEM,dofel>(x0,gv1,gfs,mbe, problem, helper);
 }

#endif
