 
#ifndef DUNE_GENEO_PARTITIONOFUNITYBASIS_HH
#define DUNE_GENEO_PARTITIONOFUNITYBASIS_HH

#include "subdomainbasis.hh"

template<class X>
class PartitionOfUnityBasis : public SubdomainBasis<X>
{

public:
  PartitionOfUnityBasis(X& part_unity) {
    this->local_basis.resize(1);
    this->local_basis[0] = std::make_shared<X>(part_unity);
  }

};

#endif //DUNE_GENEO_PARTITIONOFUNITYBASIS_HH
