// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_PDELAB_FINITEELEMENTMAP_SERENDIPITYFEM_HH
#define DUNE_PDELAB_FINITEELEMENTMAP_SERENDIPITYFEM_HH

#include <cstddef>

#include "serendipity.hh"
#include <dune/pdelab/finiteelementmap/finiteelementmap.hh>

namespace Dune {
  namespace PDELab {

    //! wrap up element from local functions
    //! \ingroup FiniteElementMap
    template<typename GV, typename D, typename R, std::size_t k>
    class SerendipityLocalFiniteElementMap
      : public SimpleLocalFiniteElementMap< Dune::SerendipityLocalFiniteElement<D,R,GV::dimension,k> >
    {

    public:

      SerendipityLocalFiniteElementMap(const GV& gv)
      {}

      bool fixedSize() const
      {
        return true;
      }

      bool hasDOFs(int codim) const
      {
        switch(k)
          {
          case 1:
            return codim == GV::dimension;  //only nodal degrees of freedom
          case 2:
            if (GV::dimension != 2 && GV::dimension != 3){
              DUNE_THROW(NotImplemented,"SerendipityLocalFiniteElementMap with k = 2 is only implemented for d = 2,3");
            }
            else{
              if(codim == GV::dimension || codim == (GV::dimension-1)){ //nodes and edges
                return true;
              }
              return false;
            }

          default:
            DUNE_THROW(NotImplemented,"SerendipityLocalFiniteElementMap is only implemented for k <= 2");
          }
      }

      std::size_t size(GeometryType gt) const
      {
        switch (k)
          {
          case 1:
            return gt.isVertex() ? 1 : 0;
          case 2:
            {
              if (GV::dimension != 2 && GV::dimension != 3)
                DUNE_THROW(NotImplemented,"SerendipityLocalFiniteElementMap with k = 2 is only implemented for d = 2,3");
              // Q2 simply attaches a single DOF to each node and edge
              return (gt.isVertex() || gt.isLine() ) ? 1 : 0;
            }
          default:
            DUNE_THROW(NotImplemented,"SerendipityLocalFiniteElementMap is only implemented for k <= 2");
          }
      }

      std::size_t maxLocalSize() const
      {
        const auto d = GV::dimension;
        if(d == 1){
          return k+1;
        }
        if(d == 2){
          return 2*(k+1)+2*(k-1);
        }
        if(d == 3){
          return 4*(k+1)+8*(k-1);
        }
      }

    };

  }
}

#endif // DUNE_PDELAB_FINITEELEMENTMAP_SERENDIPITYFEM_HH
