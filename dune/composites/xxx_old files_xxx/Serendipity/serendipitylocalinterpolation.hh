// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOCALFUNCTIONS_SERENDIPITYLOCALINTERPOLATION_HH
#define DUNE_LOCALFUNCTIONS_SERENDIPITYLOCALINTERPOLATION_HH

#include <dune/common/fvector.hh>
#include <dune/common/power.hh>

#include <dune/geometry/type.hh>

#include <dune/localfunctions/common/localbasis.hh>
#include <dune/localfunctions/common/localfiniteelementtraits.hh>


namespace Dune
{
  /** \todo Please doc me! */
  template<int k, int d, class LB>
  class SerendipityLocalInterpolation
  {

    typedef typename Dune::FieldVector<int,d> DuneVector;
    static void multiindex (DuneVector& alpha)
    {
	    int i = 0;
	    for (int j=0; j<d; j++){
		    i+=alpha[j]*pow(k+1,j);
	    }
	    i++;
	    for (int j=0; j<d; j++)
	    {
		    alpha[j] = i % (k+1);
		    i = i/(k+1);
	    }
	    unsigned int sum = 0;
	    for (unsigned int j=0; j<d; j++){
          if(alpha[j] > 0 && alpha[j] < k){ sum++; }
	    }
	    if(sum > 1){
          multiindex(alpha);
	    }
    }

    //! \brief number of shape functions
    unsigned int size () const
    {
      if(d == 1){
        return k+1;
      }
      if(d == 2){
        return 2*(k+1)+2*(k-1);
      }
      if(d == 3){
        return 4*(k+1)+8*(k-1);
      }
    }

  public:

    //! \brief Local interpolation of a function
    template<typename F, typename C>
    void interpolate (const F& f, std::vector<C>& out) const
    {
      typename LB::Traits::DomainType x;
      typename LB::Traits::RangeType y;

      out.resize(size());
      DuneVector alpha(0);
      for (unsigned int i=0; i<size(); i++)
      {
        // convert index i to multiindex
        if(i>0){ multiindex(alpha); }

        // Generate coordinate of the i-th Lagrange point
        for (int j=0; j<d; j++)
          x[j] = (1.0*alpha[j])/k;

        f.evaluate(x,y);
        out[i] = y;
      }
    }
  };

  /** \todo Please doc me! */
  template<int d, class LB>
  class SerendipityLocalInterpolation<0,d,LB>
  {
  public:
    //! \brief Local interpolation of a function
    template<typename F, typename C>
    void interpolate (const F& f, std::vector<C>& out) const
    {
      typename LB::Traits::DomainType x(0);
      typename LB::Traits::RangeType y;
      f.evaluate(x,y);
      out.resize(1);
      out[0] = y;
    }
  };

}


#endif
