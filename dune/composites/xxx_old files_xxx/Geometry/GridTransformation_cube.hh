#include "RandomFields/integrator.hh"
#include "RandomFields/defectGenerator.hh"

#ifndef GRIDTRANSFORMATION__HH
#define GRIDTRANSFORMATION__HH

template<typename DomainVector, typename RangeVector,class MODEL>
class GeometryFct{
    public:
        GeometryFct(const MODEL& model_, COEFF myDefect_) : model(model_),  myDefect(myDefect_)
    {
    }

        RangeVector returnY(const DomainVector& x) const{
            double theta;
            return returnY(x,theta);
        }

        RangeVector returnY(const DomainVector& x, double& theta) const{
            RangeVector y = defect(x);
            return y;
        }

        double returnTheta(const DomainVector& x) const{
            RangeVector y,yph;
            double theta,thetaph;
            double h = 1e-10;
            RangeVector xph = x; xph[0] += h/2.;
            RangeVector xmh = x; xmh[0] -= h/2.;
            y = returnY(xmh,theta);
            yph = returnY(xph,thetaph);
            auto angle = atan2((yph[2]-y[2])/h,(yph[0]-y[0])/h);
            return angle;
        }


    private:

        RangeVector defect(const DomainVector &x) const{
            //Get defect data from ini file
            double defect_size = config.get<double>("defect_size",0);
            RangeVector y = x;
            if(defect_size>0){
                //int wavelength = config.get<int>("wavelength",7);
                Dune::FieldVector<double,3> damping = {config.get<double>("dampingX",1),config.get<double>("dampingY",1),config.get<double>("dampingZ",1)};
                Dune::FieldVector<double,3> defect_location = {config.get<double>("defect_locationX",model.radius/2.+model.limb),
                    config.get<double>("defect_locationY",model.Y/2.),
                    config.get<double>("defect_locationZ",model.thickness/2)};
                //Define defect function
                if(config.get<std::string>("defect_type","simple") == "simple"){
                    y[2] += defect_size*cos(M_PI*config.get<double>("frequency",4)*x[0])
                        * 1./pow(cosh(damping[0] * M_PI * (defect_location[0] - x[0])/model.limb),2.)
                        * 1./pow(cosh(damping[1] * M_PI * (defect_location[1] - x[1])/model.Y),2.)
                        * 1./pow(cosh(damping[2] * M_PI * (defect_location[2] - x[2])/model.thickness),2.);
                }
                if(config.get<std::string>("defect_type","simple") == "MC"){
                    // In curvy bit
                    double xval = (x[0]-model.limb)/model.radius * config.get<double>("LengthX"); // xval goes from 0 to 346

                    double pix = 5.0/58.0;
                    xval = xval*pix; // xval converted to mm


                    auto xd = std::log(1.0/98); // because the processed image is 98 pixels tall
                    auto xdamp = std::exp(pow((x[0] - defect_location[0]),2) / (pow((model.radius/2.0),2) / xd));

                    auto zd = std::log(1.0/98.0);
                    auto zdamp = std::exp(x[2]*x[2] / (pow((model.thickness),2) / zd)); // this one is different because the wrinkle is located at the inner radius.

                    auto yd = std::log(1.0 /98.0);
                    auto ydamp = std::exp(pow((x[1] - defect_location[1]),2) / (pow(26,2) / yd));

                    for (int i = 0; i < myDefect.getN(); i++){                  

                        y[2] += defect_size * myDefect.evalPhi(xval,i) * myDefect.getxi(i) * xdamp * ydamp * zdamp;

                    }
                }
            }
            return y;
        }

        const MODEL& model;
        COEFF myDefect;

};


//Grid Transformation
template <int dim, class MODEL>
class GridTransformation
: public Dune :: AnalyticalCoordFunction< double, dim, dim, GridTransformation <dim,MODEL> >
{
  typedef GridTransformation This;
  typedef Dune :: AnalyticalCoordFunction< double, dim, dim, This > Base;

public:
  typedef typename Base :: DomainVector DomainVector;
  typedef typename Base :: RangeVector RangeVector;

  GridTransformation(MODEL& model_, int my_rank_,COEFF myDefect_) : model(model_), giveOutput(1), my_rank(my_rank_), myDefect(myDefect_){  }

  void evaluate ( const DomainVector &x, RangeVector &y ) const
  {
      GeometryFct<DomainVector,RangeVector,MODEL> g(model,myDefect);
      y = g.returnY(x);
  }
private:
  COEFF myDefect;
  mutable int giveOutput;
  MODEL model;
  int my_rank;
};

#endif
