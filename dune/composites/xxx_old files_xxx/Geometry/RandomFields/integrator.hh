#include <dune/geometry/quadraturerules.hh>
#include <math.h>

#ifndef INTEGRATOR_HH
#define INTEGRATOR_HH

template<typename DEF>
double integrator(double xval, DEF myDefect){
    double y = 0.0;

    for (int i = 0; i < myDefect.getN(); i++){
        y += std::sqrt(myDefect.getLam1Dx(i)) * myDefect.evalPhi(xval,i) * myDefect.getxi(i);
    }

    return tan(y);
}

#endif
