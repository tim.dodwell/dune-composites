#include <random>
#include "KLFunctions.hh"
#include "general.hh"
#include "readData.hh"

#ifndef COEFF_HH
#define COEFF_HH

// Class which constructs
class COEFF
{
public:
    // Constructor for COEFF class
    COEFF(){
        if(config.get<std::string>("defect_type","simple")=="MC"){
        //: L(LengthX), N(numKLmodes), sigKL(sdRandomField), ellx(correlationLength)
        L = config.get<double>("LengthX");
        N = config.get<int>("numKLmodes");
        //sigKL = config.get<double>("sdRandomField");
        ellx = config.get<double>("correlationLength");
        /*std::cout << "LengthX " << L << std::endl;
        std::cout << "KLmodes " << N << std::endl;
        std::cout << "corr " << sigKL << std::endl;
        std::cout << "sd " << ellx << std::endl;*/
        freqx.resize(N);  // std::vector<double> - contain N eigenfrequencies associate with N largest eigenvalues of covariance kernel
        lam1Dx.resize(N); // std::vector<double> - contain N largest eigenvalues of covariance kernel
        rootFinder(N,ellx,freqx); // Find eigenfrequencies
        evaluate_eigenValues(ellx,lam1Dx,freqx); // Evaluates eigenvalues
        xi.resize(N);
        }
        //std::cout << "construct COEFF " << std::endl;
    };


    double inline evalPhi(double x, int i) const{
        double phi = 0.0;
        double omega;
	// x *= 58 / 5; // to convert back to pixels from mm
	double l = 0.0;
	double LL = 0.0; // L in mm stored as LL
	LL = L * 5.0 / 58.0;
	l = ellx/L; // ellx is in pixels so its normalized by L in pixels

	x /= LL; // x is normalized such that x \in [0,1]
	omega = freqx[i];
	double nm = std::sin(2.0*omega)*(0.25*(l*l*omega - 1.0/omega)) - (0.5 * l * std::cos(2.0 * omega)) + (0.5 * ( 1 + l + l*l*omega*omega));
	
	double norm = 1.0 / std::sqrt(L * nm);

        phi = norm * (std::sin(x * omega) + (l * omega * std::cos(x * omega)) );
	return phi;
    }

    double inline getxi(int i) const{
      return xi[i];
    }


    int inline getN() const{
        return N;
    }

  double inline getFreq(int i) const{
    return freqx[i];
  }


  double inline getLam1Dx(int i) const{
    return lam1Dx[i];
  }

  // void inline user_random_field_normal()
  // {
  //   std::random_device rd;
  //   std::mt19937 gen(rd());
  //   std::normal_distribution<double> randn(0.0,1.0);
  //   std::fill(xi.begin(), xi.end(), 0.0);
  //   for (int j = 0; j < N; j++){
  //     xi[j] = randn(gen);
  //   }

  // } // end user_random_field

  void inline user_random_field()
  {
      if(config.get<std::string>("defect_type","simple")=="MC"){
          //Sample number TODO
          int sn = config.get<int>("sampleN");
          auto r = readData(config.get<std::string>("filename","RandomFieldCoefficient/coeffs.txt"));
          for (int i=0; i<N; i++){
              xi[i] = r[sn][i];
          }
          // std::cout <<  "Xi: " << std::endl;
          // for(int i =0; i < N; i++)
          //   std::cout << xi[i] << std::endl;
      }
  } // end user_random_field


private:

    int N;
    double L, ellx, sigKL;
    std::vector<double> xi;
    std::vector<double> freqx, lam1Dx;

};

#endif
