#ifndef GEOMETRY_INFO_HH
#define GEOMETRY_INFO_HH

//#include "readCsv.hh"

//Geometry info
class modelGeometry{
public:
  //constructor 1: User input
  modelGeometry(double limb_, double radius_, int num_layers_, double layer_com_h_, double layer_res_h_,
          double Y_, int num_limbs_, double angle_, double limb_a_, double limb_b_ , std::string filename) :
                                                                             limb(limb_),
                                                                             radius(radius_),
                                                                             layer_com_h(layer_com_h_),
                                                                             layer_res_h(layer_res_h_),
                                                                             Y(Y_), num_limbs(num_limbs_),
                                                                             angle(angle_), limb_a(limb_a_),
                                                                             limb_b(limb_b_),
                                                                             stack_seq(0), macroGrid(0)   {
        //Read in stacking sequence and macrogrid
        auto values = readCsv(filename);

        layupInfo layup(values);

        //calculate number of layers and thickness informations
        num_layers = layup.num_layers + layup.num_layers-1;
        thickness = layer_com_h*layup.num_layers+(layup.num_layers-1)*layer_res_h;

        //Read stacking sequence
        stack_seq.resize(num_layers);
        for(int i = 0; i< num_layers; i++)
            stack_seq[i].resize(1);
        for(int i = 0; i < layup.num_layers; i++){
            stack_seq[2*i][0] = layup.stacking_seq[i][0];
        }
        macroGrid = layup.macroGridPoints;

  };

  //constructor 2: predefined model
  modelGeometry(std::string name, int num_ranks) : stack_seq(0), macroGrid(0), angle(90){
      std::string filename;
      //Cubes
      if(name.compare("cube")==0){
        limb = 1;
        radius = 0.;
        layer_res_h = 1;
        layer_com_h = 1;
        Y = 1.;
        //Add variable width mm edge treatment on both sides
        if(config.get<int>("edge_treatment",0))
            Y += 2*config.get<double>("edge_width");
        thickness = 1;
        num_limbs = 1;
        filename = "stackingSequences/cube.csv";
    }
    if(name.compare("cube_hole")==0){
        limb = 1;
        radius = 0.;
        layer_res_h = .1;
        layer_com_h = 0;
        Y = 1.;
        //Add variable width mm edge treatment on both sides
        if(config.get<int>("edge_treatment",0))
            Y += 2*config.get<double>("edge_width");
        thickness = .1;
        num_limbs = 1;
        filename = "stackingSequences/cube.csv";
    }
    if(name.compare("cube_geneo")==0){
        limb = num_ranks;
        radius = 0.;
        layer_res_h = 0.25;
        layer_com_h = 0.25;
        Y = 1.;
        //Add variable width mm edge treatment on both sides
        if(config.get<int>("edge_treatment",0))
            Y += 2*config.get<double>("edge_width");
        num_limbs = 1;
        filename = "stackingSequences/cube_geneo.csv";
    }
    if(name.compare("cube_2")==0){
        limb = 1;
        radius = 0.;
        layer_com_h = 0.1/3.;
        layer_res_h = 0.01;
        Y = .5;
        //Add variable width mm edge treatment on both sides
        if(config.get<int>("edge_treatment",0))
            Y += 2*config.get<double>("edge_width");
        num_limbs = 1;
        filename = "stackingSequences/cube_2.csv";
    }
    if(name.compare("cube_12") == 0){
        limb = 8.*2 + 6.6;
        radius = 0.;
        layer_com_h = 0.23;
        layer_res_h = 0.02;
        Y = 5.;
        //Add variable width mm edge treatment on both sides
        if(config.get<int>("edge_treatment",0))
            Y += 2*config.get<double>("edge_width");
        num_limbs = 1;
        filename = "stackingSequences/cube_12.csv";
    }
    if(name.compare("cube_4") == 0){
        limb = 100;
        radius = 0.;
        layer_com_h = 0.23;
        layer_res_h = 0.02;
        Y = 100.;
        //Add variable width mm edge treatment on both sides
        if(config.get<int>("edge_treatment",0))
            Y += 2*config.get<double>("edge_width");
        num_limbs = 1;
        filename = "stackingSequences/cube_4.csv";
    }
    /*if(name.compare("cube_plydrop") == 0){
        limb = 1;
        radius = 0.;
        num_layers = 3;
        layer_res_h = 1/3.;
        layer_com_h = 1/3.;
        Y = 1.;
        thickness = 1;
        num_limbs = 1;
        stack_seq[0][0] = 90;
    }*/

    //L-shapes
    if(name.compare("lshape_thick2")==0){
        limb = 1/3.;
        radius = 1/3.;
        layer_com_h = 1/9.;
        layer_res_h = 1/9.;
        Y = 0.25;
        //Add variable width mm edge treatment on both sides
        if(config.get<int>("edge_treatment",0))
            Y += 2*config.get<double>("edge_width");
        num_limbs = 1;
        filename = "stackingSequences/cube_2.csv";
    }
    if(name.compare("lshape_thin2")==0){
        limb = 1./3.;
        radius = 2./3. - 1./10.;
        layer_com_h = 1./30.;
        layer_res_h = 1./30.;
        Y = 0.5;
        //Add variable width mm edge treatment on both sides
        if(config.get<int>("edge_treatment",0))
            Y += 2*config.get<double>("edge_width");
        num_limbs = 2;
        filename = "stackingSequences/cube_2.csv";
    }
    if(name.compare("lshape_12")==0){
        limb = 8.;
        radius = 6.6;
        layer_com_h = 0.23;
        layer_res_h = 0.02;
        Y = 60.;
        //Add variable width mm edge treatment on both sides
        if(config.get<int>("edge_treatment",0))
            Y += 2*config.get<double>("edge_width");
        num_limbs = 2;
        filename = "stackingSequences/cube_12.csv";
    }
    if(name.compare("lshape_12_mpc")==0){
        limb = 3;
        radius = 6.6;
        layer_com_h = 0.23;
        layer_res_h = 0.02;
        Y = 15;
        //Add variable width mm edge treatment on both sides
        if(config.get<int>("edge_treatment",0))
            Y += 2*config.get<double>("edge_width");
        thickness = 2.98;
        num_limbs = 2;
        filename = "stackingSequences/cube_12.csv";
    }
    if(name.compare("lshape_39")==0){
        limb = 10;
        radius = 22;
        layer_com_h = 0.23;
        layer_res_h = 0.02;
        Y = config.get<double>("Y",15);
        //Add variable width mm edge treatment on both sides
        if(config.get<int>("edge_treatment",0))
            Y += 2*config.get<double>("edge_width");
        num_limbs = 2;
        filename = "stackingSequences/lshape_39.csv";
    }
    if(name.compare("lshape_85")==0){
        limb = 21;
        radius = 29; //inner radius
        layer_com_h = 0.23;
        layer_res_h = 0.02;
        Y = 304;
        //Add variable width mm edge treatment on both sides
        if(config.get<int>("edge_treatment",0))
            Y += 2*config.get<double>("edge_width");
        angle = 92;
        num_limbs = 2;
        filename = "stackingSequences/lshape_85.csv";

    }
    if(name.compare("wingbox")==0){
        limb_a = 1000;
        limb_b = 300;
        radius = 15;
        Y = config.get<double>("Y",1000);

        layer_com_h = .23;
        layer_res_h = .02;

        filename = "stackingSequences/wingbox.csv";
    }
    //Read stacking sequence
    auto values = readCsv(filename);

    layupInfo layup(values);

    //Calculate number of layers and thickness
    num_layers = layup.num_layers + layup.num_layers-1;
    thickness = layer_com_h*layup.num_layers+(layup.num_layers-1)*layer_res_h;

    //Write into vector
    macroGrid = layup.macroGridPoints;
    stack_seq.resize(num_layers);
    for(int i = 0; i< num_layers; i++)
        stack_seq[i].resize(macroGrid.size());
    for(int i = 0; i < layup.num_layers; i++){
        for(int j = 0; j < macroGrid.size(); j++)
        stack_seq[2*i][j] = layup.stacking_seq[i][j];
    }

  }

  //members
  double limb;
  double radius;
  int num_layers;
  double layer_com_h;
  double layer_res_h;
  double thickness;
  double Y;
  int num_limbs;
  double angle;
  //only for wingbox
  double limb_a;
  double limb_b;

  std::string gname;
  //Read in from csv file (in stacking sequence folder)
  std::vector<std::vector<double>> stack_seq;
  std::vector<double> macroGrid;
};




#endif //GEOMETRY_INFO_HH
