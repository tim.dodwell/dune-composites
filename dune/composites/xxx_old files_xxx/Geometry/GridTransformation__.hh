#include "RandomFields/integrator.hh"
#include "RandomFields/defectGenerator.hh"

#ifndef GRIDTRANSFORMATION__HH
#define GRIDTRANSFORMATION__HH

template<typename DomainVector, typename RangeVector,class MODEL>
class GeometryFct{
    public:
        GeometryFct(const MODEL& model_, COEFF myDefect_) : model(model_),  myDefect(myDefect_)
        {
        }

        RangeVector returnY(const DomainVector& x) const{
            double theta;
            return returnY(x,theta);
        }

        RangeVector returnY(const DomainVector& x, double& theta) const{
            RangeVector y(0.0);
            y = x;
            //RangeVector y_ply_drops = insert_ply_drops(x);
            RangeVector ydef = defect(y);
            theta = 0;
            y = x;

            //Transformation for wingbox
            if(model.limb_a>0){
                //no transformation needed for first limb section
                double offset =  model.radius+model.thickness;
                y[0] = x[0] + offset;
                //transform first radial section
                if(x[0] >= model.limb_a && x[0] <= model.limb_a+model.radius){
                    //Angle in radians
                    theta = M_PI/2*(ydef[0]-model.limb_a)/model.radius;
                    //Radius
                    double r = model.radius + model.thickness - ydef[2];
                    y[0] = offset + model.limb_a + r*sin(theta);
                    y[1] = ydef[1];
                    y[2] = model.radius + model.thickness - r*cos(theta);
                }
                //Model the second limb
                if(x[0] > model.limb_a+model.radius && x[0]<=model.limb_a+model.limb_b+model.radius){
                    theta = M_PI/2;
                    double y0 = ydef[0] - model.limb_a - model.radius;
                    double y2 = ydef[2];
                    y[0] = offset + model.limb_a + model.radius + model.thickness - y2;
                    y[1] = ydef[1];
                    y[2] = model.radius + model.thickness + y0;
                }
                //model second radial section
                if(x[0] >= model.limb_a+model.limb_b+model.radius && x[0] <= model.limb_a+model.limb_b+2*model.radius){
                    //Angle in radians
                    theta = M_PI/2*(ydef[0]-model.limb_a-model.limb_b-model.radius)/model.radius + M_PI/2;
                    //Radius
                    double r = model.radius + model.thickness - ydef[2];
                    y[0] = offset + model.limb_a + r*sin(theta);
                    y[1] = ydef[1];
                    y[2] = model.radius + model.thickness + model.limb_b - r*cos(theta);
                }
                //Model the third limb
                if(x[0] > model.limb_a + model.limb_b + 2*model.radius && x[0]<=2*model.limb_a + model.limb_b + 2*model.radius){
                    theta = M_PI;
                    double y0 = ydef[0] - model.limb_a -model.limb_b - 2*model.radius;
                    double y2 = ydef[2];
                    y[0] = offset + model.limb_a + cos(theta)*y0 - sin(theta)*y2;
                    y[1] = ydef[1];
                    y[2] = 2*model.radius + 2*model.thickness +model.limb_b + sin(theta)*y0 + cos(theta)*y2;
                }
                //Model the third radial section
                if( x[0] > 2*model.limb_a + model.limb_b + 2*model.radius && x[0] <= 2*model.limb_a + model.limb_b + 3*model.radius){
                    //Angle in radians
                    theta = M_PI/2*(ydef[0]-2*model.limb_a-model.limb_b-2*model.radius)/model.radius + M_PI;
                    //Radius
                    double r = model.radius + model.thickness - ydef[2];
                    y[0] = offset + r*sin(theta);
                    y[1] = ydef[1];
                    y[2] = model.radius + model.thickness + model.limb_b - r*cos(theta);
                }
                //Model the fourth limb
                if(x[0] > 2*model.limb_a + model.limb_b + 3*model.radius && x[0]<=2*model.limb_a + 2*model.limb_b + 3*model.radius){
                    theta = 3*M_PI/2;
                    double y0 = ydef[0] - 2*model.limb_a - model.limb_b - 3*model.radius;
                    double y2 = ydef[2];
                    y[0] = offset - model.radius - model.thickness + cos(theta)*y0 - sin(theta)*y2;
                    y[1] = ydef[1];
                    y[2] = model.radius + model.thickness + model.limb_b + sin(theta)*y0 + cos(theta)*y2;
                }
                //Model final radial section
                if( x[0] > 2*model.limb_a + 2*model.limb_b + 3*model.radius ){
                    //Angle in radians
                    theta = M_PI/2*(ydef[0]-2*model.limb_a-2*model.limb_b-3*model.radius)/model.radius + 3*M_PI/2;
                    //Radius
                    double r = model.radius + model.thickness - ydef[2];
                    y[0] = offset + r*sin(theta);
                    y[1] = ydef[1];
                    y[2] = model.radius + model.thickness - r*cos(theta);
                }
            }
            else{
                //L-shape
                if(model.radius > 1e-06){
                    if(model.num_limbs == 2){
                        //No transformation necessary for first limb
                        //Model the curve
                        if(x[0] >= model.limb && x[0] <= model.limb+model.radius){
                            //Angle in radians
                            theta = model.angle*M_PI/180*(ydef[0]-model.limb)/model.radius;
                            //Radius
                            double r = model.radius + model.thickness - ydef[2];
                            y[0] = model.limb + r*sin(theta);
                            y[1] = ydef[1];
                            y[2] = model.radius + model.thickness - r*cos(theta);
                        }
                    //Model the second limb
                    if(x[0] > model.limb+model.radius){
                        theta = model.angle*M_PI/180;
                        double y0 = ydef[0] - model.limb - model.radius;
                        double y2 = ydef[2];
                        y[0] = model.limb + model.radius + model.thickness + cos(theta)*y0 - sin(theta)*y2;
                        y[1] = ydef[1];
                        y[2] = model.radius + model.thickness + sin(theta)*y0 + cos(theta)*y2;
                    }
                }
                else{
                    if( x[0] < model.radius+1e-06){  //Curvy bit
                        //Angle in radians
                        theta = (M_PI/2.)*ydef[0]/model.radius;
                        //Radius
                        double r = model.radius + model.thickness - ydef[2];
                        y[0] = r*sin(theta);
                        y[1] = ydef[1];
                        y[2] = model.radius + model.thickness - r*cos(theta) ;
                    }
                    if(x[0] >= model.radius+1e-06){
                        theta = M_PI/2.;
                        y[0] = model.radius + model.thickness - ydef[2];
                        y[1] = ydef[1];
                        y[2] = model.thickness + ydef[0];
                    }
                }
            }
            //Cube
            else{
                theta = 0; //angle of the cube
                y[0] = cos(theta)*ydef[0]-sin(theta)*ydef[2];
                y[1] = ydef[1];
                y[2] = sin(theta)*ydef[0]+cos(theta)*ydef[2];
                return y;
            }
                }
        }
        double returnTheta(const DomainVector& x) const{
            RangeVector y,yph;
            double theta,thetaph;
            double h = 1e-10;
            RangeVector xph = x; xph[0] += h/2.;
            RangeVector xmh = x; xmh[0] -= h/2.;
            y = returnY(xmh,theta);
            yph = returnY(xph,thetaph);
            auto angle = atan2((yph[2]-y[2])/h,(yph[0]-y[0])/h);
            return angle;
        }


    private:
        /*RangeVector insert_ply_drops(const DomainVector &x) const{
            RangeVector y = x;
            if(model.macroGrid.size() > 0 ){
                double shortened = (model.thickness - model.macroGrid.size()*model.layer_com_h)/model.thickness;
                for( int i = 0; i < model.macroGrid.size(); i++){
                    //double drop_loc_left = model.ply_drops[i][0];
                    //double drop_loc_right = model.ply_drops[i+1][0];
                    //if(drop_loc_left < x[0] && x[0] < drop_loc_right ){
                        y[0] = x[0];
                        y[1] = x[1];
                        y[2] = shortened * x[2];
                    //}
                }
            }
            return y;
            }*/

        RangeVector defect(const DomainVector &x) const{
            //Get defect data from ini file
            double defect_size = config.get<double>("defect_size",0);
            RangeVector y = x;
            if(defect_size>0){
                //int wavelength = config.get<int>("wavelength",7);
                Dune::FieldVector<double,3> damping = {config.get<double>("dampingX",1),config.get<double>("dampingY",1),config.get<double>("dampingZ",1)};
                Dune::FieldVector<double,3> defect_location = {config.get<double>("defect_locationX",model.radius/2.+model.limb),
                    config.get<double>("defect_locationY",model.Y/2.),
                    config.get<double>("defect_locationZ",model.thickness/2)};
                double dampingZ = damping[2];
                //Define defect function
                if(x[0] > model.limb && x[0] < model.limb+model.radius || model.radius == 0){
                    if(config.get<std::string>("defect_type","simple") == "simple"){
                        if(model.radius==0){
                            y[2] += defect_size*cos(M_PI*config.get<double>("frequency",4)*x[0])
                                * 1./pow(cosh(damping[0] * M_PI * (defect_location[0] - x[0])/model.limb),2.)
                                * 1./pow(cosh(damping[1] * M_PI * (defect_location[1] - x[1])/model.Y),2.)
                                * 1./pow(cosh(dampingZ * M_PI * (defect_location[2] - x[2])/model.thickness),2.);
                        }
                        else{
                            if(x[2] > defect_location[2]) dampingZ *= 4;
                            y[2] += defect_size
                                * 1./pow(cosh(damping[0] * M_PI * (defect_location[0] - x[0])/model.radius*(x[2]+0.1)),2.)
                                * 1./pow(cosh(damping[1] * M_PI * (defect_location[1] - x[1])/model.Y),2.)
                                * 1./pow(cosh(dampingZ * M_PI * (defect_location[2] - x[2])/model.thickness*0.5),2.);
                        }
                    }
                    if(config.get<std::string>("defect_type","simple") == "MC"){
                        // In curvy bit
                        double xval = (x[0]-model.limb)/model.radius * config.get<double>("LengthX"); // xval goes from 0 to 346

                        double pix = 5.0/58.0;
                        xval = xval*pix; // xval converted to mm


                        auto xd = std::log(1.0/98); // because the processed image is 98 pixels tall
                        auto xdamp = std::exp(pow((x[0] - defect_location[0]),2) / (pow((model.radius/2.0),2) / xd));

                        auto zd = std::log(1.0/98.0);
                        auto zdamp = std::exp(x[2]*x[2] / (pow((model.thickness),2) / zd)); // this one is different because the wrinkle is located at the inner radius.

                        auto yd = std::log(1.0 /98.0);
                        auto ydamp = std::exp(pow((x[1] - defect_location[1]),2) / (pow(26,2) / yd));

                        for (int i = 0; i < myDefect.getN(); i++){                  

                            y[2] += defect_size * myDefect.evalPhi(xval,i) * myDefect.getxi(i) * xdamp * ydamp * zdamp;

                        }
                    }
                    if(config.get<std::string>("defect_type","simple")  == "cosdefect" ){
                        double theta = 2*((x[0]-model.limb)/model.radius - 1./2.);
                        if(theta > 1 || theta<-1) std::cout << theta << std::endl;
                        double l = x[1];
                        double r = model.radius + (model.thickness - x[2]);

                        auto Amax = config.get<double>("Amax");

                        auto rmin = config.get<double>("rmin");
                        auto rAmax = config.get<double>("rAmax");
                        auto rmax = config.get<double>("rmax");

                        auto rthetamin = config.get<double>("rthetamin");
                        auto rthetamax = config.get<double>("rthetamax");
                        auto rlmin = config.get<double>("rlmin");
                        auto rlmax = config.get<double>("rlmax");

                        auto thetamin = config.get<double>("thetamin");
                        auto thetamax = config.get<double>("thetamax");
                        auto omega1 = config.get<double>("omega1");
                        auto omega2 = config.get<double>("omega2");

                        auto lmin = config.get<double>("lmin");
                        auto lmax = config.get<double>("lmax");
                        auto psi1 = config.get<double>("psi1");
                        auto psi2 = config.get<double>("psi2");

                        double A = 0;
                        if(r > rmin  && r < rAmax+1e-03) A = Amax*(r - rmin)/(rAmax-rmin);
                        if(r >= rAmax && r < rmax)  A = Amax*(rmax-r)/(rmax-rAmax);

                        double theta1 = thetamin + omega1*(r - rthetamin);
                        double theta2 = thetamax + omega2*(r - rthetamax);

                        double l1 = lmin + psi1*(r - rlmin);
                        double l2 = lmax + psi2*(r - rlmax);

                        if( fabs(A) >1e-6){
                            if( l<l2 && l>l1 && theta>theta1 && theta<theta2){
                                double defect_size = A/4. * (1-cos(2*M_PI*(theta-theta1)/(theta2-theta1))) * (1-cos(2*M_PI*(l-l1)/(l2-l1)));
                                y[2] -= defect_size;
                                //std::cout << "defect size " << defect_size << std::endl;
                                //  std::cout << "A " << A/4. << std::endl;
                                //  std::cout << "costheta " << cos(2*M_PI*(theta-theta1)/(theta2-theta1)) << std::endl;
                                //  std::cout << "cosl " << cos(2*M_PI*(l-l1)/(l2-l1)) << std::endl;
                            }
                        }
                    }
                }
            }
            return y;
        }

        const MODEL& model;
        COEFF myDefect;

};


//Grid Transformation
template <int dim, class MODEL>
class GridTransformation
: public Dune :: AnalyticalCoordFunction< double, dim, dim, GridTransformation <dim,MODEL> >
{
  typedef GridTransformation This;
  typedef Dune :: AnalyticalCoordFunction< double, dim, dim, This > Base;

public:
  typedef typename Base :: DomainVector DomainVector;
  typedef typename Base :: RangeVector RangeVector;

  GridTransformation(MODEL& model_, int my_rank_,COEFF myDefect_) : model(model_), giveOutput(1), my_rank(my_rank_), myDefect(myDefect_)
  {
  }

  void evaluate ( const DomainVector &x, RangeVector &y ) const
  {
      GeometryFct<DomainVector,RangeVector,MODEL> g(model,myDefect);
      y = g.returnY(x);
  }
private:
  COEFF myDefect;
  mutable int giveOutput;
  MODEL model;
  int my_rank;
};

#endif
