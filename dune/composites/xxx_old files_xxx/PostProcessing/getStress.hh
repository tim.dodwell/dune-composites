#ifndef GET_STRESS_HH
#define GET_STRESS_HH

#include <dune/geometry/type.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>

namespace Dune {
  namespace PDELab {


    template<typename GV, typename PROBLEM, typename DGF, int dofel>  
    class getStressLinear :
        public NumericalJacobianApplyVolume<getStressLinear<GV,PROBLEM,DGF,dofel> >,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<double>,
        public NumericalJacobianVolume<getStressLinear<GV,PROBLEM,DGF, dofel> >
    {
    public:
      // pattern assembly flags
      enum { doPatternVolume = false };

      // residual assembly flags
      enum { doAlphaVolume  = true };

      //Constructor
      getStressLinear (const GV& gv_, const PROBLEM& prob_, const DGF& dgf_, double& maxDisp_) : gv(gv_), problem(prob_) ,  maxDisp(maxDisp_), dgf(dgf_){}
      //
      // alpha_volume for getStress
      //
      // Volume Integral Depending on Test and Ansatz Functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
        void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
        {
          const int dim = 3;

          //Unwrap function spaces
          typedef typename LFSU::template Child<0>::Type LFS1;
          typedef typename LFS1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
          typedef typename LFS1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType;
          //Unwrap displacements
          const auto& lfs1 = lfsu.template child<0>();
          const auto& lfs2 = lfsu.template child<1>();
          const auto& lfs3 = lfsu.template child<2>();
          //Unwrap stress
          const auto& lfsv1 = lfsv.template child<0>();
          const auto& lfsv2 = lfsv.template child<1>();
          const auto& lfsv3 = lfsv.template child<2>();
          const auto& lfsv4 = lfsv.template child<3>();
          const auto& lfsv5 = lfsv.template child<4>();
          const auto& lfsv6 = lfsv.template child<5>();

          //Evaluate elasticity tensor
          Dune::FieldMatrix<double,6,6> C(0.0),Cthermal(0.0);
          problem.evaluate(eg.entity(),C);
          problem.evaluate_strain(eg.entity(),Cthermal);

          //Residual heat stresses
          Dune::FieldVector<double,6> delta(0.0);
          if(problem.getMaterialType(eg.entity()) == 0)//resin
              problem.evaluateHeat(delta,true);
          else
              problem.evaluateHeat(delta,false); //ply

          //Calculate input heat stress
          Dune::FieldVector<double,6> appliedStress(0.0);
          Cthermal.mv(delta, appliedStress);

          int nodes_per_element = lfsv1.size();
          int dof = lfs1.size();

          // select quadrature rule
          for (int iter = 0; iter < nodes_per_element; iter++){
              Dune::FieldVector<double,1> num_elem(1.0);
              Dune::FieldVector<double,dim> pt = eg.geometry().local(eg.geometry().corner(iter));
              Dune::FieldVector<double,dim> pt_cen = eg.geometry().local(eg.geometry().center());

              auto theta = problem.getThetaFct(eg.entity(),pt);
              auto Cijkl = getMatrix(C,theta);

              // Evaluate Jacobian
              std::vector<JacobianType> js(dof);
              lfs1.finiteElement().localBasis().evaluateJacobian(pt,js);

              // Transform gradient to real element and evaluate at integration points
              const auto jac = eg.geometry().jacobianInverseTransposed(pt);
              std::vector<Dune::FieldVector<double,dim> > gradphi(dof);
              for (int i=0; i < dof; i++){
                  gradphi[i] = 0.0;
                  jac.umv(js[i][0],gradphi[i]);
              }
              Dune::FieldMatrix<double,6,dofel> B(0.0);
              for (int i = 0; i < dof; i++)
              {
                  B[0][i] = gradphi[i][0];                                          // E11
                  B[1][i + dof] = gradphi[i][1];                                    // E22
                  B[2][i + 2 * dof] = gradphi[i][2];                                // E33
                  B[3][i + dof] = gradphi[i][2]; B[3][i + 2 * dof] = gradphi[i][1]; // E23
                  B[4][i] = gradphi[i][2];       B[4][i + 2 * dof] = gradphi[i][0]; // E13
                  B[5][i] = gradphi[i][1];       B[5][i + dof] = gradphi[i][0];     // E12	
              }

              Dune::FieldVector<double,6> e(0.0),s(0.0);
              Dune::FieldVector<double,dofel>  xlc(0.0);
              for(int i = 0; i < dof; i++){
                  xlc[i]       = x(lfs1,i);
                  xlc[i+dof]   = x(lfs2,i);
                  xlc[i+2*dof] = x(lfs3,i);
              }

              B.mv(xlc,e);    // e = B * x 
              Cijkl.mv(e,s);  // s = Cijkl * e

              s -= appliedStress;

              // Evaluate Jacobian
              std::vector<JacobianType> js_cen(dof);
              lfs1.finiteElement().localBasis().evaluateJacobian(pt_cen,js_cen);

              // Transform gradient to real element and evaluate at integration points
              const auto jac_cen = eg.geometry().jacobianInverseTransposed(pt_cen);
              std::vector<Dune::FieldVector<double,dim> > gradphi_cen(dof);
              for (int i=0; i < dof; i++){
                  gradphi_cen[i] = 0.0;
                  jac_cen.umv(js_cen[i][0],gradphi_cen[i]);
              }
              Dune::FieldMatrix<double,6,dofel> B_cen(0.0);
              for (int i = 0; i < dof; i++)
              {
                  B_cen[0][i] = gradphi_cen[i][0];                                          // E11
                  B_cen[1][i + dof] = gradphi_cen[i][1];                                    // E22
                  B_cen[2][i + 2 * dof] = gradphi_cen[i][2];                                // E33
                  B_cen[3][i + dof] = gradphi_cen[i][2]; B_cen[3][i + 2 * dof] = gradphi_cen[i][1]; // E23
                  B_cen[4][i] = gradphi_cen[i][2];       B_cen[4][i + 2 * dof] = gradphi_cen[i][0]; // E13
                  B_cen[5][i] = gradphi_cen[i][1];       B_cen[5][i + dof] = gradphi_cen[i][0];     // E12	
              }

              Dune::FieldVector<double,6> e_cen(0.0),s_cen(0.0);
              B_cen.mv(xlc,e_cen);    // e = B * x  
              Cijkl.mv(e_cen,s_cen);  // s = Cijkl * e

              s_cen -= appliedStress;

              for(int i = 0; i < dof; i++){
                  double maxUpdate = sqrt(xlc[i]*xlc[i]+xlc[i+dof]*xlc[i+dof]+xlc[i+2*dof]*xlc[i+2*dof]);
                  if(maxUpdate > maxDisp){ maxDisp = maxUpdate; } 
              }

              //Stresses
              dgf.evaluate(eg.entity(),pt,num_elem);
              r.accumulate(lfsv1, iter, s[0]);
              r.accumulate(lfsv2, iter, s[1] /num_elem[0]);
              r.accumulate(lfsv3, iter, s_cen[2]/num_elem[0]);
              r.accumulate(lfsv4, iter, s[3]);
              r.accumulate(lfsv5, iter, s[4] /num_elem[0]);
              r.accumulate(lfsv6, iter, s[5]);
          }


        }
    private:

      Dune::FieldMatrix<double,6,6> getR(Dune::FieldMatrix<double,3,3> A) const{
          Dune::FieldMatrix<double,6,6> R;
          R[0][0] = A[0][0]*A[0][0]; R[0][1] = A[0][1]*A[0][1]; R[0][2] = A[0][2]*A[0][2];
          R[1][0] = A[1][0]*A[1][0]; R[1][1] = A[1][1]*A[1][1]; R[1][2] = A[1][2]*A[1][2];
          R[2][0] = A[2][0]*A[2][0]; R[2][1] = A[2][1]*A[2][1]; R[2][2] = A[2][2]*A[2][2];

          R[0][3] = 2.0*A[0][1]*A[0][2]; R[0][4] = 2.0*A[0][0]*A[0][2]; R[0][5] = 2.0*A[0][0]*A[0][1];
          R[1][3] = 2.0*A[1][1]*A[1][2]; R[1][4] = 2.0*A[1][0]*A[1][2]; R[1][5] = 2.0*A[1][0]*A[1][1];
          R[2][3] = 2.0*A[2][1]*A[2][2]; R[2][4] = 2.0*A[2][0]*A[2][2]; R[2][5] = 2.0*A[2][0]*A[2][1];

          R[3][0] = A[1][0]*A[2][0]; R[3][1] = A[1][1]*A[2][1]; R[3][2] = A[1][2]*A[2][2];
          R[4][0] = A[0][0]*A[2][0]; R[4][1] = A[0][1]*A[2][1]; R[4][2] = A[0][2]*A[2][2];
          R[5][0] = A[0][0]*A[1][0]; R[5][1] = A[0][1]*A[1][1]; R[5][2] = A[0][2]*A[1][2];

          R[3][3] = A[1][1]*A[2][2]+A[1][2]*A[2][1]; R[3][4] = A[1][0]*A[2][2]+A[1][2]*A[2][0]; R[3][5] = A[1][0]*A[2][1]+A[1][1]*A[2][0];
          R[4][3] = A[0][1]*A[2][2]+A[2][1]*A[0][2]; R[4][4] = A[0][0]*A[2][2]+A[2][0]*A[0][2]; R[4][5] = A[2][0]*A[0][1]+A[2][1]*A[0][0];
          R[5][3] = A[0][1]*A[1][2]+A[0][2]*A[1][1]; R[5][4] = A[0][0]*A[1][2]+A[0][2]*A[1][0]; R[5][5] = A[0][0]*A[1][1]+A[0][1]*A[1][0];
          return R;
      }

      Dune::FieldMatrix<double,6,6> getMatrix(Dune::FieldMatrix<double,6,6> C, std::vector<double> theta) const{
          double c = cos(theta[1]*M_PI/180.0);
          double s = sin(theta[1]*M_PI/180.0);
          Dune::FieldMatrix<double,3,3> A = {{c,0,-s},{0,1,0},{s,0,c}};

          Dune::FieldMatrix<double,6,6> R = getR(A);

          auto Ch = C;
          Dune::FieldMatrix<double,6,6> RT(0.0);
          transpose<Dune::FieldMatrix<double,6,6>,6>(R,RT);
          Ch.rightmultiply(RT);
          return Ch;
      }

      const GV& gv;
      const PROBLEM& problem;
      const DGF& dgf;
      double& maxDisp;
    };


    template<typename GV, typename PROBLEM, int dofel>  
    class getStress :
        public NumericalJacobianApplyVolume<getStress<GV,PROBLEM,dofel> >,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<double>,
        public NumericalJacobianVolume<getStress<GV,PROBLEM, dofel> >
    {
    public:
      // pattern assembly flags
      enum { doPatternVolume = false };

      // residual assembly flags
      enum { doAlphaVolume  = true };

      //Constructor
      getStress (const GV& gv_, const PROBLEM& prob_, double& maxDisp_, Dune::FieldVector<double,7>& maxFail_, Dune::FieldVector<double,3>& loc_) : gv(gv_), problem(prob_) ,  maxDisp(maxDisp_), maxFail(maxFail_), loc(loc_) {}
      //
      // alpha_volume for getStress
      //
      // Volume Integral Depending on Test and Ansatz Functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
        void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
        {
          const int dim = 3;

          //Unwrap function spaces
          typedef typename LFSU::template Child<0>::Type LFS1;
          typedef typename LFS1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
          typedef typename LFS1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType;
          //Unwrap displacements
          const auto& lfs1 = lfsu.template child<0>();
          const auto& lfs2 = lfsu.template child<1>();
          const auto& lfs3 = lfsu.template child<2>();
          //Unwrap stress
          const auto& lfsv1 = lfsv.template child<0>();
          const auto& lfsv2 = lfsv.template child<1>();
          const auto& lfsv3 = lfsv.template child<2>();
          const auto& lfsv4 = lfsv.template child<3>();
          const auto& lfsv5 = lfsv.template child<4>();
          const auto& lfsv6 = lfsv.template child<5>();
          //Unwrap failure criterion
          const auto& lfsvf = lfsv.template child<6>();

          //Evaluate elasticity tensor
          Dune::FieldMatrix<double,6,6> C(0.0),Cthermal(0.0);
          problem.evaluate(eg.entity(),C);
          problem.evaluate_strain(eg.entity(),Cthermal);

          int nodes_per_element = lfsv1.size();
          int dof = lfs1.size();

          //Residual heat stresses
          Dune::FieldVector<double,6> delta(0.0);
          if(problem.getMaterialType(eg.entity()) == 0)//resin
              problem.evaluateHeat(delta,true);
          else
              problem.evaluateHeat(delta,false); //ply

          //Calculate input heat stress
          Dune::FieldVector<double,6> appliedStress(0.0);
          Cthermal.mv(delta, appliedStress);

          auto stress33 = 0.0;         auto stress13 = 0.0;         auto stress23 = 0.0;
          std::vector<double> stress(6);
          for(int i = 0; i<6; i++) stress[i] = 0;
          // select quadrature rule
          GeometryType gt = eg.geometry().type();
          const QuadratureRule<DF,dim>& rule = QuadratureRules<DF,dim>::rule(gt,problem.getIntOrder());
          for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it){
              Dune::FieldVector<double,dim> pt = it->position();
              auto theta = problem.getTheta(eg.entity());
              auto Cijkl = getMatrix(C,theta);

              // Evaluate Jacobian
              std::vector<JacobianType> js(dof);
              lfs1.finiteElement().localBasis().evaluateJacobian(pt,js);

              // Transform gradient to real element and evaluate at element center
              const auto jac = eg.geometry().jacobianInverseTransposed(pt);
              std::vector<Dune::FieldVector<double,dim> > gradphi(dof);
              for (int i=0; i < dof; i++){
                  gradphi[i] = 0.0;
                  jac.umv(js[i][0],gradphi[i]);
              }
              Dune::FieldMatrix<double,6,dofel> B(0.0);
              for (int i = 0; i < dof; i++)
              {
                  B[0][i] = gradphi[i][0];                                          // E11
                  B[1][i + dof] = gradphi[i][1];                                    // E22
                  B[2][i + 2 * dof] = gradphi[i][2];                                // E33
                  B[3][i + dof] = gradphi[i][2]; B[3][i + 2 * dof] = gradphi[i][1]; // E23
                  B[4][i] = gradphi[i][2];       B[4][i + 2 * dof] = gradphi[i][0]; // E13
                  B[5][i] = gradphi[i][1];       B[5][i + dof] = gradphi[i][0];     // E12	
              }

              Dune::FieldVector<double,6> e(0.0),s(0.0);
              Dune::FieldVector<double,dofel>  xlc(0.0);
              for(int i = 0; i < dof; i++){
                  xlc[i]       = x(lfs1,i);
                  xlc[i+dof]   = x(lfs2,i);
                  xlc[i+2*dof] = x(lfs3,i);
              }

              B.mv(xlc,e);    // e = B * x  
              Cijkl.mv(e,s);  // s = Cijkl * e

              s-=appliedStress;

              for(int i = 0; i < dof; i++){
                  double maxUpdate = sqrt(xlc[i]*xlc[i]+xlc[i+dof]*xlc[i+dof]+xlc[i+2*dof]*xlc[i+2*dof]);
                  if(maxUpdate > maxDisp){ maxDisp = maxUpdate; } 
              }

              for (int i = 0; i < nodes_per_element; i++){
                  r.accumulate(lfsv1, i, s[0] *it->weight());
                  r.accumulate(lfsv2, i, s[1] *it->weight());
                  r.accumulate(lfsv3, i, s[2] *it->weight());
                  r.accumulate(lfsv4, i, s[3] *it->weight());
                  r.accumulate(lfsv5, i, s[4] *it->weight());
                  r.accumulate(lfsv6, i, s[5] *it->weight());
              }
              stress[0] += s[0]*it->weight();
              stress[1] += s[1]*it->weight();
              stress[2] += s[2]*it->weight();
              stress[3] += s[3]*it->weight();
              stress[4] += s[4]*it->weight();
              stress[5] += s[5]*it->weight();
          }

          double maxF = failureCriterion<PROBLEM,EG>(problem,eg,stress);
          for(int i = 0; i < nodes_per_element; i++){
              r.accumulate(lfsvf, i, maxF);
              if(maxF > maxFail[0]){
                  maxFail[0] = maxF;
                  maxFail[1] = stress[0];
                  maxFail[2] = stress[1];
                  maxFail[3] = stress[2];
                  maxFail[4] = stress[3];
                  maxFail[5] = stress[4];
                  maxFail[6] = stress[5];
                  loc = eg.geometry().center();
              }
          }
        }
    private:
      Dune::FieldMatrix<double,6,6> getR(Dune::FieldMatrix<double,3,3> A) const{
          Dune::FieldMatrix<double,6,6> R;
          R[0][0] = A[0][0]*A[0][0]; R[0][1] = A[0][1]*A[0][1]; R[0][2] = A[0][2]*A[0][2];
          R[1][0] = A[1][0]*A[1][0]; R[1][1] = A[1][1]*A[1][1]; R[1][2] = A[1][2]*A[1][2];
          R[2][0] = A[2][0]*A[2][0]; R[2][1] = A[2][1]*A[2][1]; R[2][2] = A[2][2]*A[2][2];

          R[0][3] = 2.0*A[0][1]*A[0][2]; R[0][4] = 2.0*A[0][0]*A[0][2]; R[0][5] = 2.0*A[0][0]*A[0][1];
          R[1][3] = 2.0*A[1][1]*A[1][2]; R[1][4] = 2.0*A[1][0]*A[1][2]; R[1][5] = 2.0*A[1][0]*A[1][1];
          R[2][3] = 2.0*A[2][1]*A[2][2]; R[2][4] = 2.0*A[2][0]*A[2][2]; R[2][5] = 2.0*A[2][0]*A[2][1];

          R[3][0] = A[1][0]*A[2][0]; R[3][1] = A[1][1]*A[2][1]; R[3][2] = A[1][2]*A[2][2];
          R[4][0] = A[0][0]*A[2][0]; R[4][1] = A[0][1]*A[2][1]; R[4][2] = A[0][2]*A[2][2];
          R[5][0] = A[0][0]*A[1][0]; R[5][1] = A[0][1]*A[1][1]; R[5][2] = A[0][2]*A[1][2];

          R[3][3] = A[1][1]*A[2][2]+A[1][2]*A[2][1]; R[3][4] = A[1][0]*A[2][2]+A[1][2]*A[2][0]; R[3][5] = A[1][0]*A[2][1]+A[1][1]*A[2][0];
          R[4][3] = A[0][1]*A[2][2]+A[2][1]*A[0][2]; R[4][4] = A[0][0]*A[2][2]+A[2][0]*A[0][2]; R[4][5] = A[2][0]*A[0][1]+A[2][1]*A[0][0];
          R[5][3] = A[0][1]*A[1][2]+A[0][2]*A[1][1]; R[5][4] = A[0][0]*A[1][2]+A[0][2]*A[1][0]; R[5][5] = A[0][0]*A[1][1]+A[0][1]*A[1][0];
          return R;
      }

      Dune::FieldMatrix<double,6,6> getMatrix(Dune::FieldMatrix<double,6,6> C, std::vector<double> theta) const{
          double c = cos(theta[1]*M_PI/180.0);
          double s = sin(theta[1]*M_PI/180.0);
          Dune::FieldMatrix<double,3,3> A = {{c,0,-s},{0,1,0},{s,0,c}};

          Dune::FieldMatrix<double,6,6> R = getR(A);

          auto Ch = C;
          Dune::FieldMatrix<double,6,6> RT(0.0);
          transpose<Dune::FieldMatrix<double,6,6>,6>(R,RT);
          Ch.rightmultiply(RT);

          return Ch;
      }


      const GV& gv;
      const PROBLEM& problem;
      double& maxDisp;
      Dune::FieldVector<double,7>& maxFail;
      Dune::FieldVector<double,3>& loc;
    };

    class countElem :
        public NumericalJacobianApplyVolume<countElem >,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<double>,
        public NumericalJacobianVolume<countElem >
      {
          public:
              // pattern assembly flags
              enum { doPatternVolume = false };

              // residual assembly flags
              enum { doLambdaVolume  = true };

              //Constructor
              countElem (){}

              // alpha_volume for countElem
              template<typename EG, typename LFSV, typename R>
                  void lambda_volume (const EG& eg, const LFSV& lfsv, R& r) const
                  { 
                      for (int i = 0; i < lfsv.size(); i++){
                          r.accumulate(lfsv, i, 1.);
                      }
                  }
      };


    template<typename GV, typename PROBLEM, typename DGF, int dofel>  
    class getStressUG :
        public NumericalJacobianApplyVolume<getStressUG<GV,PROBLEM,DGF,dofel> >,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<double>,
        public NumericalJacobianVolume<getStressUG<GV,PROBLEM,DGF, dofel> >
    {
    public:
      // pattern assembly flags
      enum { doPatternVolume = false };

      // residual assembly flags
      enum { doAlphaVolume  = true };

      //Constructor
      getStressUG (const GV& gv_, const PROBLEM& prob_, const DGF& dgf_, double& maxDisp_) : gv(gv_), problem(prob_) ,  maxDisp(maxDisp_), dgf(dgf_){}
      //
      // alpha_volume for getStress
      //
      // Volume Integral Depending on Test and Ansatz Functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
        void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
        {
          const int dim = 3;

          //Unwrap function spaces
          typedef typename LFSU::template Child<0>::Type LFS1;
          typedef typename LFS1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
          typedef typename LFS1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType;
          //Unwrap displacements
          const auto& lfs1 = lfsu.template child<0>();
          const auto& lfs2 = lfsu.template child<1>();
          const auto& lfs3 = lfsu.template child<2>();
          //Unwrap stress
          const auto& lfsv1 = lfsv.template child<0>();
          const auto& lfsv2 = lfsv.template child<1>();
          const auto& lfsv3 = lfsv.template child<2>();
          const auto& lfsv4 = lfsv.template child<3>();
          const auto& lfsv5 = lfsv.template child<4>();
          const auto& lfsv6 = lfsv.template child<5>();

          //Evaluate elasticity tensor
          Dune::FieldMatrix<double,6,6> C(0.0),Cthermal(0.0);
          problem.evaluate(eg.entity(),C);
          problem.evaluate_strain(eg.entity(),Cthermal);

          //Residual heat stresses
          Dune::FieldVector<double,6> delta(0.0);
          if(problem.getMaterialType(eg.entity()) == 0)//resin
              problem.evaluateHeat(delta,true);
          else
              problem.evaluateHeat(delta,false); //ply

          //Calculate input heat stress
          Dune::FieldVector<double,6> appliedStress(0.0);
          Cthermal.mv(delta, appliedStress);

          int nodes_per_element = lfsv1.size();
          int dof = lfs1.size();

          // select quadrature rule
          for (int iter = 0; iter < nodes_per_element; iter++){
              Dune::FieldVector<double,1> num_elem(1.0);
              Dune::FieldVector<double,dim> pt = eg.geometry().local(eg.geometry().corner(iter));

              auto theta = problem.getThetaFct(eg.entity(),pt);
              auto Cijkl = getMatrix(C,theta);

              // Evaluate Jacobian
              std::vector<JacobianType> js(dof);
              lfs1.finiteElement().localBasis().evaluateJacobian(pt,js);

              // Transform gradient to real element and evaluate at integration points
              const auto jac = eg.geometry().jacobianInverseTransposed(pt);
              std::vector<Dune::FieldVector<double,dim> > gradphi(dof);
              for (int i=0; i < dof; i++){
                  gradphi[i] = 0.0;
                  jac.umv(js[i][0],gradphi[i]);
              }
              Dune::FieldMatrix<double,6,dofel> B(0.0);
              for (int i = 0; i < dof; i++)
              {
                  B[0][i] = gradphi[i][0];                                          // E11
                  B[1][i + dof] = gradphi[i][1];                                    // E22
                  B[2][i + 2 * dof] = gradphi[i][2];                                // E33
                  B[3][i + dof] = gradphi[i][2]; B[3][i + 2 * dof] = gradphi[i][1]; // E23
                  B[4][i] = gradphi[i][2];       B[4][i + 2 * dof] = gradphi[i][0]; // E13
                  B[5][i] = gradphi[i][1];       B[5][i + dof] = gradphi[i][0];     // E12	
              }

              Dune::FieldVector<double,6> e(0.0),s(0.0);
              Dune::FieldVector<double,dofel>  xlc(0.0);
              for(int i = 0; i < dof; i++){
                  xlc[i]       = x(lfs1,i);
                  xlc[i+dof]   = x(lfs2,i);
                  xlc[i+2*dof] = x(lfs3,i);
              }

              B.mv(xlc,e);    // e = B * x 
              Cijkl.mv(e,s);  // s = Cijkl * e

              s -= appliedStress;

              for(int i = 0; i < dof; i++){
                  double maxUpdate = sqrt(xlc[i]*xlc[i]+xlc[i+dof]*xlc[i+dof]+xlc[i+2*dof]*xlc[i+2*dof]);
                  if(maxUpdate > maxDisp){ maxDisp = maxUpdate; } 
              }

              //Stresses
              dgf.evaluate(eg.entity(),pt,num_elem);
              r.accumulate(lfsv1, iter, s[0]/num_elem[0]);
              r.accumulate(lfsv2, iter, s[1]/num_elem[0]);
              r.accumulate(lfsv3, iter, s[2]/num_elem[0]);
              r.accumulate(lfsv4, iter, s[3]/num_elem[0]);
              r.accumulate(lfsv5, iter, s[4]/num_elem[0]);
              r.accumulate(lfsv6, iter, s[5]/num_elem[0]);
          }


        }
    private:

      Dune::FieldMatrix<double,6,6> getR(Dune::FieldMatrix<double,3,3> A) const{
          Dune::FieldMatrix<double,6,6> R;
          R[0][0] = A[0][0]*A[0][0]; R[0][1] = A[0][1]*A[0][1]; R[0][2] = A[0][2]*A[0][2];
          R[1][0] = A[1][0]*A[1][0]; R[1][1] = A[1][1]*A[1][1]; R[1][2] = A[1][2]*A[1][2];
          R[2][0] = A[2][0]*A[2][0]; R[2][1] = A[2][1]*A[2][1]; R[2][2] = A[2][2]*A[2][2];

          R[0][3] = 2.0*A[0][1]*A[0][2]; R[0][4] = 2.0*A[0][0]*A[0][2]; R[0][5] = 2.0*A[0][0]*A[0][1];
          R[1][3] = 2.0*A[1][1]*A[1][2]; R[1][4] = 2.0*A[1][0]*A[1][2]; R[1][5] = 2.0*A[1][0]*A[1][1];
          R[2][3] = 2.0*A[2][1]*A[2][2]; R[2][4] = 2.0*A[2][0]*A[2][2]; R[2][5] = 2.0*A[2][0]*A[2][1];

          R[3][0] = A[1][0]*A[2][0]; R[3][1] = A[1][1]*A[2][1]; R[3][2] = A[1][2]*A[2][2];
          R[4][0] = A[0][0]*A[2][0]; R[4][1] = A[0][1]*A[2][1]; R[4][2] = A[0][2]*A[2][2];
          R[5][0] = A[0][0]*A[1][0]; R[5][1] = A[0][1]*A[1][1]; R[5][2] = A[0][2]*A[1][2];

          R[3][3] = A[1][1]*A[2][2]+A[1][2]*A[2][1]; R[3][4] = A[1][0]*A[2][2]+A[1][2]*A[2][0]; R[3][5] = A[1][0]*A[2][1]+A[1][1]*A[2][0];
          R[4][3] = A[0][1]*A[2][2]+A[2][1]*A[0][2]; R[4][4] = A[0][0]*A[2][2]+A[2][0]*A[0][2]; R[4][5] = A[2][0]*A[0][1]+A[2][1]*A[0][0];
          R[5][3] = A[0][1]*A[1][2]+A[0][2]*A[1][1]; R[5][4] = A[0][0]*A[1][2]+A[0][2]*A[1][0]; R[5][5] = A[0][0]*A[1][1]+A[0][1]*A[1][0];
          return R;
      }

      Dune::FieldMatrix<double,6,6> getMatrix(Dune::FieldMatrix<double,6,6> C, std::vector<double> theta) const{
          double c = cos(theta[1]*M_PI/180.0);
          double s = sin(theta[1]*M_PI/180.0);
          Dune::FieldMatrix<double,3,3> A = {{c,0,-s},{0,1,0},{s,0,c}};

          Dune::FieldMatrix<double,6,6> R = getR(A);

          auto Ch = C;
          Dune::FieldMatrix<double,6,6> RT(0.0);
          transpose<Dune::FieldMatrix<double,6,6>,6>(R,RT);
          Ch.rightmultiply(RT);
          return Ch;
      }

      const GV& gv;
      const PROBLEM& problem;
      const DGF& dgf;
      double& maxDisp;
    };

  }
}

#endif
