#ifndef READ_CSV_HH
#define READ_CSV_HH

#include "fast-cpp-csv-parser-master/csv.h"

class layupInfo{
    public:
        layupInfo(std::vector<std::vector<double>> values){
            num_layers = values.size()-1;
            sizeMacroGrid = values[0].size()-1;

            auto row = values.begin();
            for (auto it=++row->begin(); it != row->end(); it++) {
                macroGridPoints.push_back(*it);
            }

            for (auto row=++values.begin(); row != values.end(); row++){
                std::vector<double> line;
                for (auto it=++row->begin(); it != row->end(); it++){
                    line.push_back(*it);
                }
                stacking_seq.push_back(line);
            }
        }

        int num_layers;
        int sizeMacroGrid;
        std::vector<std::vector<double>> stacking_seq;
        std::vector<double> macroGridPoints;
};

namespace std{
    layupInfo readCsv(string pathToCsv) {
        std::cout << "******" << std::endl;
        vector<vector<double>> values;
        vector<double> valueline;
        ifstream csvFile(pathToCsv);

        if(! csvFile) {
            throw std::runtime_error("Could not open file " + pathToCsv);
        }
        string item;
        for (string line; getline(csvFile, line);)
        {
            istringstream lineStream(line);

            int i = 0;

            std::cout << "newline" << std::endl;

            while (getline(lineStream, item, ','))
            {
                valueline.push_back(atof(item.c_str()));
                std::cout << "!Line = " << valueline[i] << std::endl;
                i++;
            }

            

            values.push_back(valueline);
            valueline.clear();
        }

        layupInfo layup(values);

        return layup;
    }
}

#endif
