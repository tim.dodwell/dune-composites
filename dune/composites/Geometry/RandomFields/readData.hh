#include<vector>
#include <string>
#include <sstream>

#ifndef READDATA_HH
#define READDATA_HH

using namespace std;

Dune::FieldMatrix<double,1000,346> readData(string filename){

  string data;
  ifstream infile(filename);
  if(! infile) {
      std::cout << "Could not open file " + filename << std::endl;
  }
  //std::cout << "\n READING FROM FILE - " << filename << "\n" << std::endl;
  Dune::FieldMatrix<double,1000,346> samples;
  int count = 0;
  while(getline(infile, data)){
    std::istringstream iss(data);
//samples[count].resize(346);

    for (int i = 0; i < 346; i++){
      double val;
      iss >> val;
      samples[count][i] = val;
    }
    count++;
  }
  infile.close();
  return samples;
}

#endif
