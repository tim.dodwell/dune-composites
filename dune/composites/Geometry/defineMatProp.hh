#ifndef _defineMatProp__hh_
#define _defineMatProp__hh_

#include <string>
#include "dune/common/fvector.hh"

template<typename MODEL>
class matProp{
    public:
        //Constructor
        matProp(MODEL& model) {
            //Set material types
            const int numMaterials = 9;
            materialType.resize(numMaterials);
            matprop.resize(numMaterials);
            for(int i = 0; i < numMaterials; i++)
                matprop[i].resize(22);

            //Define all, use a subset
            // Material 1 (Resin - Isotropic)
            materialType[0] = 0; // Isotropic
            matprop[0][0] = 10000; //  Young's Modulus
            matprop[0][1] = 0.35; // - Poisson Ratio
            matprop[0][21] = 0.001; // Density
            // Material 2 (AS4/8552 - Composite)
            materialType[1] = 1; // Composite
            matprop[1][0] = 162000;   matprop[1][1] = 10000;   matprop[1][2] = 10000; // E1, E2, E3 in MPa
            matprop[1][3] = 0.35;  matprop[1][4] = 0.35;  matprop[1][5] = 0.49; // nu 12, nu 13, nu23
            matprop[1][6] = 5200;   matprop[1][7] = 5200;   matprop[1][8] = 3500; //G12, G13, G23
            matprop[1][21] = 0.0001; // Resin Density
            //MPC constraint: apply very stiff material to outer edge of the geometry
            materialType[2] = 0; // Isotropic
            matprop[2][0] = 400*matprop[1][0]; //  Young's Modulus
            matprop[2][1] = 0.35; // - Poisson Ratio
            //Resin edge treatment
            materialType[3] = 0;
            matprop[3][0] = 3800;
            matprop[3][1] = 0.35;
            // Material 1 (Resin - Isotropic)
            materialType[4] = 0; // Isotropic
            matprop[4][0] = 8.5; //  Young's Modulus
            matprop[4][1] = 0.35; // - Poisson Ratio
            matprop[4][21] = 0.001; // Density
            // Material 2 (AS4/8552 - Composite)
            materialType[5] = 1; // Composite
            matprop[5][0] = 130.;  matprop[5][1] = 8.5;    matprop[5][2] = 8.5; // E1, E2, E3
            matprop[5][3] = 0.35;  matprop[5][4] = 0.35;   matprop[5][5] = 0.45; // nu 12, nu 13, nu23
            matprop[5][6] = 3.5;   matprop[5][7] = 4.;     matprop[5][8] = 4.; //G23, G13, G12
            matprop[5][21] = 0.0001; // Resin Density 
            //Resin Geneo 1
            materialType[6] = 0; // Isotropic
            matprop[6][0] = 2*1e11; //  Young's Modulus
            matprop[6][1] = 0.3; // - Poisson Ratio
            //Resin Geneo 2
            materialType[7] = 0; // Isotropic
            matprop[7][0] = config.get<double>("conE",1e10); //  Young's Modulus
            matprop[7][1] = config.get<double>("connu",0.3); // - Poisson Ratio
            //Aluminium
            materialType[8] = 0; // Isotropic
            matprop[8][0] = 6900; //  Young's Modulus
            matprop[8][1] = 0.3; // - Poisson Ratio


            num_layers = model.num_layers;
            height = model.thickness;
            //Set geometry
            if(model.radius == 0){ //cubes
                const int numRegions = 1;
                    lay_mat.resize(numRegions);
                    lay_rot.resize(numRegions);
                    region_data.resize(numRegions);
                    for(int i = 0; i < numRegions; i++){
                        lay_mat[i].resize(num_layers);
                        lay_rot[i].resize(num_layers);
                        region_data[i].resize(6);
                    }
                    region_info.resize(numRegions);
                region_info[0] = 0;
                height = 0.;
                //Set material types and stacking sequence
                for(int i = 0; i < num_layers;  i++){
                    lay_mat[0][i] = ceil((i+1)/2.) - floor((i+1)/2.);
                    lay_rot[0][i] = model.stack_seq[i][0];
                }
                if(num_layers == 1) lay_mat[0][0] = 0;
                if(config.get<std::string>("grid_name","cube").compare("cube_hole")==0)
                    lay_mat[0][0] = 8;

            }
            else{
                if(config.get<std::string>("grid_name").compare("wingbox")==0){
                    const int numRegions = 9;
                    lay_mat.resize(numRegions);
                    lay_rot.resize(numRegions);
                    region_data.resize(numRegions);
                    for(int i = 0; i < numRegions; i++){
                        lay_mat[i].resize(num_layers);
                        lay_rot[i].resize(num_layers);
                        region_data[i].resize(6);
                    }
                    region_info.resize(numRegions);

                    if(config.get<bool>("yaspgrid",true)==false) std::cout << "Warning: mapping of layers not tested for UG, use yaspgrid instead." << std::endl;

                    region_info[0] = 1;
                    region_info[1] = 2;
                    region_info[3] = 1;
                    region_info[4] = 2;
                    region_info[5] = 1;
                    region_info[6] = 2;
                    region_info[7] = 1;
                    if(config.get<int>("mpc",0)){
                        region_info[8] = 1; // Flat plate MPC constraints
                    }

                    //Set material types and stacking sequence
                    for(int i = 0; i < model.num_layers;  i++){
                        for(int j = 0; j < 7;  j++){
                            lay_mat[j][i] = ceil((i+1)/2.) - floor((i+1)/2.);
                            lay_rot[j][i] = model.stack_seq[i][0];
                        }
                        if(config.get<int>("mpc",0)){
                            lay_mat[8][i] = 2; //MPC "resin"
                        }
                    }
                }
                else{
                    if(model.num_limbs == 2){
                        const int numRegions = 5;
                    lay_mat.resize(numRegions);
                    lay_rot.resize(numRegions);
                    region_data.resize(numRegions);
                    for(int i = 0; i < numRegions; i++){
                        lay_mat[i].resize(num_layers);
                        lay_rot[i].resize(num_layers);
                        region_data[i].resize(6);
                    }
                    region_info.resize(numRegions);

                        //Define the three or four regions of the lshape
                        region_info[0] = 1; // Flat plate
                        region_info[1] = 2; // Circle segment
                        region_info[2] = 1; // Flat plate
                        if(config.get<int>("mpc",0)){
                            region_info[3] = 1; // Flat plate MPC constraints
                        }
                        region_data[1][4] = model.limb;
                        region_data[1][5] = model.thickness+model.radius; // For the circle segment, define center

                        if(config.get<int>("edge_treatment",0)){
                            region_info[4] = 1;
                            lay_mat[4][0] = 3;
                            lay_rot[4][0] = 0;
                        }	

                        region_data[2][0] = 0.0;
                        region_data[2][1] = 90.0;
                        region_data[2][2] = 0.0;     // Rotation in x0, x1, x2

                        //Set material types and stacking sequence
                        for(int i = 0; i < model.num_layers;  i++){
                            lay_mat[0][i] = lay_mat[1][i] = lay_mat[2][i] = ceil((i+1)/2.) - floor((i+1)/2.);
                            lay_rot[0][i] = lay_rot[1][i] = lay_rot[2][i] = model.stack_seq[i][0];
                            if(config.get<int>("mpc",0)){
                                lay_mat[3][i] = 2; //MPC "resin"
                            }
                        }

                    }
                    if(model.num_limbs == 1){
                        const int numRegions = 4;
                    lay_mat.resize(numRegions);
                    lay_rot.resize(numRegions);
                    region_data.resize(numRegions);
                    for(int i = 0; i < numRegions; i++){
                        lay_mat[i].resize(num_layers);
                        lay_rot[i].resize(num_layers);
                        region_data[i].resize(6);
                    }
                    region_info.resize(numRegions);

                        //Define the three or four regions of the lshape
                        region_info[0] = 2; // Circle segment
                        region_info[1] = 1; // Flat plate

                        region_data[0][4] = 0.0;
                        region_data[0][5] = model.thickness+model.radius; // For the circle segment, define center

                        region_data[1][0] = 0.0;
                        region_data[1][1] = 90.0;
                        region_data[1][2] = 0.0;     // Rotation in x0, x1, x2

                        //Set material types and stacking sequence
                        for(int i = 0; i < model.num_layers;  i++){
                            lay_mat[0][i] = lay_mat[1][i] = ceil((i+1)/2.) - floor((i+1)/2.);
                            lay_rot[0][i] = lay_rot[1][i] = model.stack_seq[i][0];
                        }

                    }
                }
            }
        }

    public:
	std::string gridName;
	// Define Material Type :
	// 0 - Isotropic,   1 - Orthotropic,    2 - Fully Anisotropic
	std::vector<int> materialType;
    std::vector<std::vector<double>> matprop;
    std::vector<std::vector<double>> region_data;
    std::vector<std::vector<double>> lay_mat;
    std::vector<std::vector<double>> lay_rot;
    std::vector<int> region_info;
	double height;
	int num_layers;
}; // end class

#endif
