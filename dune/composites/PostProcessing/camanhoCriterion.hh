
template<typename PROBLEM, typename EG>
double failureCriterion(const PROBLEM & problem,const EG& eg, std::vector<double> xlc){
    //Failure criterion
    double s33 = config.get<double>("camanho_s33",61);  //in MPa
    double s13 = config.get<double>("camanho_s13",97);
    double s23 = config.get<double>("camanho_s23",94);
    double maxF = sqrt(pow(std::max(xlc[2],0.0)/s33,2) + pow(xlc[4]/s13,2) + pow(xlc[3]/s23,2));
    if(problem.getMaterialType(eg.entity()) == 0 &&problem.getMatID(eg.entity()) == 2){ //Only check in the curved part and resin layers
        return maxF;
    }
    return 0.0;
}

