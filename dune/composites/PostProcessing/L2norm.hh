#ifndef L2NORM_HH
#define L2NORM_HH

#include <cmath>

#include <dune/geometry/referenceelements.hh>
#include <dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include <dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/localvector.hh>
#include <dune/pdelab/gridfunctionspace/subspace.hh>
#include<dune/geometry/type.hh>
#include<dune/geometry/quadraturerules.hh>

template< class GFS, typename GV, class X> 
double L2norm( const GFS& gfs, const GV& gv, X& x, int qorder=5) {
    const int dim = 3;

    // make local function space
    typedef Dune::PDELab::LocalFunctionSpace<GFS> CLFS;
    CLFS clfs(gfs);
    typedef Dune::PDELab::LFSIndexCache<CLFS> CLFSCache;
    CLFSCache ic(clfs);

    typedef typename CLFS::template Child<0>::Type LFSU_U1;

    //FE Info
    typedef typename LFSU_U1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType D;

    std::vector<double> xlc(clfs.maxSize());
    typedef typename X::template ConstLocalView<CLFSCache> XView;
    XView xView(x);

    // loop over grid view
    auto L2norm = 0.0;
    //Loop over elements of grid view
    for (const auto& eit : elements(gv)){
        //Get quadrature rule
        //Dune::GeometryType gt = eit.geometry().type();
        //const Dune::QuadratureRule<D, dim>& rule = Dune::QuadratureRules<D, dim >::rule( gt ,qorder);

        clfs.bind(eit);      // bind local function space to element 
        ic.update();
        xView.bind(ic);
        xView.read(xlc);     //read coefficient vector

        //Local function spaces
        const auto& lfs1 = clfs.template child<0>();
        int npe = lfs1.size();

        //Integrate
        //for(typename Dune::QuadratureRule<D, dim >::const iterator qit=rule.begin(); qit != rule.end(); ++qit){
        for(int i = 0; i < npe; i++){
            auto sum =0.0;
            for(int j = 0; j < dim; j++){
                sum += xlc[i+j*npe]*xlc[i+j*npe];
            }
            L2norm += sum/npe*eit.geometry().volume();
        }
        //}
    }
    return L2norm;
}

template< class GFS, typename GV, class X> 
Dune::FieldVector<double,3> remove_mean( const GFS& gfs, const GV& gv, X& x, int qorder=5) {
    const int dim = 3;

    // make local function space
    typedef Dune::PDELab::LocalFunctionSpace<GFS> CLFS;
    CLFS clfs(gfs);
    typedef Dune::PDELab::LFSIndexCache<CLFS> CLFSCache;
    CLFSCache ic(clfs);

    typedef typename CLFS::template Child<0>::Type LFSU_U1;

    //FE Info
    typedef typename LFSU_U1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType D;

    std::vector<double> xlc(clfs.maxSize());
    typedef typename X::template ConstLocalView<CLFSCache> XView;
    XView xView(x);

    // loop over grid view
    Dune::FieldVector<double,3> xmean;
    //Loop over elements of grid view
    int cnt = 0;
    for (const auto& eit : elements(gv)){
        //Get quadrature rule
        //Dune::GeometryType gt = eit.geometry().type();
        //const Dune::QuadratureRule<D, dim>& rule = Dune::QuadratureRules<D, dim >::rule( gt ,qorder);

        clfs.bind(eit);      // bind local function space to element 
        ic.update();
        xView.bind(ic);
        xView.read(xlc);     //read coefficient vector

        //Local function spaces
        const auto& lfs1 = clfs.template child<0>();
        int npe = lfs1.size();

        //Integrate
        //for(typename Dune::QuadratureRule<D, dim >::const iterator qit=rule.begin(); qit != rule.end(); ++qit){
        for(int i = 0; i < npe; i++){
                xmean[0] += xlc[i+0*npe]/npe;
                xmean[1] += xlc[i+1*npe]/npe;
                xmean[2] += xlc[i+2*npe]/npe;
        }
        cnt++;
        //}
    }
    xmean /= cnt;
    return xmean;

}

#endif //L2NORM_HH
