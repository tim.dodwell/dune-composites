#ifndef CAMANHO_CRITERION_HH
#define CAMANHO_CRITERION_HH

namespace Dune{

	namespace Composites{

		double Camanho(const std::vector<double>& sig, int materialType, const std::vector<double>& param){
		    assert(param.size() == 3); // Check 3 parameters are supplied for Camanho
		    
		    double s33 = param[0]; //config.get<double>("camanho_s33",61);  //in MPa
		    double s13 = param[1]; //config.get<double>("camanho_s13",97);
		    double s23 = param[2]; //config.get<double>("camanho_s23",94);
		    
		    double maxF = sqrt(pow(std::max(sig[2],0.0)/s33,2) + pow(sig[4]/s13,2) + pow(sig[3]/s23,2));
		    if(materialType == 0){ //Only applicatble in resin layers
		        return maxF;
		    }
		    return 0.0;
		}
	}
}
#endif

