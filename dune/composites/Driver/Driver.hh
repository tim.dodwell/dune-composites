

// Need to develop at DRIVER class

#include "../PostProcessing/computeStresses.hh"
#include "../PostProcessing/plot_properties.hh"
#include "localOperator/linearelasticity.hh"
#include "../MathFunctions/L2norm.hh"

namespace Dune{

namespace Composites{


template <class GV, class GFS, class MODEL, class IF, class GV0>

class Driver{

public:

	typedef double RF;

	typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::none,1> Scalar_VectorBackend;

	typedef typename GFS::template ConstraintsContainer<RF>::Type C;

	typedef Scalar_BC<GV,MODEL,RF> BC;

	typedef Dune::PDELab::CompositeConstraintsParameters<BC,BC,BC> Constraints;

	typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;

	const int dofel = 60;

	// Constructor class
	Driver(const GFS& gfs_, const GV& gv_, MODEL& model_, const IF& initial_solution_, const GV0& gv0_) 
		: gfs(gfs_),
		gv(gv_),
		model(model_),
		initial_solution(initial_solution_),
		gv0(gv0_){

	}



	void apply(){

		// Make constraints map and initialize it from a function
    	C cg, cg_ext;
		cg.clear(); cg_ext.clear();

		//
		BC U1_cc(gv,model,0), U2_cc(gv,model,1), U3_cc(gv,model,2);

		Constraints constraints(U1_cc,U2_cc,U3_cc);

		Dune::PDELab::constraints(constraints,gfs,cg);

		//

		/*typedef Scalar_BC<GV0,MODEL,RF> BC0;

		BC0 U10_cc(gv0,model,0), U20_cc(gv0,model,1), U30_cc(gv0,model,2);

		typedef Dune::PDELab::CompositeConstraintsParameters<BC0,BC0,BC0> Constraints0;

		Constraints0 constraints0(U10_cc,U20_cc,U30_cc);

		Dune::PDELab::constraints(constraints0,gfs,cg);*/


		//	Construct Linear Operator on FEM Space
		typedef Dune::PDELab::linearelasticity<GV, MODEL, 60> LOP;
		LOP lop(gv, model);


		typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,C,C> GO;
		
		int non_zeros = 81;
    	if(dofel == 8*3) non_zeros = 27;
    	if(dofel == 27*3) non_zeros = 125;
    	
    	MBE mbe(non_zeros); // Maximal number of nonzeroes per row can be cross-checked using buildpattern below
		
		GO go(gfs,cg,gfs,cg,lop,mbe);

		using Dune::PDELab::Backend::native;
    	typedef typename GO::Traits::Jacobian J;
    	J jac(go);
    	auto ss =  mbe.buildPattern<GO,J>(go,jac);
    	std::cout << ss[0] << std::endl;

    	//file << "Time to set up matrix " << timer.elapsed() << "\n";
		//times.push_back(timer.elapsed());
		//timer.reset();

    	//

    	// Make coefficent vector and initialize it from a function
		typedef Dune::PDELab::Backend::Vector<GFS,double> V;
		V x0(gfs,0.0);
		Dune::PDELab::interpolate(initial_solution,gfs,x0);

	    // Set non constrained dofs to zero
	    Dune::PDELab::set_shifted_dofs(cg,0.0,x0);

		//file << "Time to finish interpolation:" << timer.elapsed() << "\n";
		//times.push_back(timer.elapsed());
		//timer.reset();

		Dune::Timer timer_solver;

		model.template solve<GO,V,GFS,C,Constraints,MBE,LOP>(go,x0,gfs,cg,constraints,mbe,lop);

		std::cout << "Check solution does something! ||x||_2 = " <<  x0.two_norm() << std::endl;

		
        Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,0);
        Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,x0);
        vtkwriter.write(model.vtk_displacement_output,Dune::VTK::appendedraw);
        

        std::cout << "=== Calculate Stresses" << std::endl;
       
		calculateStresses<MODEL,V,GV,GFS,MBE>(model,x0,gv,gfs,mbe);
        
		plotProperties<MODEL,V,GV,GFS,MBE>(model,gv,gfs,mbe);

        model.template postprocess<GO,V,GFS,C,MBE,GV>(go,x0,gfs,cg,gv,mbe);


		// Apply linear solver
    	//file << "Total time to Solve:" << timer_solver.elapsed() << "\n",vtk_output;
		//times.push_back(timer_solver.elapsed());


    	//calculateStresses<V,GV,GFS,MBE, PROBLEM,dofel>(x0,gv,gfs,mbe, problem, helper);



	}

private:


	MODEL& model;
	const GV& gv;
	const GV0& gv0;
	const GFS& gfs;
	const IF& initial_solution;



};

}
}

