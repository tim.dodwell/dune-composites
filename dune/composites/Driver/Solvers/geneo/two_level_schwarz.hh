#ifndef TWO_LEVEL_SCHWARZ_HH
#define TWO_LEVEL_SCHWARZ_HH

#if HAVE_ARPACKPP

#include <dune/pdelab/boilerplate/pdelab.hh>

#include <dune/common/timer.hh>

#include "coarsespace.hh"
#include "coarse_prec.hh"

#include <dune/common/deprecated.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/istl/owneroverlapcopy.hh>
#include <dune/istl/solvercategory.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/scalarproducts.hh>
#include <dune/istl/paamg/amg.hh>
#include <dune/istl/paamg/pinfo.hh>
#include <dune/istl/io.hh>
#include <dune/istl/superlu.hh>
#include <dune/istl/umfpack.hh>

#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include <dune/pdelab/backend/solver.hh>
#include <dune/pdelab/backend/istl/vector.hh>
#include <dune/pdelab/backend/istl/bcrsmatrix.hh>

// myModel.template solve<GO,V,GFS,C,Constraints,MBE,LOP>(go,u,gfs,cg,constraints,mbe,lop);

/** Two level overlapping Schwarz preconditioner with exact subdomain solves and coarse solved with direct solver UMFPack
 */
template<class GFS, class M, class X, class Y>
class TwoLevelOverlappingAdditiveSchwarzDC
  : public Dune::Preconditioner<X,Y>
{
public:
  typedef Dune::PDELab::Backend::Native<M> ISTLM;

  typedef Dune::BlockVector<Dune::FieldVector<double,1> > COARSE_V;
  typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > COARSE_M;

  // define the category
  virtual Dune::SolverCategory::Category category() const
  {
    return Dune::SolverCategory::overlapping;
  }

  /*! \brief Constructor.

    Constructor gets all parameters to operate the prec.
    \param A The matrix to operate on.
    \param n The number of iterations to perform.
    \param w The relaxation factor.
  */
  TwoLevelOverlappingAdditiveSchwarzDC (const GFS& gfs_, const M& AF, std::shared_ptr<CoarseSpace<M,X> > coarse_space, bool coarseSpaceActive_ = true)
    : gfs(gfs_),
      solverf(Dune::PDELab::Backend::native(AF),false),
      my_rank(gfs.gridView().comm().rank()),
      coarse_space_(coarse_space)  ,
      coarse_solver_(*coarse_space_->get_coarse_system()),
      coarseSpaceActive(coarseSpaceActive_)
  {}

  /*!
    \brief Prepare the preconditioner.

    \copydoc Preconditioner::pre(X&,Y&)
  */
  virtual void pre (X& x, Y& b)
  { }

  /*!
    \brief Apply the precondioner.

    \copydoc Preconditioner::apply(X&,const Y&)
  */
  double coarse_time = 0.0;
  int apply_calls = 0;

  virtual void apply (X& v, const Y& d)
  {
    // first the subdomain solves
    Y b(d); // need copy, since solver overwrites right hand side
    Dune::InverseOperatorResult result;
    solverf.apply(v,b,result);
    if (!coarseSpaceActive) {

      Dune::PDELab::AddDataHandle<GFS,X> adddh(gfs,v);
      // Just add local results and return in 1-level Schwarz case
      gfs.gridView().communicate(adddh,Dune::All_All_Interface,Dune::ForwardCommunication);

    } else {

      MPI_Barrier(gfs.gridView().comm());
      Dune::Timer timer_setup;

      Dune::InverseOperatorResult result;

      auto coarse_defect = coarse_space_->restrict_defect (d);

      // Solve coarse system
      COARSE_V v0(coarse_space_->basis_size(),coarse_space_->basis_size());

      //coarse_solver_2->apply(v0, *coarse_defect, result);
      coarse_solver_.apply(v0,*coarse_defect,result);

      // Prolongate coarse solution on local domain
      auto coarse_correction = coarse_space_->prolongate_defect (v0);
      v += *coarse_correction;

      coarse_time += timer_setup.elapsed();
      apply_calls++;

      Dune::PDELab::AddDataHandle<GFS,X> result_addh(gfs,v);
      gfs.gridView().communicate(result_addh,Dune::All_All_Interface,Dune::ForwardCommunication);
    }
  }

  /*!
    \brief Clean up.

    \copydoc Preconditioner::post(X&)
  */
  virtual void post (X& x) {
    if (my_rank == 0) std::cout << "Coarse time CT=" << coarse_time << std::endl;
    if (my_rank == 0) std::cout << "Coarse time per apply CTA=" << coarse_time / (double)apply_calls << std::endl;
  }

private:

  const GFS& gfs;
  Dune::UMFPack<ISTLM> solverf;
  std::shared_ptr<CoarseSpace<M,X> > coarse_space_;
  Dune::UMFPack<COARSE_M> coarse_solver_;
  int my_rank;
  bool coarseSpaceActive;
};


/** Two level overlapping Schwarz preconditioner with exact subdomain solves and coarse solve with PCG
 */
template<class GFS, class M, class X, class Y>
class TwoLevelOverlappingAdditiveSchwarzPCGC
  : public Dune::Preconditioner<X,Y>
{
public:
  typedef Dune::PDELab::Backend::Native<M> ISTLM;

  typedef Dune::BlockVector<Dune::FieldVector<double,1> > COARSE_V;
  typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > COARSE_M;


  //Set up coarse solver
  typedef typename Dune::CGSolver<COARSE_V> CSOLVER;
  typedef Dune::CoarsePrec<GFS,COARSE_M,COARSE_V, COARSE_V,M,X,Y> PREC;

  // define the category
  virtual Dune::SolverCategory::Category category() const
  {
    return Dune::SolverCategory::overlapping;
  }

  /*! \brief Constructor.

    Constructor gets all parameters to operate the prec.
    \param A The matrix to operate on.
    \param n The number of iterations to perform.
    \param w The relaxation factor.
  */
  TwoLevelOverlappingAdditiveSchwarzPCGC (const GFS& gfs_, const M& AF, std::shared_ptr<CoarseSpace<M,X> > coarse_space, std::shared_ptr<CoarseSpace<M,X>> coarse_space_small, bool coarseSpaceActive_ = true)
    : gfs(gfs_),
      solverf(Dune::PDELab::Backend::native(AF),false),
      my_rank(gfs.gridView().comm().rank()),
      coarse_space_(coarse_space),
      coarse_space_small_(coarse_space_small),
      prec(gfs,*coarse_space_small_->get_coarse_system(), coarse_space_,coarse_space_small_),
      opa(*coarse_space_->get_coarse_system()),
      coarseSpaceActive(coarseSpaceActive_)
  {
      using Dune::PDELab::Backend::native;
      int verb = 0;
      if(my_rank == 0) verb = 1;
      coarse_solver_2 = std::make_shared<CSOLVER>(opa,prec,1e-3,5000,verb);
  }

  /*!
    \brief Prepare the preconditioner.

    \copydoc Preconditioner::pre(X&,Y&)
  */
  virtual void pre (X& x, Y& b)
  { }

  /*!
    \brief Apply the precondioner.

    \copydoc Preconditioner::apply(X&,const Y&)
  */
  double coarse_time = 0.0;
  int apply_calls = 0;

  virtual void apply (X& v, const Y& d)
  {
    // first the subdomain solves
    Y b(d); // need copy, since solver overwrites right hand side
    Dune::InverseOperatorResult result;
    solverf.apply(v,b,result);

    if (!coarseSpaceActive) {

      Dune::PDELab::AddDataHandle<GFS,X> adddh(gfs,v);
      // Just add local results and return in 1-level Schwarz case
      gfs.gridView().communicate(adddh,Dune::All_All_Interface,Dune::ForwardCommunication);

    } else {

      MPI_Barrier(gfs.gridView().comm());
      Dune::Timer timer_setup;

      Dune::InverseOperatorResult result;

      auto coarse_defect = coarse_space_->restrict_defect (d);

      // Solve coarse system
      COARSE_V v0(coarse_space_->basis_size(),coarse_space_->basis_size());

      coarse_solver_2->apply(v0, *coarse_defect, result);

      // Prolongate coarse solution on local domain
      auto coarse_correction = coarse_space_->prolongate_defect (v0);
      v += *coarse_correction;

      coarse_time += timer_setup.elapsed();
      apply_calls++;

      Dune::PDELab::AddDataHandle<GFS,X> result_addh(gfs,v);
      gfs.gridView().communicate(result_addh,Dune::All_All_Interface,Dune::ForwardCommunication);
    }
  }

  /*!
    \brief Clean up.

    \copydoc Preconditioner::post(X&)
  */
  virtual void post (X& x) {
    if (my_rank == 0) std::cout << "Coarse time CT=" << coarse_time << std::endl;
    if (my_rank == 0) std::cout << "Coarse time per apply CTA=" << coarse_time / (double)apply_calls << std::endl;
  }

private:

  const GFS& gfs;
  Dune::UMFPack<ISTLM> solverf;
  std::shared_ptr<CoarseSpace<M,X> > coarse_space_;
  std::shared_ptr<CoarseSpace<M,X> > coarse_space_small_;
  Dune::MatrixAdapter<COARSE_M,COARSE_V,COARSE_V> opa;
  PREC prec;
  std::shared_ptr<CSOLVER> coarse_solver_2;
  bool coarseSpaceActive;
  int my_rank;
};

#endif

#endif
