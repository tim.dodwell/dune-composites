#ifndef DUNE_GENEO_COARSESPACE_HH
#define DUNE_GENEO_COARSESPACE_HH

template <class M, class X>
class CoarseSpace {

public:
  typedef Dune::BlockVector<Dune::FieldVector<double,1> > COARSE_V;
  typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > COARSE_M;

  virtual std::shared_ptr<COARSE_V> restrict_defect (const X& d) const = 0;

  virtual std::shared_ptr<X> prolongate_defect (const COARSE_V& v0) const = 0;

  virtual std::shared_ptr<COARSE_M> get_coarse_system () = 0;

  virtual int get_local_basis_sizes (int rank) = 0;

  virtual int basis_size() = 0;
  
  virtual int basis_array_offset(int rank) = 0;
};

#endif //DUNE_GENEO_COARSESPACE_HH
