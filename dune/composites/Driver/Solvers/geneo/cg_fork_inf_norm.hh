#ifndef CGSOLVERFORKINFNORM
#define CGSOLVERFORKINFNORM

#include <dune/istl/eigenvalue/arpackpp.hh>

namespace Dune {

  //! \brief conjugate gradient method
  template<class X>
  class CGSolverForkInfNorm : public InverseOperator<X,X> {
  public:
    //! \brief The domain type of the operator to be inverted.
    typedef X domain_type;
    //! \brief The range type of the operator to be inverted.
    typedef X range_type;
    //! \brief The field type of the operator to be inverted.
    typedef typename X::field_type field_type;
    //! \brief The real type of the field type (is the same if using real numbers, but differs for std::complex)
    typedef typename FieldTraits<field_type>::real_type real_type;
    //typedef typename FieldTraits<field_type>::real_type real_type;

    // copy base class constructors
    using InverseOperator<X,X>::InverseOperator;
    /*!
     *       \brief Set up conjugate gradient solver.
     *
     *       \copydoc LoopSolver::LoopSolver(L&,P&,double,int,int)
     */
    template<class L, class P>
    CGSolverForkInfNorm (L& op, P& prec, real_type reduction, int maxit, int verbose) :
    ssp(), _op(op), _prec(prec), _sp(ssp), _reduction(reduction), _maxit(maxit), _verbose(verbose)
    {
      /*static_assert(static_cast<int>(L::category) == static_cast<int>(P::category),
                    "L and P must have the same category!");
      static_assert(static_cast<int>(L::category) == static_cast<int>(SolverCategory::sequential),
                    "L must be sequential!");*/
    }
    /*!
     *       \brief Set up conjugate gradient solver.
     *
     *       \copydoc LoopSolver::LoopSolver(L&,S&,P&,double,int,int)
     */
    template<class L, class S, class P>
    CGSolverForkInfNorm (L& op, S& sp, P& prec, real_type reduction, int maxit, int verbose) :
    _op(op), _prec(prec), _sp(sp), _reduction(reduction), _maxit(maxit), _verbose(verbose)
    {
      /*static_assert(static_cast<int>(L::category) == static_cast<int>(P::category),
                    "L and P must have the same category!");
      static_assert(static_cast<int>(L::category) == static_cast<int>(S::category),
                    "L and S must have the same category!");*/
    }

    /*!
       \brief Apply inverse operator.

       \copydoc InverseOperator::apply(X&,Y&,InverseOperatorResult&)

       \note Currently, the CGSolver aborts when a NaN or infinite defect is
             detected.  However, -ffinite-math-only (implied by -ffast-math)
             can inhibit a result from becoming NaN that really should be NaN.
             E.g. numeric_limits<double>::quiet_NaN()*0.0==0.0 with gcc-5.3
             -ffast-math.
     */
    virtual void apply (X& x, X& b, InverseOperatorResult& res){
      std::cerr << "meep. don't call me." << std::endl;
    }

    /*!
       \brief Apply inverse operator.

       \copydoc InverseOperator::apply(X&,Y&,InverseOperatorResult&)

       \note Currently, the CGSolver aborts when a NaN or infinite defect is
             detected.  However, -ffinite-math-only (implied by -ffast-math)
             can inhibit a result from becoming NaN that really should be NaN.
             E.g. numeric_limits<double>::quiet_NaN()*0.0==0.0 with gcc-5.3
             -ffast-math.
     */
    virtual void apply (X& x, X& b, X& reference_solution_cg, X& reference_solution, InverseOperatorResult& res)
    {
      using std::isfinite;
      typedef Dune::PDELab::istl::ParallelHelper<typename X::GridFunctionSpace> PIH;
      PIH pihf(x.gridFunctionSpace());

      res.clear();                  // clear solver statistics
      Timer watch;                // start a timer
      _prec.pre(x,b);             // prepare preconditioner
      _op.applyscaleadd(-1,x,b);  // overwrite b with defect

      X p(x);              // the search direction
      X q(x);              // a temporary vector

      X solution_diff = x;
      solution_diff -= reference_solution_cg;
      pihf.maskForeignDOFs(solution_diff);
      double local_diff_norm = solution_diff.infinity_norm();
      double global_diff_norm = 0.0;
      MPI_Allreduce(&local_diff_norm, &global_diff_norm, 1, MPI_DOUBLE, MPI_MAX, x.gridFunctionSpace().gridView().comm());
      double initial_global_diff_norm = global_diff_norm;

      double local_ref_norm = reference_solution.infinity_norm();
      double global_ref_norm;
      MPI_Allreduce(&local_ref_norm, &global_ref_norm, 1, MPI_DOUBLE, MPI_MAX, x.gridFunctionSpace().gridView().comm());


      if (!isfinite(global_diff_norm)) // check for inf or NaN
      {
        if (_verbose>0)
          std::cout << "=== CGSolver: abort due to infinite or NaN initial defect"
                    << std::endl;
        DUNE_THROW(SolverAbort, "CGSolver: initial defect=" << global_diff_norm
                   << " is infinite or NaN");
      }

      if (global_diff_norm<1E-30)    // convergence check
      {
        res.converged  = true;
        res.iterations = 0;               // fill statistics
        res.reduction = 0;
        res.conv_rate  = 0;
        res.elapsed=0;
        if (_verbose>0)                 // final print
          std::cout << "=== rate=" << res.conv_rate
                    << ", T=" << res.elapsed << ", TIT=" << res.elapsed
                    << ", IT=0" << std::endl;
        return;
      }

      if (_verbose>0)             // printing
      {
        std::cout << "=== CGSolver" << std::endl;
        if (_verbose>1) {
          this->printHeader(std::cout);
          this->printOutput(std::cout,real_type(0),global_diff_norm);
        }
      }

      // some local variables
      //real_type def=def0;   // loop variables
      field_type rho,rholast,lambda,alpha,beta;

      // determine initial search direction
      p = 0;                          // clear correction
      _prec.apply(p,b);               // apply preconditioner
      rholast = _sp.dot(p,b);         // orthogonalization

      // Remember alpha and beta values for condition estimate
      std::vector<real_type> alphas(0);
      std::vector<real_type> betas(0);
      std::vector<std::shared_ptr<X>> ps(0);

      // the loop
      int i=1;
      double global_diff_norm_old = global_diff_norm;
      for ( ; i<=_maxit; i++ )
      {
        global_diff_norm_old = global_diff_norm;

        if (configuration.get<bool>("OrthogonalizeCG",false)) {

          for (auto previous_p : ps) {
            X correction = x;
            _op.apply (*previous_p, correction);

            double scp = _sp.dot(p, correction) / _sp.dot(*previous_p, correction);
            correction = *previous_p;
            correction *= scp;
            p -= correction;
          }
          ps.push_back(std::make_shared<X>(p));

        }

        // minimize in given search direction p
        _op.apply(p,q);             // q=Ap
        alpha = _sp.dot(p,q);       // scalar product
        lambda = rholast/alpha;     // minimization
        alphas.push_back(lambda);
        x.axpy(lambda,p);           // update solution
        b.axpy(-lambda,q);          // update defect

        // Update infinity_norm(solution - reference_solution_cg)
        solution_diff = x;
        solution_diff -= reference_solution_cg;
        pihf.maskForeignDOFs(solution_diff);
        local_diff_norm = solution_diff.infinity_norm();
        //global_diff_norm;
        MPI_Allreduce(&local_diff_norm, &global_diff_norm, 1, MPI_DOUBLE, MPI_MAX, x.gridFunctionSpace().gridView().comm());

        if (_verbose>1)             // print
          this->printOutput(std::cout,real_type(i),global_diff_norm,global_diff_norm_old);

        if (!isfinite(global_diff_norm)) // check for inf or NaN
        {
          if (_verbose>0)
            std::cout << "=== CGSolver: abort due to infinite or NaN defect"
                      << std::endl;
          DUNE_THROW(SolverAbort,
                     "CGSolver: defect=" << global_diff_norm << " is infinite or NaN");
        }

        if (global_diff_norm < global_ref_norm*_reduction || global_diff_norm<1E-30)    // convergence check
        {
          res.converged  = true;
          break;
        }

        // determine new search direction
        q = 0;                      // clear correction
        _prec.apply(q,b);           // apply preconditioner
        rho = _sp.dot(q,b);         // orthogonalization
        beta = rho/rholast;         // scaling factor
        betas.push_back(beta);
        p *= beta;                  // scale old search direction
        p += q;                     // orthogonalization with correction
        rholast = rho;              // remember rho for recurrence
      }

      //correct i which is wrong if convergence was not achieved.
      i=std::min(_maxit,i);

#if HAVE_ARPACKPP
      if (configuration.get<bool>("ConditionEstimate",false)) {

        // Build T matrix
        MAT T(i, i, MAT::row_wise);

        for (auto row = T.createbegin(); row != T.createend(); ++row) {
          if (row.index() > 0)
            row.insert(row.index()-1);
          row.insert(row.index());
          if (row.index() < T.N() - 1)
            row.insert(row.index()+1);
        }
        for (int row = 0; row < i; ++row) {
          if (row > 0) {
            T[row][row-1] = std::sqrt(betas[row-1]) / alphas[row-1];
          }

          T[row][row] = 1.0 / alphas[row];
          if (row > 0) {
            T[row][row] += betas[row-1] / alphas[row-1];
          }

          if (row < i - 1) {
            T[row][row+1] = std::sqrt(betas[row]) / alphas[row];
          }
        }

        // Estimate largest and smallest eigenvalue of T matrix
        Dune::ArPackPlusPlus_Algorithms<MAT, VEC> arpack(T);

        double eps = 0.0;
        VEC eigv;
        double min_eigv, max_eigv;
        arpack.computeSymMinMagnitude (eps, eigv, min_eigv);
        arpack.computeSymMaxMagnitude (eps, eigv, max_eigv);

        if (_verbose > 0) {
          std::cout << "Min eigv: " << min_eigv << std::endl;
          std::cout << "Max eigv: " << max_eigv << std::endl;
          std::cout << "Condition: " << max_eigv / min_eigv << std::endl;
        }

      }
#endif

      if (_verbose==1)                // printing for non verbose
        this->printOutput(std::cout,real_type(i),global_diff_norm);

      _prec.post(x);                  // postprocess preconditioner
      res.iterations = i;               // fill statistics
      res.reduction = static_cast<double>(global_diff_norm/initial_global_diff_norm);
      res.conv_rate  = static_cast<double>(pow(res.reduction,1.0/i));
      res.elapsed = watch.elapsed();

      if (_verbose>0)                 // final print
      {
        std::cout << "=== rate=" << res.conv_rate
                  << ", T=" << res.elapsed
                  << ", TIT=" << res.elapsed/i
                  << ", IT=" << i << std::endl;
      }
    }

    /*!
     *  \brief Apply inverse operator with given reduction factor.
     *
     *  \copydoc InverseOperator::apply(X&,Y&,double,InverseOperatorResult&)
     *
     *  \note Currently, the CGSolver aborts when a NaN or infinite defect is
     *        detected.  However, -ffinite-math-only (implied by -ffast-math)
     *        can inhibit a result from becoming NaN that really should be NaN.
     *        E.g. numeric_limits<double>::quiet_NaN()*0.0==0.0 with gcc-5.3
     *        -ffast-math.
     */
    virtual void apply (X& x, X& b, double reduction,
                        InverseOperatorResult& res)
    {
      real_type saved_reduction = _reduction;
      _reduction = reduction;
      (*this).apply(x,b,res);
      _reduction = saved_reduction;
    }
  private:
    typedef Dune::BCRSMatrix<Dune::FieldMatrix<real_type,1,1> > MAT;
    typedef Dune::BlockVector<Dune::FieldVector<real_type,1> > VEC;

  protected:
    SeqScalarProduct<X> ssp;
    LinearOperator<X,X>& _op;
    Preconditioner<X,X>& _prec;
    ScalarProduct<X>& _sp;
    real_type _reduction;
    int _maxit;
    int _verbose;
  };

}

#endif
