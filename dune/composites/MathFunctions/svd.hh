#include "twoNorm.hh"

template <typename M>
void svd(const M& A, M& U, M& S, M& V)
{
	// -----------------------------------------------------------------------------
	// svd computes the single value decompositon of real matrix A (n by n) so that
	// 
	// 	A = U * S * V'
	// 
	//  Dr Timothy Dowell, University of Exeter, t.dodwell@exeter.ac.uk
	//  3rd October 2015.
	// -----------------------------------------------------------------------------
	const int m = A.rows();
	const int n = A.cols();

	// Various checks to see U, S and V are the right sizes
	int steps = 0; // Counter for number of steps
	int max_steps = 100; // Max number of steps
	double tol = 1e-5; // Tolerance of value of maximum off diagonal entry
	double error = 1.0; // Initialise error greater than tol
	double singval = 0.0;
	
	double alpha, beta, gamma, xi, t , c, s, T, tmp, sgn; // Initialise various double variables
	
	U = A;
	V = 0.0;
	S = 0.0;
	
	for (int i = 0; i < n; i++){	V[i][i] = 1.0;	} // Set V = I_{n x n}
	
	while (error > tol || steps > max_steps)
	{
		steps += 1;
		for (int j = 1; j < n; j++)
		{
			for (int k = 0; k < j; k++)
			{
				// Compute Sub Matrix [alpha, gamma ; gamma, beta] (k,j) submatrix of U' * U
				alpha = 0.0; beta = 0.0; gamma = 0.0;
				for (int i = 0; i < n; i++) {
					alpha += U[i][k] * U[i][k];
					beta  += U[i][j] * U[i][j];
					gamma += U[i][k] * U[i][j];
				}
				
                tmp = abs(gamma) / (sqrt(alpha * beta));
                
                if (error < tmp)
                {
                    error = tmp;
                }
                
				// Compute the Jacobi rotation which diagonalises sub-matrix
				
				xi = (beta - alpha) / (2.0 * gamma);
				
                if (xi > 0){sgn = 1.0;}
                if (xi < 0){sgn = -1.0;}
                if (xi == 0){sgn = 0.0;}
				
				
				t = sgn / (abs(xi) + sqrt(1 + xi * xi));
				
				c = 1.0 / sqrt(1.0 + t * t);
				
				s = c * t;
				
				
				for (int i = 0; i < n; i++)
				{
					// Update Columns k and j of U
					T = U[i][k];
					U[i][k] = c * T - s * U[i][j];
					U[i][j] = s * T + c * U[i][j];
					
					// Update Columns k and j of V
					T = V[i][k];
					V[i][k] = c * T - s * V[i][j];
					V[i][j] = s * T + c * V[i][j];
				}
					
			} // end for each k
		} // end for each j
		
	
	} // end while
	
    
	for (int i = 0; i < n; i++)
	{
		singval = twoNorm<M>(U,i); // Compute L2 norm of the i-th column of U
		for (int j = 0; j < n; j++)
		{
			U[j][i] = U[j][i] / singval; // Normalise column
		} // end for each j in i-th column		
	} // end i (each column)
	
} // End function svd (no return value)








