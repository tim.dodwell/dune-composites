#ifndef CREATE_STRUCTUREMODEL__HH
#define CREATE_STRUCTUREMODEL__HH

#include <dune/composites/Setup/Cijkl.hh>
#include <dune/composites/Geometry/readCsv.hh>
#include <dune/composites/Setup/Region.hh>

#include <dune/composites/Driver/Solvers/solver_info.hh>
#include <dune/composites/Driver/Solvers/solvers.hh>


using namespace std;


namespace Dune{

namespace Composites{

//!  structuredGridModel class. 
/*!
  structuredGridModel provides a base class for 3D structure grid models built on 
  using Dune::YaspGrid and Dune::GeometryGrid. The class provides all default functions for definition of a user defined model.
*/

class baseStructuredGridModel{

public:

    typedef Dune::YaspGrid<3,Dune::TensorProductCoordinates<double,3>> YGRID;
    typedef YGRID::LeafGridView YGV;
    typedef Dune::FieldVector<double,3> FieldVec;
    typedef Dune::FieldVector<double,6> Tensor2;
    typedef Dune::FieldMatrix<double,6,6> Tensor4;

    //! Size of overlap between subdomains for domain decomposition
    int overlap;

    std::array<std::vector<double>,3>  coords;
    
     //! Array which stores periodicity of grid in each of the 3 dimensions, default value are set to all false.
    std::bitset<3> periodic;

    Dune::MPIHelper& helper;

    std::vector<Tensor4> baseTensor;

    //! Number of Different types of materials defined for applications
    int numMaterials; 
   
    std::vector<Material> myMaterials; //! std::vector containing information of each material defined

    std::vector<Region> R; //! std::vector containing information of each Region
 
    std::vector<int> elemIndx2PG; //! std::vector containing information of each material defined

    std::vector<double> rot123; //! Vector defining rotation of ply in material coordinates.

    std::vector<Tensor4> C; //! Vector storing elastic tensor within each element

    bool verbosity; //! Variable controls level of output to interface

    int solver_type; //! Variable which controls solver type (UMFPack for sequential and GENEO for parallel)

    int solver_tolerance; //! Tolerance of Solver (ignored for a direct solver)

    int Krylov_Verb; //! Variable Controlling Verbosity of Krylov Solver

    int Krylov_Max_It; //! Variable define max iterations

    double Krylov_Tol; //! Variable Krylov Tolerance

    int solver_verb;

    std::vector<double> stackSeq;

    std::vector<std::vector<double>> sig;

    bool stress_Plot; //! Variable if stress is plotted out in vtk format

    bool sigResized;

    std::string vtk_displacement_output;
    std::string vtk_stress_output;
    std::string vtk_properties_output;

    int intOrder; //! Gaussian order of integration (default value is 5)

    std::vector<int> nel; //! Number of elements in each direction

    bool isParallel; //! Boolean variable which indicates if run is parallel or not, this later will allow to run multiple solves in sequential mode even though helper.size() > 1


    bool setUp_Required;

    SolverInfo solverParameters;


    //! The main constructor
    /*!
      Constructor of structuredGridModel, requiring only the MPI Helper defined by dune
    */
    baseStructuredGridModel(Dune::MPIHelper& helper_, bool GenEO = false):helper(helper_){

        isParallel = false;
        if (helper.size() > 1){
            isParallel = true;
        }

        solver_type = config.get<int>("solver.solver_type", 1);
        Krylov_Max_It = config.get<int>("solver.Krylov_Max_It", 500);
        Krylov_Verb = config.get<int>("solver.Krylov_Verb", 0);
        Krylov_Tol = config.get<double>("solver.Krylov_Tol", 1e-4);
        intOrder = config.get<int>("fe.intOrder",5);

        solver_verb = 1;

        sigResized = false;

        stress_Plot = true;

        vtk_displacement_output = "baseModel_displacement";
        vtk_stress_output = "baseModel_stress";
        vtk_properties_output = "baseModel_properties"; 

        setUp_Required = true;


    };

    void inline setup(){
        // setup - 
        LayerCake();
        setUpMaterials();
        computeElasticTensors();
    }


    void inline sizeStressVector(int nel){
        sig.resize(nel);
        for (int i = 0; i < sig.size(); i++){
            sig[i].resize(6);
        }
    }

    void inline setStress(int id, Dune::FieldVector<double,6>& element_stress){

        for (int i = 0; i < 6; i++){

            sig[id][i] = element_stress[i];
        }
    }

    int inline getIntOrder() const {return 5;}

    int inline whichRegion(const FieldVec& x){
        double Lx = 0.0;
        int ans = 0;
        bool flag = false;
        for (int k = 0; k < R.size(); k++){
            Lx += R[k].L[0];
            if (x[2] < Lx && flag == false){ ans = k; flag = true; }
        }
        return ans;
    }

    int inline whichLayer(FieldVec& x, int r){
        assert(r < R.size());
        bool flag = false;
        double z = 0.0;
        int ans = 0;
        for (int k = 0; k < R[r].layers.size(); k++){
            z += R[r].layers[k].getThickness();
            if (x[2] < z && flag == false){ ans = k; flag = true; }
        }

        return ans;
    }





    void inline evaluateHeat(Tensor2& f,int id) const{
        f = 0.0;
       /* if(config.get<bool>("residualHeat",false)){
        double deltaT = -160.; //temperature difference
            double alpha11 = config.get<double>("alpha11", -.342)*1e-6;
            double alpha22 = config.get<double>("alpha22", 25.8)*1e-6;
            double alpha33 = config.get<double>("alpha33", 25.8)*1e-6;
            double alpharesin = config.get<double>("alpharesin",25.8)*1e-6;
            if(resin){
                f[0] = alpharesin*deltaT;
                f[1] = alpharesin*deltaT;
                f[2] = alpharesin*deltaT;
            }
            else{
                f[0] = alpha11*deltaT;
                f[1] = alpha22*deltaT;
                f[2] = alpha33*deltaT;
            }
        }*/
    }

    inline void evaluateWeight(Dune::FieldVector<double,3>& f, int id) const{
        int pg = elemIndx2PG[id];
        f[2] = - gravity * myMaterials[pg].density;
    }



    inline void evaluateNeumann(const Dune::FieldVector<double,3> &x,Dune::FieldVector<double,3>& h,const  Dune::FieldVector<double,3>& normal) const{
        h = 0.0;
    }

    //! \fn A member function which sets the solver type
    void inline setSolver(){
        solver_type = 0; // UmfPack as default solver
        if (helper.size() > 1){ // Parallel Solver
            solver_type = 1;
        }

    }

    int inline getElements(int i){ return nel[i]; }

    //! \fn A member taking which constructs the base layered laminate in untransformed coordinates

    void inline LayerCake(){
        
        // Defines Default values for Base Class - can be overwritten by defining your own derived class  
        

        // Define Periodic Boundary Conditions
        periodic[0] = false; periodic[1] = false; periodic[2] = false;
        
        // Uniform Tensor Product Grid on [0,1]^3
        std::vector<int> nel(3);

        nel[0] = 5; nel[1] = 5; nel[2] = 5;
        
        for (int i = 0; i < 3; i++){
            double h = 1. / nel[i];
            coords[i].resize(nel[i] + 1);
            coords[i][0] = 0.0;
            for (int j = 1; j <= nel[i]; j++){
                coords[i][j] = coords[i][j-1] + h;
            }
        }
    }

    void inline LayerCakeFromFile(string pathToCsv){


        ifstream csvFile(pathToCsv);

        if(! csvFile) {
            throw std::runtime_error("Could not open file " + pathToCsv);
        }

        string item;

        int row = 0;

        int numRegions = 0;

        int j = -1; // Initialise Region Counter

        int maxPly = 0;

            string line;
            getline(csvFile, line);
            istringstream lineStream(line);

            for (int jj = 0; jj < 2; jj++){
                // Obtain number of regions
                getline(lineStream, item, ',');
                std::cout << item << std::endl;
            }


            numRegions = atof(item.c_str());
            std::cout << "Number of Regions = " << numRegions << std::endl;
            R.resize(numRegions);

            //
  
            getline(csvFile, line);
            

            istringstream lS2(line);

            std::cout << line << std::endl;

            // Obtain max number of plies
            getline(lS2, item, ',');
            getline(lS2, item, ',');
            maxPly = atof(item.c_str());
            std::cout << "maxPly = " << maxPly << std::endl;

            getline(csvFile, line);

            for (int jj = 0; j < 4; j++){
                 istringstream lS3(line);
                 // Obtain max number of plies
                  getline(lS3, item, ',');
                if (j > 0){
                   periodic[jj-1] = atof(item.c_str());
                }
            }
            std::cout << "periodic = " << periodic << std::endl;
            
            std::cout << "Reading Each Region Now!" << std::endl;

            for(int j = 0; j < numRegions; j++){ // For each regions

                std::cout << "Region = " << j << std::endl;

                R[j].setNumLayers(maxPly);

                getline(csvFile, line); getline(csvFile, line);

                int numParameters = 1;

                istringstream lS4(line);

                std::cout << line << std::endl;

                for (int ii = 0; ii < numParameters; ii++){
                    getline(lS4, item, ','); 
                    if (ii == 0){ 
                        R[j].setType(atof(item.c_str()));
                        numParameters = R[j].numParameters;
                    }
                    else{

                        R[j].setParameter(ii-1,atof(item.c_str()));
                    }
                } // For each parameter


                for (int k = 0; k < maxPly; k++){

                    std::cout << "Ply Number = " << k << std::endl;

                    getline(csvFile, line); // Obtain next row

                    std::cout << line << std::endl;

                    int temp_material = 0;

                    

                    istringstream lS5(line);

                    getline(lS5,item,',');

                    R[j].layers[k].setElements(atof(item.c_str()));

                    std::cout << "Elements " << atof(item.c_str()) << std::endl;

                    getline(lS5,item,',');

                            
                    std::cout << "Material " << atof(item.c_str()) << std::endl;

                    R[j].layers[k].setMaterial(atof(item.c_str()) );

                    getline(lS5,item,',');

                    R[j].layers[k].setThickness(atof(item.c_str()));

                    std::cout << "Thickness " << atof(item.c_str()) << std::endl;

                    getline(lS5,item,',');
                         
                    R[j].layers[k].setOrientation(atof(item.c_str()));
                    
                    std::cout << "Orientation " << atof(item.c_str()) << std::endl;

                } // for each ply


            } // For each region

    }



void inline GeometryBuilder(){
    

    // Builds Overall Geometry and Mesh from a set of regions

    std::cout << "=== Building Geometry" << std::endl;


    Dune::FieldVector<double,3> origin(0.0); // Define Origin, the default of this will be set to [0.0,0.0,0.0] 


    // Compute number of elements in x and y. Since structures are current prismatic and tensor product grid y elements must be the same so we check this

    int numRegions = R.size();

    std::cout << numRegions << std::endl;

    int nelx = 0;
    int nely = R[0].nel[1];

    for (int i = 0; i < numRegions;i++){

        nelx += R[i].nel[0];

        assert(R[i].nel[1] == nely); //"Overall tensor product grid y must be equal across all regions, check geometry input file");

    }

    std::cout << "Elements in x direction " << std::endl;

    // === For first case lets think about uniform grids, without any grading

    //std::vector<std::vector<double>> coords(3);

    coords[0].resize(nelx + 1);

    coords[0][0] = origin[0];

    int k = 1; // initialise counter

    for (int i = 0; i < numRegions; i++){

        double hx = R[i].L[0] / R[i].nel[0]; // This makes grid uniform across a region TO DO:: Update for non-uniform grids

        for (int j = 0; j < R[i].nel[0]; j++){

            coords[0][k] = coords[0][k-1] + hx;

            k++; // Increment Counter in the x-direction

        }
    }

    // === Prismatic mesh in y

    coords[1].resize(nely + 1);

    coords[1][0] = origin[0];

    double hy = R[0].L[1] / R[0].nel[1];

    for (int j = 1; j < R[0].nel[1] + 1; j++){
        coords[1][j] = coords[1][j-1] + hy;
    }

    // Build Coordinates in z

    // For now let's assume uniform thickness (not necessary though)
    int nelz = 0;
    for (int i = 0; i < R[0].layers.size(); i++){
        // for each layer
        nelz += R[0].layers[i].getElements();
    }

    coords[2].resize(nelz + 1);

    coords[2][0] = origin[2];

    int e = 0;

    for (int i = 0; i < R[0].layers.size(); i++){
        double hz = R[0].layers[i].getThickness() / R[0].layers[i].getElements();
        for (int k = 0; k < R[0].layers[i].getElements(); k++){
            e++;
            coords[2][e] = coords[2][e-1] + hz;
        }
    }


}


int inline getSizeCoords(int i){ return coords[i].size(); }

void setUpMaterials(){

            numMaterials = 2;

            myMaterials.resize(numMaterials);

            // Material I: Isotropic Resin

            myMaterials[0].setType(0); // Isotropic
            myMaterials[0].setProp(0,10000.); // - E,  Young's Modulus
            myMaterials[0].setProp(1,0.35); // - nu,   Poisson Ratio       
            myMaterials[0].setDensity(1.57e-5); // - rho,  Density  kg/mm^2

            // Material II: Orthotropic, Composite Ply - AS4/8552

            myMaterials[1].setType(1); // Orthotropic Composite
            myMaterials[1].setProp(0,162000.); // E11
            myMaterials[1].setProp(1,10000.); // E22
            myMaterials[1].setProp(2,10000.); // E33
            myMaterials[1].setProp(3,0.35); // nu12
            myMaterials[1].setProp(4,0.35); // nu13
            myMaterials[1].setProp(5,0.49); // nu23
            myMaterials[1].setProp(6,5200.); // G12
            myMaterials[1].setProp(7,5200.); // G13
            myMaterials[1].setProp(8,3500.); // G23
            myMaterials[1].setDensity(1.57e-5); // - rho, Density kg/mm^2

    }


    double inline FailureCriteria(std::vector<double>& stress) const{

        return stress[0];

    }

    template <class GV, class GV2>
    void computeTensorsOnGrid(const GV& gv, const GV2& gv2){

        typedef typename GV::Traits::template Codim<0>::Iterator ElementIterator;
        
        auto is = gv.indexSet();

        C.resize(gv.size(0));

        // ================================================================================
        //  << a >>     Compute Elasticity Tensors for Isotropic & Orthropic Cases
        // ================================================================================
        auto my_rank = gv.comm().rank();

       /* CalCijkl<MAT_PROP> field(mat_prop, my_rank);*/
        // ================================================================================
        //  << b >>     Calculate Cijkl[id] in each element
        // ================================================================================
        auto it2 = gv2.template begin<0>();
        
        for (ElementIterator it = gv.template begin<0>(); it!=gv.template end<0>(); ++it)
        { // loop through each element
            int id = is.index(*it); // Get element id
            int pg = elemIndx2PG[id];
            C[id] = localTensorRotation(baseTensor[pg],rot123[id]);
            it2++;
        } // End for-loop of each element */
    }; // End Constructor

    FieldVec inline gridTransformation(const FieldVec & x) const{ 
        // Default grid transformation is the identity map
        FieldVec y = x;
        return y;
    }

    std::array<int, 3> inline GridPartition(){
      // Function returns default partition
      std::array<int, 3> Partitioning;
      std::fill(Partitioning.begin(),Partitioning.end(), 1);
      return Partitioning;
    }

    void inline setPG(const YGV& ygv){
        elemIndx2PG.resize(ygv.size(0));
        rot123.resize(ygv.size(0));
        // Loop Through Each element and Assign Physical Group and Rotation
        for (const auto& eit : elements(ygv)){
            int id = ygv.indexSet().index(eit);
            auto point = eit.geometry().center();
            //elemIndx2PG[id] = setPhysicalGroup(point);
            // Which Layer?
            int theRegion = whichRegion(point);
            int theLayer = whichLayer(point,theRegion);
            elemIndx2PG[id] = R[theRegion].layers[theLayer].getMaterial();
            rot123[id] = R[theRegion].layers[theLayer].getOrientation();
        }
    }

    int inline getMaterialType(int whichMaterial){
        assert(whichMaterial < numMaterials);
        return myMaterials[whichMaterial].Type;
    }


    int inline setPhysicalGroup(FieldVec& x){
        return 0;
    }

    void inline computeElasticTensors(){
        // Calculate Elastic Tensors for each material
        baseTensor.resize(numMaterials);
        for (int i = 0; i < numMaterials; i++){
            baseTensor[i] = Cijkl(myMaterials[i]);
        }
    }

    int inline refineBaseGrid(int i = 0){
        return 0;
    }

    Tensor4 localTensorRotation(Tensor4& baseC, double theta) const
    {
        Tensor4 locC = baseC;
        Tensor4 R2(0.0) ,   R2T(0.0);
        // Rotation theta about x2;
        double c = cos(theta * M_PI/180.0);
        double s = sin(theta * M_PI/180.0);
        Dune::FieldMatrix<double,3,3> A = {{c,s,0},{-s,c,0},{0,0,1}};
        R2 = getMatrix(A);
        transpose<Tensor4,6>(R2,R2T);
        locC.leftmultiply(R2);
        locC.rightmultiply(R2T);
        return locC;
    } // end eval

    //void inline evaluateTensor(int id, Tensor4& C_){    C_ = C[id];    }


    Tensor4 inline getElasticTensor(int id) const { return C[id]; }


    std::vector<double> returnTheta(const FieldVec& x) const{
            FieldVec y, yph;
            double h = 1e-10;
            FieldVec xph = x; xph[0] += h/2.;
            FieldVec xmh = x; xmh[0] -= h/2.;
            y = gridTransformation(xph);
            yph = gridTransformation(xmh);
            auto angle = atan2((yph[2]-y[2])/h,(yph[0]-y[0])/h);
            std::vector<double> theta(3);
            theta[1] = angle;
            return theta;
    }

    bool inline isDirichlet(FieldVec& x, int dof){
        // Default isDirichlet function, returning homogeneous Dirichlet boundary conditioners
        bool answer = false;
        if (x[0] < 1e-6){   answer = true;  }
        return answer;
    }

    double inline getMaterialTypeFromElement(int id) const{
        int pg = elemIndx2PG[id];
        return myMaterials[pg].Type;
    }

    double inline getOrientation(int id) const {return rot123[id];}

    double inline evaluateDirichlet(const FieldVec& x, int dof) const{
        // Return homogeneous boundary conditions
        return 0.0;
    }

    template<class GO, class U, class GFS, class C,class MBE, class GV>
    void inline postprocess(const GO& go, U& u, const GFS& gfs, const C& cg, const GV gv, MBE& mbe){

        // Default does nothing
        
    }

    template<class GO, class V, class GFS, class C, class CON, class MBE, class LOP>
    bool inline solve(GO& go, V& u, const GFS& gfs, C& cg, CON& constraints, MBE& mbe, LOP& lop){

        bool flag = true;

        if(gfs.gridView().comm().size() > 1){ // == DEFAULT AVAILABLE PARALLEL SOLVERS


            if(solverParameters.preconditioner == "GenEO"){

                typedef Dune::Composites::CG_GenEO<V,GFS,GO,LOP,CON,MBE> GenEO;

                GenEO mysolver(u,gfs,lop,constraints,mbe,solverParameters,helper);

                mysolver.apply();

                //CG_GenEO<V,GFS,GO,LOP,CON,MBE,SolverInfo>(u,gfs,lop,constraints,mbe,solverParameters,helper);

            }
            else{

                typedef Dune::PDELab::ISTLBackend_OVLP_CG_UMFPack<GFS, C> PAR_UMF;  //CG with UMFPack
                    PAR_UMF ls(gfs,cg,Krylov_Max_It,solverParameters.verb);
                Dune::PDELab::StationaryLinearProblemSolver<GO,PAR_UMF,V> slp(go,ls,u,Krylov_Tol);
                    slp.apply();
                }
            

        }

        else{ // == SEQUENTIAL SOLVER

            #if HAVE_SUITESPARSE
                typedef Dune::PDELab::ISTLBackend_SEQ_UMFPack SEQ_UMF;
                SEQ_UMF ls(solver_verb); //Direct solver
                Dune::PDELab::StationaryLinearProblemSolver<GO, SEQ_UMF,V> slp(go,ls,u,1e-6); // note tolerance is ignored as Direct solver
                slp.apply();
            #else
                std::cout << "SEQ_UMFPack solver not available, please install SuiteSparse" << std::endl;
                flag = false;
        #endif



        }

        return flag;


    }


private:

    double gravity = 9.80665;

    bool GenEO;




};

}
}

#endif
