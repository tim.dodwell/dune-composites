#ifndef MATERIAL__HH
#define MATERIAL__HH

namespace Dune{

namespace Composites{


class Material{

public:

	Material(){}

	int Type;
	std::vector<double> matprop;
	double density; 

	void inline setType(int t){
		Type = t;
		if (t == 0){ matprop.resize(2);	} // Linear Isotropic Material  - E and nu
		else if(t == 1){ matprop.resize(9);} // Linear Orthotropic Material 
		else{ matprop.resize(21); } // Linear Fully Anisotropic Material 
	}

	void inline setProp(int i, double val){
		assert(i < matprop.size());
		matprop[i] = val;
	}

	void inline setDensity(double val){density = val;}


private:


};

}
}

#endif
