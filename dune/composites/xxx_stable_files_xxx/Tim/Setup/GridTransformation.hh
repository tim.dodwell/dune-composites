#ifndef DUNE_COMPOSITIES_GRIDTRANSFORMATION__HH
#define DUNE_COMPOSITIES_GRIDTRANSFORMATION__HH


namespace Dune{

  namespace Composites{

    //Grid Transformation
    template <int dim, class MODEL>
    class GridTransformation
    : public Dune :: AnalyticalCoordFunction< double, dim, dim, GridTransformation <dim,MODEL> >{
      typedef GridTransformation This;
      typedef Dune :: AnalyticalCoordFunction< double, dim, dim, This > Base;

    public:
      typedef typename Base :: DomainVector DomainVector;
      typedef typename Base :: RangeVector RangeVector;

      GridTransformation(MODEL& model_, int my_rank_) : model(model_), giveOutput(1), my_rank(my_rank_){  }

      void evaluate ( const DomainVector &x, RangeVector &y ) const { y = model.gridTransformation(x); }
      
    private:
      mutable int giveOutput;
      MODEL model;
      int my_rank;
    };

  }
}

#endif