//
//  twoNorm.hh
//  
//
//  Created by tjd20 on 05/10/2015.
//
//

#ifndef _twoNorm_hh
#define _twoNorm_hh

template<typename X>
double twoNorm(const X& x , int col)
{
    // twoNorm returns a double with the L2 norm of the col-th column of matrix X
    double norm = 0.0;
    int n = x.rows();
    for (int i = 0; i < n; i++){
        norm += x[i][col] * x[i][col];
    }
    norm = sqrt(norm);
    return norm;
}

#endif
