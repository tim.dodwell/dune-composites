#ifndef Calculate_Cijkl_hh
#define Calculate_Cijkl_hh

// Cijkl is a class which computes Elasticity Tensor on each element and contains all required functions

#include "../MathFunctions/transpose.hh"

template<typename MAT_PROP>
class CalCijkl{
    /* The Class Compute Cijkl - Calculates the Elastic Tensor sij = Cijkl for each element */
    typedef typename Dune::FieldMatrix<double,6,6> M;
    
public:
    // Constructor - Computes Cijkl in material coordinates for each material type
    CalCijkl(const MAT_PROP& mat_prop, int my_rank) : C(9){
        // For each material
        for (int i = 0; i < 9; i++){
            //isotropic material
            if (mat_prop.materialType[i] == 0){
                double E = mat_prop.matprop[i][0];
                double nu = mat_prop.matprop[i][1];

                double tmp = E / ( (1.0 + nu) * ( 1.0 - 2.0 * nu ) );
                double G = E / (2.0 * (1 + nu));

                C[i][0][0] = ( 1.0 - nu ) * tmp;	C[i][0][1] = nu * tmp; C[i][0][2] = nu * tmp;
                C[i][1][0] = nu * tmp; C[i][1][1] = ( 1.0 - nu ) * tmp; C[i][1][2] = nu * tmp;
                C[i][2][0] = nu * tmp; C[i][2][1] = nu * tmp; C[i][2][2] = ( 1.0 - nu ) * tmp;
                C[i][3][3] = G;
                C[i][4][4] = G;
                C[i][5][5] = G;
            //if(my_rank == 0){ std::cout << "Resin material tensor: " << std::endl << C[i] << std::endl; }
	    }

            if (mat_prop.materialType[i] == 1)
            {
                double E1 = mat_prop.matprop[i][0];
                double E2 = mat_prop.matprop[i][1];
                double E3 = mat_prop.matprop[i][2];
                double nu12 = mat_prop.matprop[i][3];
                double nu21 = nu12 * (E2 / E1);
                double nu13 = mat_prop.matprop[i][4];
                double nu31 = nu13 * (E3 / E1);
                double nu23 = mat_prop.matprop[i][5];
                double nu32 = nu23 * (E3 / E2);
                double G12 = mat_prop.matprop[i][6];
                double G13 = mat_prop.matprop[i][7];
                double G23 = mat_prop.matprop[i][8];

		Dune::FieldMatrix<double,3,3> Sinv = {{1./E1,-nu21/E2,-nu31/E3},{-nu12/E1,1./E2,-nu32/E3},{-nu13/E1,-nu23/E2,1./E3}}; 
		Sinv.invert();

                C[i][0][0] = Sinv[0][0];
                C[i][1][1] = Sinv[1][1];
                C[i][2][2] = Sinv[2][2];
                C[i][0][1] = Sinv[0][1]; C[i][1][0] = Sinv[1][0];
                C[i][0][2] = Sinv[0][2]; C[i][2][0] = Sinv[2][0];
                C[i][1][2] = Sinv[1][2]; C[i][2][1] = Sinv[2][1];
                C[i][3][3] = G23;
                C[i][4][4] = G13;
                C[i][5][5] = G12;
            //if(my_rank == 0){ std::cout << "Ply material tensor: " << std::endl << C[i] << std::endl; }
            }
        } // end for each material 
    }

    M getMatrix(Dune::FieldMatrix<double,3,3> A) const{
	    M R;
        R[0][0] = A[0][0]*A[0][0]; R[0][1] = A[0][1]*A[0][1]; R[0][2] = A[0][2]*A[0][2];
	    R[1][0] = A[1][0]*A[1][0]; R[1][1] = A[1][1]*A[1][1]; R[1][2] = A[1][2]*A[1][2];
	    R[2][0] = A[2][0]*A[2][0]; R[2][1] = A[2][1]*A[2][1]; R[2][2] = A[2][2]*A[2][2];

	    R[0][3] = 2.0*A[0][1]*A[0][2]; R[0][4] = 2.0*A[0][0]*A[0][2]; R[0][5] = 2.0*A[0][0]*A[0][1];
	    R[1][3] = 2.0*A[1][1]*A[1][2]; R[1][4] = 2.0*A[1][0]*A[1][2]; R[1][5] = 2.0*A[1][0]*A[1][1];
	    R[2][3] = 2.0*A[2][1]*A[2][2]; R[2][4] = 2.0*A[2][0]*A[2][2]; R[2][5] = 2.0*A[2][0]*A[2][1];

	    R[3][0] = A[1][0]*A[2][0]; R[3][1] = A[1][1]*A[2][1]; R[3][2] = A[1][2]*A[2][2];
	    R[4][0] = A[0][0]*A[2][0]; R[4][1] = A[0][1]*A[2][1]; R[4][2] = A[0][2]*A[2][2];
	    R[5][0] = A[0][0]*A[1][0]; R[5][1] = A[0][1]*A[1][1]; R[5][2] = A[0][2]*A[1][2];
	
	    R[3][3] = A[1][1]*A[2][2]+A[1][2]*A[2][1]; R[3][4] = A[1][0]*A[2][2]+A[1][2]*A[2][0]; R[3][5] = A[1][0]*A[2][1]+A[1][1]*A[2][0];
	    R[4][3] = A[0][1]*A[2][2]+A[2][1]*A[0][2]; R[4][4] = A[0][0]*A[2][2]+A[2][0]*A[0][2]; R[4][5] = A[2][0]*A[0][1]+A[2][1]*A[0][0];
	    R[5][3] = A[0][1]*A[1][2]+A[0][2]*A[1][1]; R[5][4] = A[0][0]*A[1][2]+A[0][2]*A[1][0]; R[5][5] = A[0][0]*A[1][1]+A[0][1]*A[1][0];
        return R;
    }

    void eval_strain(M& C_local, const std::vector<double> theta, const int mat_id, const int mat_type, const int region_id){
	    C_local = C[mat_id];
	    M R2(0.0),R2T(0.0);
        double c = cos(theta[2]*M_PI/180.0);
	    double s = sin(theta[2]*M_PI/180.0);
	    Dune::FieldMatrix<double,3,3> A = {{c,s,0},{-s,c,0},{0,0,1}};
	    R2 = getMatrix(A);
        C_local.leftmultiply(R2);
    }

    void eval(M& C_global, const std::vector<double> theta, const int mat_id, const int mat_type, const int region_id) const
    {
	    C_global = C[mat_id];
	    M R2(0.0),R2T(0.0);
	    // Rotation theta about x2;
	    double c = cos(theta[2]*M_PI/180.0);
	    double s = sin(theta[2]*M_PI/180.0);
	    Dune::FieldMatrix<double,3,3> A = {{c,s,0},{-s,c,0},{0,0,1}};
	    R2 = getMatrix(A);
	    transpose<M,6>(R2,R2T);
	
        C_global.leftmultiply(R2);
	    C_global.rightmultiply(R2T);
    } // end eval

public:
    std::vector<Dune::FieldMatrix<double,6,6>> C;
};  //end class


#endif
