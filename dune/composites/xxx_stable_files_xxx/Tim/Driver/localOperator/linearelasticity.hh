#ifndef DUNE_PDELAB_LINEARELASTIC_HH
#define DUNE_PDELAB_LINEARELASTIC_HH

#include "../../MathFunctions/transpose.hh"

#include "defaultimp.hh"
#include "pattern.hh"
#include "flags.hh"
#include "idefault.hh"

namespace Dune {
    namespace PDELab {
        template<typename GV, typename LE, int dofel>
            class linearelasticity : 
                public NumericalJacobianApplyVolume<linearelasticity<GV,LE,dofel>>,
                public NumericalJacobianApplyBoundary<linearelasticity<GV,LE,dofel>>,
                public FullVolumePattern,
                public LocalOperatorDefaultFlags,
                public InstationaryLocalOperatorDefaultMethods<double>,
                public Dune::PDELab::NumericalJacobianVolume<linearelasticity<GV,LE,dofel> >,
                public Dune::PDELab::NumericalJacobianBoundary<linearelasticity<GV,LE,dofel> >

        {
            public:
                // pattern assembly flags
                enum { doPatternVolume = true };

                // residual assembly flags
                enum { doAlphaVolume = true };
                enum { doLambdaVolume = true };
                enum { doLambdaBoundary = true };

                //constructor
                linearelasticity (const GV gv_, const LE& le_, int intorder_=2, double eps_=100) : gv(gv_), le(le_), intorder(intorder_),eps(eps_){
                    my_rank = gv.comm().rank();
                }

                //Write the local stiffness matrix
                template<typename DuneMatrix1,typename DuneMatrix2, typename DuneVector>
                    void BCB(const unsigned int nodes_per_element, DuneVector &gradphi, DuneMatrix1 &C, DuneMatrix2 &K) const
                    {
                        Dune::FieldMatrix<double,6,dofel> tmp(0.0), B(0.0);
                        for (unsigned int i = 0; i < nodes_per_element; i++)
                        {
                            B[0][i] = gradphi[i][0];                                                                                                 // E11
                            B[1][i + nodes_per_element] = gradphi[i][1];                                                  // E22
                            B[2][i + 2 * nodes_per_element] = gradphi[i][2]; // E22
                            B[3][i + nodes_per_element] = gradphi[i][2]; B[3][i + 2 * nodes_per_element] = gradphi[i][1]; // E23
                            B[4][i] = gradphi[i][2];                                                B[4][i + 2 * nodes_per_element] = gradphi[i][0]; // E13
                            B[5][i] = gradphi[i][1];   B[5][i + nodes_per_element] = gradphi[i][0];                                                  // E12
                        }

                        //tmp = C * B
                        mm<DuneMatrix1,Dune::FieldMatrix<double,6,dofel>,Dune::FieldMatrix<double,6,dofel>>(C,B,tmp);

                        //	K = B' * tmp = B' * C * B
                        mtm<Dune::FieldMatrix<double,6,dofel>,Dune::FieldMatrix<double,6,dofel>,DuneMatrix2>(B,tmp,K);
                    }  //end BCB

                //Write the local stiffness matrix
                template<typename DuneMatrix, typename DuneVector1, typename DuneVector2, typename DuneVector>
                    void Bdelta(const unsigned int nodes_per_element, DuneVector1 &gradphi, DuneMatrix &C, DuneVector2 &delta, DuneVector &result) const
                    {
                        Dune::FieldMatrix<double,6,dofel>  B(0.0);
                        for (unsigned int i = 0; i < nodes_per_element; i++)
                        {
                            B[0][i] = gradphi[i][0];                                                                                                 // E11
                            B[1][i + nodes_per_element] = gradphi[i][1];                                                  // E22
                            B[2][i + 2 * nodes_per_element] = gradphi[i][2]; // E22
                            B[3][i + nodes_per_element] = gradphi[i][2]; B[3][i + 2 * nodes_per_element] = gradphi[i][1]; // E23
                            B[4][i] = gradphi[i][2];                                                B[4][i + 2 * nodes_per_element] = gradphi[i][0]; // E13
                            B[5][i] = gradphi[i][1];   B[5][i + nodes_per_element] = gradphi[i][0];                                                  // E12
                        }
                        DuneVector2 tmp(0.0);
                        C.mv(delta,tmp);
                        B.mtv(tmp,result);
                    }  //end Bdelta


                template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
                    void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, M & mat) const
                    {
                        // dimensions
                        const int dim = 3;

                        // extract local function spaces
                        typedef typename LFSU::template Child<0>::Type LFSU_U1;
                        typedef typename LFSU::template Child<1>::Type LFSU_U2;
                        typedef typename LFSU::template Child<2>::Type LFSU_U3;

                        const LFSU_U1& lfsu_u1 = lfsu.template child<0>();
                        const LFSU_U2& lfsu_u2 = lfsu.template child<1>();
                        const LFSU_U3& lfsu_u3 = lfsu.template child<2>();

                        const unsigned int npe = lfsu_u1.size();
                        assert(npe == dofel/dim); // ensure dof per element are set correctly

                        // domain and range field type
                        typedef typename LFSU_U1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
                        typedef typename M::value_type RF;
                        typedef typename LFSU_U1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType;

                        // select quadrature rule
                        GeometryType gt = eg.geometry().type();
                        const QuadratureRule<DF,dim>& rule = QuadratureRules<DF,dim>::rule(gt,intorder);

                        // Evaluate elasticity tensor (assumes it is constant over a single element)
                        Dune::FieldMatrix<double,6,6> C(0.0);
                        auto lay_id = le.getLayID(eg.entity());
                        le.evaluate(eg.entity(),C);

                        Dune::FieldMatrix<double,dofel,dofel> Kh(0.0);

                        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it){
                            auto theta = le.getThetaFct(eg.entity(),it->position());
                            auto Ch = getMatrix(C,theta);

                            Dune::FieldMatrix<double,dofel,dofel> K(0.0);
                            // Evaluate Jacobian
                            std::vector<JacobianType> js(npe);
                            lfsu_u1.finiteElement().localBasis().evaluateJacobian(it->position(),js);
                            // Transform gradient to real element
                            const typename EG::Geometry::JacobianInverseTransposed jac = eg.geometry().jacobianInverseTransposed(it->position());
                            std::vector<Dune::FieldVector<RF,dim> > gradphi(npe);
                            for (unsigned int i=0; i < npe; i++){
                                gradphi[i] = 0.0;
                                jac.umv(js[i][0],gradphi[i]);
                            }
                            Dune::FieldMatrix<double,6,dofel> B(0.0);
                            //Fill in stiffness matrix
                            BCB(npe,gradphi,Ch, K);

                            // Integration Point Weight
                            RF factor = it->weight() * eg.geometry().integrationElement(it->position());
                            K *= factor;
                            Kh += K;

                        } // end for-each quadrature point
                        if(intorder == 1) hourglass_control<Dune::FieldMatrix<double,dofel,dofel>>(Kh);

                        Dune::FieldMatrix<double,dim,dim> Kij(0.0);
                        for (unsigned int i = 0; i < npe; i++){
                            for (unsigned int j = 0; j < npe; j++){
                                Kij = {{Kh[i][j],     Kh[i][npe + j],         Kh[i][2*npe + j]},
                                    {Kh[npe + i][j],  Kh[npe + i][npe + j],   Kh[npe + i][2*npe + j]},
                                    {Kh[2*npe + i][j],Kh[2*npe + i][npe + j], Kh[2*npe + i][2*npe + j]}};
                                mat.accumulate(lfsu_u1,i,lfsu_u1,j,Kij[0][0] );
                                mat.accumulate(lfsu_u1,i,lfsu_u2,j,Kij[0][1] );
                                mat.accumulate(lfsu_u1,i,lfsu_u3,j,Kij[0][2] );
                                mat.accumulate(lfsu_u2,i,lfsu_u1,j,Kij[1][0] );
                                mat.accumulate(lfsu_u2,i,lfsu_u2,j,Kij[1][1]);
                                mat.accumulate(lfsu_u2,i,lfsu_u3,j,Kij[1][2] );
                                mat.accumulate(lfsu_u3,i,lfsu_u1,j,Kij[2][0] );
                                mat.accumulate(lfsu_u3,i,lfsu_u2,j,Kij[2][1] );
                                mat.accumulate(lfsu_u3,i,lfsu_u3,j,Kij[2][2] );
                            } // end for j
                        } // end for i

                    } // end jacobian_volume


                // volume integral depending on test and ansatz functions
                template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
                    void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
                    {
                        // dimensions
                        const int dim = 3;

                        // extract local function spaces
                        typedef typename LFSU::template Child<0>::Type LFSU_U1;
                        typedef typename LFSU::template Child<1>::Type LFSU_U2;
                        typedef typename LFSU::template Child<2>::Type LFSU_U3;

                        const LFSU_U1& lfsu_u1 = lfsu.template child<0>();
                        const LFSU_U2& lfsu_u2 = lfsu.template child<1>();
                        const LFSU_U3& lfsu_u3 = lfsu.template child<2>();

                        const unsigned int npe = lfsu_u1.size();

                        // domain and range field type
                        typedef typename LFSU_U1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
                        typedef typename R::value_type RF;
                        typedef typename LFSU_U1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType;

                        // select quadrature rule
                        GeometryType gt = eg.geometry().type();
                        const QuadratureRule<DF,dim>& rule = QuadratureRules<DF,dim>::rule(gt,intorder);

                        // Evaluate elasticity tensor (assumes it is constant over a single element)
                        Dune::FieldMatrix<double,6,6> C(0.0);
                        le.evaluate(eg.entity(),C);

                        Dune::FieldMatrix<double,dofel,dofel> K(0.0);

                        // Unwrap u
                        FieldVector<double,dofel> u(0.0);
                        for (unsigned int i=0; i<npe; i++){
                            u[i] =         x(lfsu_u1,i);
                            u[i + npe] =   x(lfsu_u2,i);
                            u[i + 2*npe] = x(lfsu_u3,i);
                        }

                        // Loop over quadrature points
                        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it)
                        {
                            auto theta = le.getThetaFct(eg.entity(),it->position());
                            auto Ch = getMatrix(C,theta);
                            // Evaluate Jacobian
                            std::vector<JacobianType> js(npe);
                            lfsu_u1.finiteElement().localBasis().evaluateJacobian(it->position(),js);

                            // Transform gradient to real element
                            const typename EG::Geometry::JacobianInverseTransposed jac = eg.geometry().jacobianInverseTransposed(it->position());
                            std::vector<Dune::FieldVector<RF,dim> > gradphi(npe);
                            for (unsigned int i=0; i < npe; i++){
                                gradphi[i] = 0.0;
                                jac.umv(js[i][0],gradphi[i]);
                            }

                            //Fill in stiffness matrix
                            BCB(npe,gradphi,Ch, K);

                            if(intorder == 1) hourglass_control<Dune::FieldMatrix<double,dofel,dofel>>(K);

                            // Compute residual = K * U - checked
                            FieldVector<double,dofel> residual(0.0);


                            for (int i = 0; i < dofel; i++){
                                for (int j = 0; j < dofel; j++){
                                    residual[i] += K[i][j] * u[j];
                                }
                            }

                            // geometric weight
                            RF factor = it->weight() * eg.geometry().integrationElement(it->position());

                            for (unsigned int i=0; i < npe; i++){
                                r.accumulate(lfsu_u1,i,residual[i] * factor);
                                r.accumulate(lfsu_u2,i,residual[i + npe] * factor);
                                r.accumulate(lfsu_u3,i,residual[i + 2*npe] * factor);
                            }


                        } // end for each quadrature point

                    } // end alpha_volume

                // volume integral depending only on test functions
                template<typename EG, typename LFSV, typename R>
                    void lambda_volume (const EG& eg, const LFSV& lfsv, R& r) const
                    {
                        // dimensions
                        const int dim = 3;

                        // extract local function spaces
                        typedef typename LFSV::template Child<0>::Type LFSV_U1;
                        typedef typename LFSV::template Child<1>::Type LFSV_U2;
                        typedef typename LFSV::template Child<2>::Type LFSV_U3;

                        const LFSV_U1& lfsv_u1 = lfsv.template child<0>();
                        const LFSV_U2& lfsv_u2 = lfsv.template child<1>();
                        const LFSV_U3& lfsv_u3 = lfsv.template child<2>();

                        const unsigned int npe = lfsv_u1.size();

                        // domain and range field type
                        typedef typename LFSV_U1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
                        typedef typename R::value_type RF;
                        typedef typename LFSV_U1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType;
                        typedef typename LFSV_U1::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RT_V;

                        //Get input heat strains
                        Dune::FieldVector<double,6> delta(0.0);
                        if(le.getMaterialType(eg.entity()) == 0)//resin
                            le.evaluateHeat(delta,true);
                        else
                            le.evaluateHeat(delta,false); //ply

                        //Get input forcing term
                        Dune::FieldVector<double,3> f1(0.0);
                        le.evaluateDensity(eg.entity(),f1);

                        // select quadrature rule
                        GeometryType gt = eg.geometry().type();
                        const QuadratureRule<DF,dim>& rule = QuadratureRules<DF,dim>::rule(gt,intorder);

                        // Evaluate elasticity tensor (assumes it is constant over a single element)
                        Dune::FieldMatrix<double,6,6> C(0.0);
                        le.evaluate_strain(eg.entity(),C);


                        // Loop over quadrature points
                        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it)
                        {
                            auto theta = le.getThetaFct(eg.entity(),it->position());
                            auto Cijkl = getMatrixStrain(C,theta);

                            //Shape functions
                            std::vector<RT_V> phi(npe);
                            lfsv_u1.finiteElement().localBasis().evaluateFunction(it->position(),phi);

                            // Evaluate Jacobian
                            std::vector<JacobianType> js(npe);
                            lfsv_u1.finiteElement().localBasis().evaluateJacobian(it->position(),js);

                            // Transform gradient to real element
                            const typename EG::Geometry::JacobianInverseTransposed jac = eg.geometry().jacobianInverseTransposed(it->position());
                            std::vector<Dune::FieldVector<RF,dim> > gradphi(npe);
                            for (unsigned int i=0; i < npe; i++){
                                gradphi[i] = 0.0;
                                jac.umv(js[i][0],gradphi[i]);
                            }

                            Dune::FieldVector<double,dofel> result;
                            Bdelta(npe,gradphi,Cijkl, delta, result);

                            // geometric weight
                            RF factor = it->weight() * eg.geometry().integrationElement(it->position());

                            for (unsigned int i=0; i < npe; i++){
                                r.accumulate(lfsv_u1,i, -(f1[0]*phi[i] + result[i])       * factor);
                                r.accumulate(lfsv_u2,i, -(f1[1]*phi[i] + result[i+npe])   * factor);
                                r.accumulate(lfsv_u3,i, -(f1[2]*phi[i] + result[i+2*npe]) * factor);
                            }

                        } // end loop over quadrature points
                    } // end lambda_volume


                // boundary integral
                template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
                    void alpha_boundary (const IG& ig,
                            const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                            R& r_s) const
                    {
                    }

                // jacobian of boundary term
                template<typename IG, typename LFSV_HAT, typename R>
                    void lambda_boundary (const IG& ig, const LFSV_HAT& lfsv_hat, R& r) const
                    {
                        // dimensions
                        const int dim = 3;
                        const int dimw = 3;

                        // extract local function spaces
                        typedef typename LFSV_HAT::template Child<0>::Type LFSV_U1;
                        typedef typename LFSV_HAT::template Child<1>::Type LFSV_U2;
                        typedef typename LFSV_HAT::template Child<2>::Type LFSV_U3;

                        const LFSV_U1& lfsv_u1 = lfsv_hat.template child<0>();
                        const LFSV_U2& lfsv_u2 = lfsv_hat.template child<1>();
                        const LFSV_U3& lfsv_u3 = lfsv_hat.template child<2>();

                        const unsigned int npe = lfsv_u1.size();

                        // domain and range field type
                        typedef typename LFSV_U1::Traits::FiniteElementType::
                            Traits::LocalBasisType::Traits::DomainFieldType DF;
                        typedef typename R::value_type RF;
                        typedef typename LFSV_U1::Traits::FiniteElementType::
                            Traits::LocalBasisType::Traits::RangeType RT_V;

                        // select quadrature rule
                        GeometryType gtface = ig.geometry().type();
                        const QuadratureRule<DF,dim-1>& rule = QuadratureRules<DF,dim-1>::rule(gtface,intorder);

                        // loop over quadrature points and integrate normal flux
                        for (typename Dune::QuadratureRule<DF,dim-1>::const_iterator it = rule.begin(), endit = rule.end();it != endit;++it)
                        {
                            // position of quadrature point in local coordinates of element
                            Dune::FieldVector<DF,dim> local = ig.geometryInInside().global(it->position());

                            // evaluate basis functions
                            std::vector<RT_V> phi(npe);
                            lfsv_u1.finiteElement().localBasis().evaluateFunction(local,phi);

                            // Compute Weight Factor
                            const RF factor = it->weight() * ig.geometry().integrationElement(it->position());
                            const Dune::FieldVector<DF,dimw> normal = ig.unitOuterNormal(it->position());
                            // evaluate flux boundary condition - needs updating
                            Dune::FieldVector<DF,dimw> neumann_stress(0.0);
                            le.evaluateNeumann(ig.geometry().global(it->position()),neumann_stress,normal);
                            for (size_t i=0; i < npe; i++){
                                r.accumulate(lfsv_u1,i, neumann_stress[0] * phi[i] * factor);
                                r.accumulate(lfsv_u2,i, neumann_stress[1] * phi[i] * factor);
                                r.accumulate(lfsv_u3,i, neumann_stress[2] * phi[i] * factor);

                            }
                        }
                    }

            private:

                template<typename Matrix1, typename Matrix2, typename Matrix3>
                    void mm(Matrix1& A, Matrix2&B, Matrix3& C) const
                    {
                        int a1 = A.N(); int a2 = A.M();
                        int b1 = B.N(); int b2 = B.M();
                        assert(a2 == b1);
                        C = 0.0;
                        for (int i = 0; i < a1; i++)
                        {
                            for (int j = 0; j < b2; j++)
                            {
                                for (int k = 0; k < a2; k++)
                                {
                                    C[i][j] += A[i][k] * B[k][j];
                                }
                            }
                        }
                    }

                template<typename Matrix1, typename Matrix2, typename Matrix3>
                    void mtm(Matrix1& A, Matrix2&B, Matrix3& C) const
                    {

                        int a1 = A.N(); int a2 = A.M();
                        int b1 = B.N(); int b2 = B.M();
                        assert(a1 == b1);
                        C = 0.0;
                        for (int i = 0; i < a2; i++)
                        {
                            for (int j = 0; j < b2; j++)
                            {
                                for (int k = 0; k < a1; k++)
                                {
                                    C[i][j] += A[k][i] * B[k][j];
                                }
                            }
                        }
                    }

                 Dune::FieldMatrix<double,6,6> getR(Dune::FieldMatrix<double,3,3> A) const{
                    Dune::FieldMatrix<double,6,6> R;
                    R[0][0] = A[0][0]*A[0][0]; R[0][1] = A[0][1]*A[0][1]; R[0][2] = A[0][2]*A[0][2];
                    R[1][0] = A[1][0]*A[1][0]; R[1][1] = A[1][1]*A[1][1]; R[1][2] = A[1][2]*A[1][2];
                    R[2][0] = A[2][0]*A[2][0]; R[2][1] = A[2][1]*A[2][1]; R[2][2] = A[2][2]*A[2][2];

                    R[0][3] = 2.0*A[0][1]*A[0][2]; R[0][4] = 2.0*A[0][0]*A[0][2]; R[0][5] = 2.0*A[0][0]*A[0][1];
                    R[1][3] = 2.0*A[1][1]*A[1][2]; R[1][4] = 2.0*A[1][0]*A[1][2]; R[1][5] = 2.0*A[1][0]*A[1][1];
                    R[2][3] = 2.0*A[2][1]*A[2][2]; R[2][4] = 2.0*A[2][0]*A[2][2]; R[2][5] = 2.0*A[2][0]*A[2][1];

                    R[3][0] = A[1][0]*A[2][0]; R[3][1] = A[1][1]*A[2][1]; R[3][2] = A[1][2]*A[2][2];
                    R[4][0] = A[0][0]*A[2][0]; R[4][1] = A[0][1]*A[2][1]; R[4][2] = A[0][2]*A[2][2];
                    R[5][0] = A[0][0]*A[1][0]; R[5][1] = A[0][1]*A[1][1]; R[5][2] = A[0][2]*A[1][2];

                    R[3][3] = A[1][1]*A[2][2]+A[1][2]*A[2][1]; R[3][4] = A[1][0]*A[2][2]+A[1][2]*A[2][0]; R[3][5] = A[1][0]*A[2][1]+A[1][1]*A[2][0];
                    R[4][3] = A[0][1]*A[2][2]+A[2][1]*A[0][2]; R[4][4] = A[0][0]*A[2][2]+A[2][0]*A[0][2]; R[4][5] = A[2][0]*A[0][1]+A[2][1]*A[0][0];
                    R[5][3] = A[0][1]*A[1][2]+A[0][2]*A[1][1]; R[5][4] = A[0][0]*A[1][2]+A[0][2]*A[1][0]; R[5][5] = A[0][0]*A[1][1]+A[0][1]*A[1][0];

                    return R;
                }

                Dune::FieldMatrix<double,6,6> getRStrain(Dune::FieldMatrix<double,3,3> A) const{
                    Dune::FieldMatrix<double,6,6> R;
                    R[0][0] = A[0][0]*A[0][0]; R[0][1] = A[0][1]*A[0][1]; R[0][2] = A[0][2]*A[0][2];
                    R[1][0] = A[1][0]*A[1][0]; R[1][1] = A[1][1]*A[1][1]; R[1][2] = A[1][2]*A[1][2];
                    R[2][0] = A[2][0]*A[2][0]; R[2][1] = A[2][1]*A[2][1]; R[2][2] = A[2][2]*A[2][2];

                    R[0][3] = 2.0*A[0][1]*A[0][2]; R[0][4] = 2.0*A[0][0]*A[0][2]; R[0][5] = 2.0*A[0][0]*A[0][1];
                    R[1][3] = 2.0*A[1][1]*A[1][2]; R[1][4] = 2.0*A[1][0]*A[1][2]; R[1][5] = 2.0*A[1][0]*A[1][1];
                    R[2][3] = 2.0*A[2][1]*A[2][2]; R[2][4] = 2.0*A[2][0]*A[2][2]; R[2][5] = 2.0*A[2][0]*A[2][1];

                    R[3][0] = A[1][0]*A[2][0]; R[3][1] = A[1][1]*A[2][1]; R[3][2] = A[1][2]*A[2][2];
                    R[4][0] = A[0][0]*A[2][0]; R[4][1] = A[0][1]*A[2][1]; R[4][2] = A[0][2]*A[2][2];
                    R[5][0] = A[0][0]*A[1][0]; R[5][1] = A[0][1]*A[1][1]; R[5][2] = A[0][2]*A[1][2];

                    R[3][3] = A[1][1]*A[2][2]+A[1][2]*A[2][1]; R[3][4] = A[1][0]*A[2][2]+A[1][2]*A[2][0]; R[3][5] = A[1][0]*A[2][1]+A[1][1]*A[2][0];
                    R[4][3] = A[0][1]*A[2][2]+A[2][1]*A[0][2]; R[4][4] = A[0][0]*A[2][2]+A[2][0]*A[0][2]; R[4][5] = A[2][0]*A[0][1]+A[2][1]*A[0][0];
                    R[5][3] = A[0][1]*A[1][2]+A[0][2]*A[1][1]; R[5][4] = A[0][0]*A[1][2]+A[0][2]*A[1][0]; R[5][5] = A[0][0]*A[1][1]+A[0][1]*A[1][0];

                    return R;
                }


                Dune::FieldMatrix<double,6,6> getMatrix(Dune::FieldMatrix<double,6,6> C, std::vector<double> theta) const{
                    double c = cos(theta[1]*M_PI/180.0);
                    double s = sin(theta[1]*M_PI/180.0);
                    Dune::FieldMatrix<double,3,3> A = {{c,0,-s},{0,1,0},{s,0,c}};

                    auto R = getR(A);
                    Dune::FieldMatrix<double,6,6> RT(0.0);
                    transpose<Dune::FieldMatrix<double,6,6>,6>(R,RT);

                    auto Ch = C;
                    Ch.leftmultiply(R);
                    Ch.rightmultiply(RT);
                    return Ch;
                }

                Dune::FieldMatrix<double,6,6> getMatrixStrain(Dune::FieldMatrix<double,6,6> C, std::vector<double> theta) const{
                    double c = cos(theta[1]*M_PI/180.0);
                    double s = sin(theta[1]*M_PI/180.0);
                    Dune::FieldMatrix<double,3,3> A = {{c,0,-s},{0,1,0},{s,0,c}};

                    auto R = getR(A);

                    auto Ch = C;
                    Ch.leftmultiply(R); 
                    return Ch;
                }

                template<typename Matrix>
                    void hourglass_control(Matrix & Kh) const{
                        if(dofel != 24){std::cout << "Warning: hourglass control not implemented for this case";}
                        Dune::FieldMatrix<double,dofel,dofel> v(0.0),vT(0.0);
                        v  = {  
                            { 1,   0,   0,   0,  -1,   1,  -1,   0,   0,  -1,  -1,   0,   1,   0,   0,   1,   0,   0,  -1,   0,   0,  -1,   0,   0},
                            { 1,   0,   0,   0,  -1,   1,   1,   0,   0,  -1,  -1,   0,   1,   0,   0,  -1,   0,   0,   1,   0,   0,   1,   0,   0},
                            { 1,   0,   0,   0,  -1,  -1,  -1,   0,   0,   1,  -1,   0,  -1,   0,   0,  -1,   0,   0,   1,   0,   0,  -1,   0,   0},
                            { 1,   0,   0,   0,  -1,  -1,   1,   0,   0,   1,  -1,   0,  -1,   0,   0,   1,   0,   0,  -1,   0,   0,   1,   0,   0},
                            { 1,   0,   0,   0,   1,   1,  -1,   0,   0,  -1,   1,   0,  -1,   0,   0,  -1,   0,   0,  -1,   0,   0,   1,   0,   0},
                            { 1,   0,   0,   0,   1,   1,   1,   0,   0,  -1,   1,   0,  -1,   0,   0,   1,   0,   0,   1,   0,   0,  -1,   0,   0},
                            { 1,   0,   0,   0,   1,  -1,  -1,   0,   0,   1,   1,   0,   1,   0,   0,   1,   0,   0,   1,   0,   0,   1,   0,   0},
                            { 1,   0,   0,   0,   1,  -1,   1,   0,   0,   1,   1,   0,   1,   0,   0,  -1,   0,   0,  -1,   0,   0,  -1,   0,   0},
                            { 0,   1,   0,   1,   0,  -1,   0,  -1,   0,  -1,   0,  -1,   0,   1,   0,   0,   1,   0,   0,  -1,   0,   0,  -1,   0},
                            { 0,   1,   0,   1,   0,   1,   0,  -1,   0,   1,   0,  -1,   0,   1,   0,   0,  -1,   0,   0,   1,   0,   0,   1,   0},
                            { 0,   1,   0,   1,   0,  -1,   0,   1,   0,  -1,   0,  -1,   0,  -1,   0,   0,  -1,   0,   0,   1,   0,   0,  -1,   0},
                            { 0,   1,   0,   1,   0,   1,   0,   1,   0,   1,   0,  -1,   0,  -1,   0,   0,   1,   0,   0,  -1,   0,   0,   1,   0},
                            { 0,   1,   0,  -1,   0,  -1,   0,  -1,   0,  -1,   0,   1,   0,  -1,   0,   0,  -1,   0,   0,  -1,   0,   0,   1,   0},
                            { 0,   1,   0,  -1,   0,   1,   0,  -1,   0,   1,   0,   1,   0,  -1,   0,   0,   1,   0,   0,   1,   0,   0,  -1,   0},
                            { 0,   1,   0,  -1,   0,  -1,   0,   1,   0,  -1,   0,   1,   0,   1,   0,   0,   1,   0,   0,   1,   0,   0,   1,   0},
                            { 0,   1,   0,  -1,   0,   1,   0,   1,   0,   1,   0,   1,   0,   1,   0,   0,  -1,   0,   0,  -1,   0,   0,  -1,   0},
                            { 0,   0,   1,  -1,   1,   0,   0,   0,  -1,   0,  -1,  -1,   0,   0,   1,   0,   0,   1,   0,   0,  -1,   0,   0,  -1},
                            { 0,   0,   1,  -1,  -1,   0,   0,   0,  -1,   0,   1,  -1,   0,   0,   1,   0,   0,  -1,   0,   0,   1,   0,   0,   1},
                            { 0,   0,   1,   1,   1,   0,   0,   0,  -1,   0,  -1,   1,   0,   0,  -1,   0,   0,  -1,   0,   0,   1,   0,   0,  -1},
                            { 0,   0,   1,   1,  -1,   0,   0,   0,  -1,   0,   1,   1,   0,   0,  -1,   0,   0,   1,   0,   0,  -1,   0,   0,   1},
                            { 0,   0,   1,  -1,   1,   0,   0,   0,   1,   0,  -1,  -1,   0,   0,  -1,   0,   0,  -1,   0,   0,  -1,   0,   0,   1},
                            { 0,   0,   1,  -1,  -1,   0,   0,   0,   1,   0,   1,  -1,   0,   0,  -1,   0,   0,   1,   0,   0,   1,   0,   0,  -1},
                            { 0,   0,   1,   1,   1,   0,   0,   0,   1,   0,  -1,   1,   0,   0,   1,   0,   0,   1,   0,   0,   1,   0,   0,   1},
                            { 0,   0,   1,   1,  -1,   0,   0,   0,   1,   0,   1,   1,   0,   0,   1,   0,   0,  -1,   0,   0,  -1,   0,   0,  -1}
                        };
                        //Normalise
                        for(int m=0; m< dofel; m++){
                            double sum = 0;
                            for(int j=0; j< dofel; j++){
                                sum+=v[j][m]*v[j][m];
                            }
                            for(int j=0; j< dofel; j++){
                                v[j][m]*=sqrt(1./sum);
                            }

                        }
                        transpose<Dune::FieldMatrix<double,dofel,dofel>, dofel>(v,vT);
                        //Diagonalise matrix		
                        Kh.rightmultiply(v);
                        Kh.leftmultiply(vT);
                        //Remove improper nullspace
                        for(int i = 12; i < dofel; i++){ 
                            Kh[i][i] = eps; 
                        }
                        //Return to initial basis
                        Kh.rightmultiply(vT);
                        Kh.leftmultiply(v);
                    }

                const LE& le;
                int intorder;
                double eps;
                const GV gv;
                int my_rank;
        };

    }
}
#endif
