
template<typename PROBLEM, typename EG>
double failureCriterion(const PROBLEM & problem,const EG& eg, std::vector<double> xlc){
    //allowables for aluminium
    double s11 = config.get<double>("vonMises_s11",250);
    double maxF = pow(xlc[1]/s11,2);
    return sqrt(maxF);
}

