#include "getStress.hh"
#include "MaxFail.hh"

template<typename V, typename GV1, typename GFS, typename MBE, typename PROBLEM, int dofel>
void calculateStresses(V& x0, const GV1& gv1, const GFS& gfs,  MBE& mbe, PROBLEM& problem, Dune::MPIHelper& helper ){
    typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::none,1> Scalar_VectorBackend;
    typedef double RF;
    std::string vtk_output = config.get<std::string>("ModelName","TestModel");
    Dune::Timer timer;

    using GV = Dune::PDELab::AllEntitySet<GV1>;
	auto gv = GV(gv1);

    //Stress plotting, choose routine to use pw. constant/linear/none via config file
    if(config.get<std::string>("stress_plot","dg") == "UG"){
        typedef typename GV::Grid::ctype e_ctype;
        typedef Dune::PDELab::PkLocalFiniteElementMap<GV,e_ctype,double,1> FEM_stress;
        // Construct FEM Space
        FEM_stress fem_stress(gv);
            typedef Dune::PDELab::GridFunctionSpace<GV, FEM_stress, Dune::PDELab::NoConstraints, Scalar_VectorBackend> SCALAR_P1GFS;
            SCALAR_P1GFS p1gfs_00(gv,fem_stress); p1gfs_00.name("stress00");  SCALAR_P1GFS p1gfs_11(gv,fem_stress); p1gfs_11.name("stress11");
            SCALAR_P1GFS p1gfs_22(gv,fem_stress); p1gfs_22.name("stress22");  SCALAR_P1GFS p1gfs_12(gv,fem_stress); p1gfs_12.name("stress12");
            SCALAR_P1GFS p1gfs_02(gv,fem_stress); p1gfs_02.name("stress02");  SCALAR_P1GFS p1gfs_01(gv,fem_stress); p1gfs_01.name("stress01");
            typedef Dune::PDELab::CompositeGridFunctionSpace <Scalar_VectorBackend,Dune::PDELab::LexicographicOrderingTag, 
                    SCALAR_P1GFS, SCALAR_P1GFS, SCALAR_P1GFS, SCALAR_P1GFS, SCALAR_P1GFS, SCALAR_P1GFS> P1GFS;
            P1GFS p1gfs(p1gfs_00,p1gfs_11,p1gfs_22,p1gfs_12,p1gfs_02,p1gfs_01);

            Dune::PDELab::countElem lopCnt;
            SCALAR_P1GFS cntpgfs(gv,fem_stress);
            typedef Dune::PDELab::EmptyTransformation NoTrafo;
            typedef Dune::PDELab::GridOperator<GFS,SCALAR_P1GFS,Dune::PDELab::countElem,MBE,RF,RF,RF,NoTrafo,NoTrafo> GOCnt;
            GOCnt goCnt(gfs,cntpgfs,lopCnt,mbe);

            typedef typename GOCnt::Traits::Range Vcnt;
            Vcnt ElemCnt(cntpgfs,0.0);

            goCnt.residual(x0,ElemCnt);

            //Turn counter into discrete grid function
            typedef Dune::PDELab::DiscreteGridFunction<SCALAR_P1GFS,Vcnt> DGF;
            DGF dgfCnt(cntpgfs, ElemCnt);

            //Set up local operator to calculate stresses
            double maxDisp = 0.0;
            Dune::PDELab::getStressUG<GV,PROBLEM,DGF,dofel> lopStress(gv,problem,dgfCnt,maxDisp);
            typedef Dune::PDELab::GridOperator<GFS,P1GFS,Dune::PDELab::getStressUG<GV,PROBLEM,DGF,dofel>,MBE,RF,RF,RF,NoTrafo,NoTrafo> GOStress;
            GOStress goStress(gfs,p1gfs,lopStress,mbe);

            typedef typename GOStress::Traits::Range V1;
            V1 stress(p1gfs,0.0);
            goStress.residual(x0,stress);

            Dune::FieldVector<double,7> maxFailR(0.0);
            Dune::FieldVector<double,3> loc(0.0);
            Dune::PDELab::maxFailUG<GV,PROBLEM,DGF,dofel> lopFail(gv,problem,dgfCnt,maxFailR,loc);
            SCALAR_P1GFS p1gfs_failure(gv,fem_stress); p1gfs_failure.name("failureCriterion");
            typedef Dune::PDELab::EmptyTransformation NoTrafo;
            typedef Dune::PDELab::GridOperator<P1GFS,SCALAR_P1GFS,Dune::PDELab::maxFailUG<GV,PROBLEM,DGF,dofel>,MBE,RF,RF,RF,NoTrafo,NoTrafo> GOFail;
            GOFail gofail(p1gfs,p1gfs_failure,lopFail,mbe);

            typedef typename GOFail::Traits::Range VF;
            VF ElemFail(p1gfs_failure,0.0);
            gofail.residual(stress,ElemFail);

            // Save solution in vtk format for paraview
            if(config.get<bool>("vtkoutput",false)){
                if(helper.rank()==0) std::cout << "Starting to write vtkoutput" << std::endl;
                Dune::SubsamplingVTKWriter<GV1> vtkwriter(gv1,0);
                Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,x0);
                Dune::PDELab::addSolutionToVTKWriter(vtkwriter,p1gfs,stress);
                Dune::PDELab::addSolutionToVTKWriter(vtkwriter,p1gfs_failure,ElemFail);
                vtkwriter.write(vtk_output,Dune::VTK::appendedraw);
                if(helper.rank()==0) std::cout << "vtkoutput successfully written" << std::endl;
            }

            //Calculate max. failure criterion over all processors
            struct{
                double val;
                int rank;
            } localmax[1], globalmax[1];

            localmax[0].val = maxFailR[0];
            localmax[0].rank = helper.rank();

            //std::cout << "Processor " << localmax[0].rank << " has " << localmax[0].val << std::endl;

            MPI_Reduce(localmax, globalmax, 1, MPI_DOUBLE_INT, MPI_MAXLOC, 0, gfs.gridView().comm());

            int procmax;
            if(helper.rank()==0){
                //std::cout << "globalmax " << globalmax[0].val << " on " << globalmax[0].rank << std::endl;
                procmax = globalmax[0].rank;
            }
            MPI_Bcast(&procmax, 1, MPI_INT, 0, gfs.gridView().comm());

            if(helper.rank() == helper.size()-1){
                std::cout << "=== Max disp. : " << maxDisp << std::endl;
            }

            MPI_Barrier(gfs.gridView().comm());
            if(helper.rank() == procmax){
                std::cout << "On process " << helper.rank() << std::endl;
                std::cout << "=== Max failure crit. : " << maxFailR[0] << std::endl;
                std::cout << "=== location of failure: " << loc << std::endl;
                std::cout << "=== Stresses. : " << maxFailR[1] << " " <<maxFailR[2] << " " << maxFailR[3] << " " << maxFailR[4] << " " << maxFailR[5] << " " << maxFailR[6] << std::endl;
            }
            MPI_Barrier(gfs.gridView().comm());

    }
    if(config.get<std::string>("stress_plot","dg") == "dg"){
            //Function spaces for Stresses
            typedef typename GV::Grid::ctype e_ctype;
            typedef Dune::PDELab::QkDGLocalFiniteElementMap<e_ctype,double,0,3> FEM_stress;
            FEM_stress fem_stress;
            typedef Dune::PDELab::GridFunctionSpace<GV, FEM_stress, Dune::PDELab::NoConstraints, Scalar_VectorBackend> SCALAR_P1GFS;
            SCALAR_P1GFS p1gfs_00(gv,fem_stress); p1gfs_00.name("stress00");  SCALAR_P1GFS p1gfs_11(gv,fem_stress); p1gfs_11.name("stress11");
            SCALAR_P1GFS p1gfs_22(gv,fem_stress); p1gfs_22.name("stress22");  SCALAR_P1GFS p1gfs_12(gv,fem_stress); p1gfs_12.name("stress12");
            SCALAR_P1GFS p1gfs_02(gv,fem_stress); p1gfs_02.name("stress02");  SCALAR_P1GFS p1gfs_01(gv,fem_stress); p1gfs_01.name("stress01");
            SCALAR_P1GFS p1gfs_failure(gv,fem_stress); p1gfs_failure.name("failureCriterion");
            typedef Dune::PDELab::CompositeGridFunctionSpace <Scalar_VectorBackend,Dune::PDELab::LexicographicOrderingTag, 
                    SCALAR_P1GFS, SCALAR_P1GFS, SCALAR_P1GFS, SCALAR_P1GFS, SCALAR_P1GFS, SCALAR_P1GFS, SCALAR_P1GFS> P1GFS;
            P1GFS p1gfs(p1gfs_00,p1gfs_11,p1gfs_22,p1gfs_12,p1gfs_02,p1gfs_01,p1gfs_failure);

            //Set up local operator to calculate stresses
            double maxDisp = 0.0;
            Dune::FieldVector<double,7> maxFail;
            Dune::FieldVector<double,3> loc;
            typedef Dune::PDELab::EmptyTransformation NoTrafo;
            Dune::PDELab::getStress<GV,PROBLEM,dofel> lopStress(gv,problem,maxDisp,maxFail,loc);
            typedef Dune::PDELab::GridOperator<GFS,P1GFS,Dune::PDELab::getStress<GV,PROBLEM,dofel>,MBE,RF,RF,RF,NoTrafo,NoTrafo> GOStress;
            GOStress goStress(gfs,p1gfs,lopStress,mbe);

            typedef typename GOStress::Traits::Range V1;
            V1 stress(p1gfs,0.0); 
            goStress.residual(x0,stress);

            // Save solution in vtk format for paraview
            if(config.get<bool>("vtkoutput",false)){
                Dune::SubsamplingVTKWriter<GV1> vtkwriter(gv1,0);
                Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,x0);
                Dune::PDELab::addSolutionToVTKWriter(vtkwriter,p1gfs,stress);
                vtkwriter.write(vtk_output,Dune::VTK::appendedraw);
            }

            if(helper.rank() == helper.size()-1){
                std::cout << "=== Max disp. : " << maxDisp << std::endl;
                std::cout << "=== Max failure crit. : " << maxFail << " location of failure: " << loc << std::endl;
            }
        }
        if(config.get<std::string>("stress_plot","dg") == "linear"){
             //Function spaces for Stresses
            typedef typename GV::Grid::ctype e_ctype;
            typedef Dune::PDELab::QkDGLocalFiniteElementMap<e_ctype,double,1,3> FEM_stress;
            typedef Dune::PDELab::QkLocalFiniteElementMap<GV,e_ctype,double,1> FEM_stress2;
            FEM_stress fem_stress;
            FEM_stress2 fem_stress2(gv);
            typedef Dune::PDELab::GridFunctionSpace<GV, FEM_stress, Dune::PDELab::NoConstraints, Scalar_VectorBackend> SCALAR_P1GFS;
            typedef Dune::PDELab::GridFunctionSpace<GV, FEM_stress2, Dune::PDELab::NoConstraints, Scalar_VectorBackend> SCALAR_P1GFS2;
            SCALAR_P1GFS p1gfs_00(gv,fem_stress); p1gfs_00.name("stress00");  SCALAR_P1GFS2 p1gfs_11(gv,fem_stress2); p1gfs_11.name("stress11");
            SCALAR_P1GFS2 p1gfs_22(gv,fem_stress2); p1gfs_22.name("stress22");  SCALAR_P1GFS p1gfs_12(gv,fem_stress); p1gfs_12.name("stress12");
            SCALAR_P1GFS2 p1gfs_02(gv,fem_stress2); p1gfs_02.name("stress02");  SCALAR_P1GFS p1gfs_01(gv,fem_stress); p1gfs_01.name("stress01");
            typedef Dune::PDELab::CompositeGridFunctionSpace <Scalar_VectorBackend,Dune::PDELab::LexicographicOrderingTag, 
                    SCALAR_P1GFS, SCALAR_P1GFS2, SCALAR_P1GFS2, SCALAR_P1GFS, SCALAR_P1GFS2, SCALAR_P1GFS> P1GFS;
            P1GFS p1gfs(p1gfs_00,p1gfs_11,p1gfs_22,p1gfs_12,p1gfs_02,p1gfs_01);

            Dune::PDELab::countElem lopCnt;
            SCALAR_P1GFS2 cntpgfs(gv,fem_stress2);
            typedef Dune::PDELab::EmptyTransformation NoTrafo;
            typedef Dune::PDELab::GridOperator<GFS,SCALAR_P1GFS2,Dune::PDELab::countElem,MBE,RF,RF,RF,NoTrafo,NoTrafo> GOCnt;
            GOCnt goCnt(gfs,cntpgfs,lopCnt,mbe);

            typedef typename GOCnt::Traits::Range Vcnt;
            Vcnt ElemCnt(cntpgfs,0.0);

            goCnt.residual(x0,ElemCnt);

            //Turn counter into discrete grid function
            typedef Dune::PDELab::DiscreteGridFunction<SCALAR_P1GFS2,Vcnt> DGF;
            DGF dgfCnt(cntpgfs, ElemCnt);

            //Set up local operator to calculate stresses
            double maxDisp = 0.0;
            Dune::PDELab::getStressLinear<GV,PROBLEM,DGF,dofel> lopStress(gv,problem,dgfCnt,maxDisp);
            typedef Dune::PDELab::GridOperator<GFS,P1GFS,Dune::PDELab::getStressLinear<GV,PROBLEM,DGF,dofel>,MBE,RF,RF,RF,NoTrafo,NoTrafo> GOStress;
            GOStress goStress(gfs,p1gfs,lopStress,mbe);

            typedef typename GOStress::Traits::Range V1;
            V1 stress(p1gfs,0.0);
            goStress.residual(x0,stress);

            Dune::FieldVector<double,7> maxFailR(0.0);
            Dune::FieldVector<double,3> loc(0.0);
            Dune::PDELab::maxFail<GV,PROBLEM,dofel> lopFail(gv,problem,maxFailR,loc);
            SCALAR_P1GFS p1gfs_failure(gv,fem_stress); p1gfs_failure.name("failureCriterion");
            typedef Dune::PDELab::EmptyTransformation NoTrafo;
            typedef Dune::PDELab::GridOperator<P1GFS,SCALAR_P1GFS,Dune::PDELab::maxFail<GV,PROBLEM,dofel>,MBE,RF,RF,RF,NoTrafo,NoTrafo> GOFail;
            GOFail gofail(p1gfs,p1gfs_failure,lopFail,mbe);

            typedef typename GOFail::Traits::Range VF;
            VF ElemFail(p1gfs_failure,0.0);
            gofail.residual(stress,ElemFail);

            // Save solution in vtk format for paraview
            if(config.get<bool>("vtkoutput",false)){
                if(helper.rank()==0) std::cout << "Starting to write vtkoutput" << std::endl;
                Dune::SubsamplingVTKWriter<GV1> vtkwriter(gv1,0);
                Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,x0);
                Dune::PDELab::addSolutionToVTKWriter(vtkwriter,p1gfs,stress);
                Dune::PDELab::addSolutionToVTKWriter(vtkwriter,p1gfs_failure,ElemFail);
                vtkwriter.write(vtk_output,Dune::VTK::appendedraw);
                if(helper.rank()==0) std::cout << "vtkoutput successfully written" << std::endl;
            }

            //Calculate max. failure criterion over all processors
            struct{
                double val;
                int rank;
            } localmax[1], globalmax[1];

            localmax[0].val = maxFailR[0];
            localmax[0].rank = helper.rank();

            //std::cout << "Processor " << localmax[0].rank << " has " << localmax[0].val << std::endl;

            MPI_Reduce(localmax, globalmax, 1, MPI_DOUBLE_INT, MPI_MAXLOC, 0, gfs.gridView().comm());

            int procmax;
            if(helper.rank()==0){
                //std::cout << "globalmax " << globalmax[0].val << " on " << globalmax[0].rank << std::endl;
                procmax = globalmax[0].rank;
            }
            MPI_Bcast(&procmax, 1, MPI_INT, 0, gfs.gridView().comm());

            if(helper.rank() == helper.size()-1){
                std::cout << "=== Max disp. : " << maxDisp << std::endl;
            }

            MPI_Barrier(gfs.gridView().comm());
            if(helper.rank() == procmax){
                std::cout << "On process " << helper.rank() << std::endl;
                std::cout << "=== Max failure crit. : " << maxFailR[0] << std::endl;
                std::cout << "=== location of failure: " << loc << std::endl;
                std::cout << "=== Stresses. : " << maxFailR[1] << " " <<maxFailR[2] << " " << maxFailR[3] << " " << maxFailR[4] << " " << maxFailR[5] << " " << maxFailR[6] << std::endl;
            }
            MPI_Barrier(gfs.gridView().comm());

        }
        if(config.get<bool>("vtkoutput",false) && config.get<std::string>("stress_plot","none") == "none"){
            // Save solution in vtk format for paraview
            Dune::SubsamplingVTKWriter<GV1> vtkwriter(gv1,0);
            Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,x0);
            vtkwriter.write(vtk_output,Dune::VTK::appendedraw);
        }

        struct{
            double val;
            int rank;
        } localmax[1], globalmax[1];

        localmax[0].val = x0.infinity_norm();
        localmax[0].rank = helper.rank();

        MPI_Reduce(localmax, globalmax, 1, MPI_DOUBLE_INT, MPI_MAXLOC, 0, gfs.gridView().comm());

        int procmax;
        if(helper.rank()==0){
            procmax = globalmax[0].rank;
        }
        MPI_Bcast(&procmax, 1, MPI_INT, 0, gfs.gridView().comm());

        if(helper.rank() == procmax){
            std::cout << "=== Output Printed to File:" << timer.elapsed() << std::endl;
            std::cout << "=== L_infty Norm : " << x0.infinity_norm() << std::endl;
        }

        //file.close();
}
