#ifndef MAXFAIL_HH
#define MAXFAIL_HH

#include <cmath>

#include <dune/geometry/type.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>

namespace Dune {
  namespace PDELab {
template<typename GV, typename PROBLEM, int dofel>  
class maxFail :
    public NumericalJacobianApplyVolume<maxFail<GV,PROBLEM,dofel> >,
    public FullVolumePattern,
    public LocalOperatorDefaultFlags,
    public InstationaryLocalOperatorDefaultMethods<double>,
    public NumericalJacobianVolume<maxFail<GV, PROBLEM, dofel> >
{
    public:
        // pattern assembly flags
        enum { doPatternVolume = false };

        // residual assembly flags
        enum { doAlphaVolume  = true };

        //Constructor
        maxFail (const GV& gv_, const PROBLEM& prob_,  Dune::FieldVector<double,7>& maxFailR_, Dune::FieldVector<double,3>& loc_) : gv(gv_), problem(prob_) , maxFailR(maxFailR_), loc(loc_) {}

        // Volume Integral Depending on Test and Ansatz Functions
        template<typename EG, typename LFSU, typename V, typename LFSV, typename R>
            void alpha_volume (const EG& eg, const LFSU& lfsu, const V& stress, const LFSV& lfsv, R& r) const
            {
                const int dim = 3;

                //Unwrap stresses
                const auto& lfs1 = lfsu.template child<0>();
                const auto& lfs2 = lfsu.template child<1>();
                const auto& lfs3 = lfsu.template child<2>();
                const auto& lfs4 = lfsu.template child<3>();
                const auto& lfs5 = lfsu.template child<4>();
                const auto& lfs6 = lfsu.template child<5>();

                int dof = lfsv.size();

                std::vector<double>  xlc(6);
                for (int i = 0; i < dof; i++){
                    xlc[0] = stress(lfs1,i);
                    xlc[1] = stress(lfs2,i);
                    xlc[2] = stress(lfs3,i);
                    xlc[3] = stress(lfs4,i);
                    xlc[4] = stress(lfs5,i);
                    xlc[5] = stress(lfs6,i);

                    double maxF = failureCriterion<PROBLEM,EG>(problem,eg,xlc);
                    r.accumulate(lfsv,i,maxF);

                    //Max failure
                    if(config.get<std::string>("defect_type","simple") != "MC" || (eg.geometry().corner(i)[1] > 15 && eg.geometry().corner(i)[1] < 37)){
                            if(maxF > maxFailR[0]){
                                maxFailR[0] = maxF;
                                maxFailR[1] = xlc[0];
                                maxFailR[2] = xlc[1];
                                maxFailR[3] = xlc[2];
                                maxFailR[4] = xlc[3];
                                maxFailR[5] = xlc[4];
                                maxFailR[6] = xlc[5];
                                loc = eg.geometry().corner(i);
                            }
                        }
                }

            }
    private:
        const GV& gv;
        const PROBLEM &problem;
        Dune::FieldVector<double,7>& maxFailR;
        Dune::FieldVector<double,3>& loc;
};

template<typename GV, typename PROBLEM, typename DGF, int dofel>  
class maxFailUG :
    public NumericalJacobianApplyVolume<maxFailUG<GV,PROBLEM,DGF,dofel> >,
    public FullVolumePattern,
    public LocalOperatorDefaultFlags,
    public InstationaryLocalOperatorDefaultMethods<double>,
    public NumericalJacobianVolume<maxFailUG<GV, PROBLEM, DGF, dofel> >
{
    public:
        // pattern assembly flags
        enum { doPatternVolume = false };

        // residual assembly flags
        enum { doAlphaVolume  = true };

        //Constructor
        maxFailUG (const GV& gv_, const PROBLEM& prob_, const DGF& dgf_,
                Dune::FieldVector<double,7>& maxFailR_, Dune::FieldVector<double,3>& loc_) :
            gv(gv_), problem(prob_), dgf(dgf_), maxFailR(maxFailR_), loc(loc_) {}

        // Volume Integral Depending on Test and Ansatz Functions
        template<typename EG, typename LFSU, typename V, typename LFSV, typename R>
            void alpha_volume (const EG& eg, const LFSU& lfsu, const V& stress, const LFSV& lfsv, R& r) const
            {
                const int dim = 3;

                //Unwrap stresses
                const auto& lfs1 = lfsu.template child<0>();
                const auto& lfs2 = lfsu.template child<1>();
                const auto& lfs3 = lfsu.template child<2>();
                const auto& lfs4 = lfsu.template child<3>();
                const auto& lfs5 = lfsu.template child<4>();
                const auto& lfs6 = lfsu.template child<5>();

                int dof = lfsv.size();

                std::vector<double>  xlc(6);
                for (int i = 0; i < dof; i++){
                    Dune::FieldVector<double,dim> pt = eg.geometry().local(eg.geometry().corner(i));

                    xlc[0] = stress(lfs1,i);
                    xlc[1] = stress(lfs2,i);
                    xlc[2] = stress(lfs3,i);
                    xlc[3] = stress(lfs4,i);
                    xlc[4] = stress(lfs5,i);
                    xlc[5] = stress(lfs6,i);

                    Dune::FieldVector<double,1> num_elem(1.0);
                    dgf.evaluate(eg.entity(),pt,num_elem);
                    double maxF = failureCriterion<PROBLEM,EG>(problem,eg,xlc);
                    r.accumulate(lfsv,i,maxF/num_elem[0]);

                    //Max failure
                    if(config.get<std::string>("defect_type","simple") != "MC" || (eg.geometry().corner(i)[1] > 15 && eg.geometry().corner(i)[1] < 37)){
                            if(maxF > maxFailR[0]){
                                maxFailR[0] = maxF;
                                maxFailR[1] = xlc[0];
                                maxFailR[2] = xlc[1];
                                maxFailR[3] = xlc[2];
                                maxFailR[4] = xlc[3];
                                maxFailR[5] = xlc[4];
                                maxFailR[6] = xlc[5];
                                loc = eg.geometry().corner(i);
                            }
                        }
                }

            }
    private:
        const GV& gv;
        const PROBLEM &problem;
        const DGF & dgf;
        Dune::FieldVector<double,7>& maxFailR;
        Dune::FieldVector<double,3>& loc;
};



  }
}



#endif
