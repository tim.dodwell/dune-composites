
#ifndef SOLVER_INFO_HH
#define SOLVER_INFO_HH




namespace Dune{

namespace Composites{

class SolverInfo{
	
public:

	int verb;

	int MaxIt;

	double KrylovTol;

	double eigenvalue_threshold;

	bool widlund_part_unity;

	int nev;

	int nev_arpack;

	double eigen_shift;

	std::vector<double> times;

	Dune::InverseOperatorResult results;

	SolverInfo(int verb_ = 0): verb(verb_){

		// Default Krylov Solver Options

		MaxIt = 1000;
		KrylovTol = 1e-10;


		// Default GenEO Preconditioner Options

		eigenvalue_threshold = 0.2;
		widlund_part_unity = true;
		nev = 30; // Number of eigenvalues used 
        nev_arpack = -1; // Use ArPack's inbuilt error estimator
        eigen_shift = 0.001;

	}

	void inline setTimes(std::vector<double>& t){
		times.resize(t.size());
		t = times;
	}

	void inline recordResult(Dune::InverseOperatorResult& res){
		results.clear();
		results.iterations = res.iterations;
		results.reduction = res.reduction;
 		results.converged = res.converged;
  		results.conv_rate = res.conv_rate;
		results.elapsed = res.elapsed;
	}

	bool inline converged(){return results.converged;}




private:


};

}
}

#endif