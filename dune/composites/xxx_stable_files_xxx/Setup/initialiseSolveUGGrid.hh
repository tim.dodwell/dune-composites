#include "../composites.hh"
#include "../Geometry/geometryInfo.hh"
#include "../Geometry/defineMatProp.hh"
#include "../Serendipity/serendipityfem.hh"
#include "../Driver/elasticityDriver.hh"
#include "../Driver/Cijkl.hh"
#include "io/mpi_write.hh"
#include "problemUGGrid.hh"

std::vector<double> initialiseSolveUGGrid(Dune::MPIHelper& helper){
    std::vector<double> times(1);
    std::string grid_name = config.get<std::string>("grid_name","cube");
    if(helper.rank() == 0)
        std::cout << "Chosen grid type: " << grid_name << std::endl; 

    modelGeometry model(grid_name,helper.size());
    typedef matProp<modelGeometry> MAT_PROP;
    MAT_PROP matProp(model);

    // Read in UG grid
    typedef Dune::UGGrid<3> GRID;
    GRID grid;
    Dune::GridFactory<GRID> factory(&grid);
    std::vector<int> bndIndx2PG;
    std::vector<int> elemIndx2PG;
    Dune::GmshReader<GRID>::read(factory,config.get<std::string>("mshName","grids/layercake.msh"),bndIndx2PG,elemIndx2PG, true , false);
    factory.createGrid();
    grid.loadBalance();
    typedef Dune::UGGrid<3>::LeafGridView GV;
    const GV& gv=grid.leafGridView();
    std::vector<int> nelem(1);
    nelem[0] = std::pow(gv.size(3),1./3.);

    //Set up problem
    typedef ElasticityProblemUG<GV, double, std::vector<int>, MAT_PROP, modelGeometry> PROBLEM;
    PROBLEM problem(gv,elemIndx2PG, matProp, model);

    // Setup initial boundary conditions for each degree of freedom
    typedef Scalar_BC<GV,modelGeometry,double> InitialDisp;
    InitialDisp u1(gv,model,0), u2(gv, model,1), u3(gv,model,2);
    u1.setDof(1);
    u2.setDof(2);
    u3.setDof(3);

    // Wrap scalar boundary conditions in to vector
    typedef Dune::PDELab::CompositeGridFunction<InitialDisp,InitialDisp,InitialDisp>	InitialSolution;
    InitialSolution initial_solution(u1,u2,u3);

    if (helper.size() > 1)   problem.setParallel(true);
    else problem.setParallel(false);

  /*  int solver_type = config.get<int>("solver_type", 1);
    problem.setSolver(solver_type);
    problem.setMaxIt(5000);
    problem.setVerbosity(config.get<int>("verbosity",1));
    problem.setTol(config.get<double>("tol",1e-4));
    int intorder=config.get<int>("intorder",5);
    problem.setIntOrder(intorder);*/

    // Construct grid function spaces for each degree of freedom
    typedef Dune::PDELab::NonoverlappingConformingDirichletConstraints<GV> CON;
    CON con(gv);

    /*
    //Set up FEM space
    if(config.get<int>("elem_order",1) == 1){
        if(helper.rank() == 0) std::cout << "UGGrid: Piecewise linear elements" << std::endl;
        const int element_order = 1; // Element order 1 - linear, 2 - quadratic
        const int dofel = 12;
        typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GV::Grid::ctype,double,element_order> FEM;
        // Construct FEM Space
        FEM fem(gv);

        typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::none,1> Scalar_VectorBackend;

        typedef Dune::PDELab::GridFunctionSpace<GV, FEM, CON, Scalar_VectorBackend> SCALAR_GFS;
        SCALAR_GFS dispU1(gv,fem,con); dispU1.name("U1");
        SCALAR_GFS dispU2(gv,fem,con); dispU2.name("U2");
        SCALAR_GFS dispU3(gv,fem,con); dispU3.name("U3");
        typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::fixed,3> VectorBackend;  //Vectors are blocked
        typedef Dune::PDELab::CompositeGridFunctionSpace <VectorBackend,Dune::PDELab::EntityBlockedOrderingTag, SCALAR_GFS, SCALAR_GFS, SCALAR_GFS> GFS;
        const GFS gfs(dispU1, dispU2, dispU3);

        con.compute_ghosts(gfs);

        //FEM driver
        driver<PROBLEM,modelGeometry,GV,GFS,GRID,InitialSolution,dofel,FEM,CON>(fem,gfs,problem,gv,grid,initial_solution,con,helper,model, nelem[0], times);
    }
    else{
        if(helper.rank() == 0) std::cout << "UGGrid: Piecewise quadratic elements" << std::endl;
        assert(config.get<int>("elem_order",1) == 2);
        const int element_order = 2; // Element order 1 - linear, 2 - quadratic
        const int dofel = 30;
        typedef Dune::PDELab::PkLocalFiniteElementMap<GV,GV::Grid::ctype,double,element_order> FEM;
        // Construct FEM Space
        FEM fem(gv);

        typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::none,1> Scalar_VectorBackend;

        typedef Dune::PDELab::GridFunctionSpace<GV, FEM, CON, Scalar_VectorBackend> SCALAR_GFS;
        SCALAR_GFS dispU1(gv,fem,con); dispU1.name("U1");
        SCALAR_GFS dispU2(gv,fem,con); dispU2.name("U2");
        SCALAR_GFS dispU3(gv,fem,con); dispU3.name("U3");
        typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::fixed,3> VectorBackend;  //Vectors are blocked
        typedef Dune::PDELab::CompositeGridFunctionSpace <VectorBackend,Dune::PDELab::EntityBlockedOrderingTag, SCALAR_GFS, SCALAR_GFS, SCALAR_GFS> GFS;
        const GFS gfs(dispU1, dispU2, dispU3);

        con.compute_ghosts(gfs);

        //FEM driver
        driver<PROBLEM,modelGeometry,GV,GFS,GRID,InitialSolution,dofel,FEM,CON>(fem,gfs,problem,gv,grid,initial_solution,con,helper,model, nelem[0], times);
    }*/
    return times;
}//end setup for UG Grid

