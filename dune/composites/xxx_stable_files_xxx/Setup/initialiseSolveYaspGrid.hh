#include "../composites.hh"
#include "../Geometry/geometryInfo.hh"
#include "../Geometry/defineMatProp.hh"
#include "../Geometry/createTPGrid.hh"
#include "parallelPartition.hh"
#include "definePG.hh"
#include "problemYaspGrid.hh"
#include "../Serendipity/serendipityfem.hh"
#include "../Driver/elasticityDriver.hh"
#include "io/mpi_write.hh"

std::vector<double> initialiseSolveYaspGrid(Dune::MPIHelper& helper){
    Dune::Timer timer;

    std::string grid_name = config.get<std::string>("grid_name","cube");
    if(helper.rank() == 0)
        std::cout << "Chosen grid type: " << grid_name << std::endl; 
    int overlap = config.get<int>("overlap", 1);

    modelGeometry model(grid_name,helper.size());
    typedef matProp<modelGeometry> MAT_PROP;
    MAT_PROP matProp(model);
    //make grid using YaspGrid
    typedef Dune::YaspGrid<3,Dune::TensorProductCoordinates<double,3>> YGRID;
    //Create model using settings in config-elasticity
    std::vector<int> nelem(5);
    nelem[0] = config.get<int>("nelem0",10); nelem[1] = config.get<int>("nelem1",20); 
    nelem[2] = config.get<int>("nelem2",10); nelem[3] = config.get<int>("nelem3",2); 
    nelem[4] = config.get<int>("nelem4",2);

    std::array< std::vector<double>, 3> coords = createCoords<modelGeometry>(model, nelem);
    std::bitset<3> periodic;
    periodic[0] = false; periodic[1] = false; periodic[2] = false;
    if(config.get<int>("periodic",0) == 1){
        assert(helper.size()>1);
        periodic[1] = true;
    }
    if(config.get<std::string>("grid_name").compare("wingbox") == 0){
        assert(helper.size()>1);
        periodic[0] = true;
    }

    YaspPartition<3> yp(helper.size());
    YGRID yaspgrid(coords,periodic,overlap,helper.getCommunicator(),(Dune::YLoadBalance<3>*)&yp);

    typedef YGRID::LeafGridView YGV;
    const YGV& ygv = yaspgrid.leafGridView(); 
    int size =  yaspgrid.globalSize(0)*yaspgrid.globalSize(1)*yaspgrid.globalSize(2);
    auto elemIndx2PG  = setPG<YGV,modelGeometry>(ygv,model, size);
    if(helper.rank() == 0){
        std::cout << "Number of elements per processor: " << ygv.size(0) << std::endl;
        std::cout << "Number of nodes per processor: "    << ygv.size(3) << std::endl;
    }

    std::vector<double> times(4);
    times[0] = timer.elapsed();
    timer.reset();
    
    //Define geometry transformation from cube to actual geometry
    COEFF myDefect;
    myDefect.user_random_field();

    GridTransformation<3,modelGeometry> gTrafo(model, helper.rank(),myDefect);
    typedef typename Dune::GeometryGrid<YGRID,GridTransformation<3,modelGeometry>> GRID;
    GRID grid(yaspgrid,gTrafo);
    if(helper.rank() == 0)
        std::cout << "Grid transformation complete" << std::endl;

    //Define Grid view
    typedef GRID::LeafGridView GV;
    const GV& gv = grid.leafGridView();

    if(helper.rank() == 0)
        std::cout << "Grid view set up" << std::endl;

    //file << "Time to transform Geometry " << timer.elapsed() << "\n";
    times[1] = timer.elapsed();
    timer.reset();

    //Set up problem
    typedef ElasticityProblem<GV,YGV, double, std::vector<int>, MAT_PROP, modelGeometry> PROBLEM;
    PROBLEM problem(gv,ygv,elemIndx2PG, matProp, model,myDefect);

    // Setup initial boundary conditions for each degree of freedom
    typedef Scalar_BC<GV,modelGeometry,double> InitialDisp;
    InitialDisp u1(gv, model,0), u2(gv, model,1), u3(gv,model,2);
    u1.setDof(1);
    u2.setDof(2);
    u3.setDof(3);

    // Wrap scalar boundary conditions in to vector
    typedef Dune::PDELab::CompositeGridFunction<InitialDisp,InitialDisp,InitialDisp>	InitialSolution;
    InitialSolution initial_solution(u1,u2,u3);

    if (helper.size() > 1){
        problem.setParallel(true); 
    }
    else {
        problem.setParallel(false); 
    }
    //Read out of ini file
    int solver_type = config.get<int>("solver_type", 1);
    problem.setSolver(solver_type);
    problem.setMaxIt(5000);
    problem.setVerbosity(config.get<int>("verbosity",1));
    problem.setTol(config.get<double>("tol",1e-4));
    int intorder=config.get<int>("intorder",5);
    problem.setIntOrder(intorder);

    // Construct grid function spaces for each degree of freedom
    typedef Dune::PDELab::OverlappingConformingDirichletConstraints CON;
    CON con;

    //file << "Time to set up problem and boundary conditions " << timer.elapsed() << "\n";
    times[2] = timer.elapsed();
    timer.reset();

    //Set up FEM space
    if(config.get<int>("elem_order",1) == 1){
        if(helper.rank() == 0){std::cout << "Piecewise linear elements" << std::endl; }
        const int element_order = 1; // Element order 1 - linear, 2 - quadratic
        const int dofel = 3*8;
        typedef Dune::PDELab::SerendipityLocalFiniteElementMap<GV,GV::Grid::ctype,double,element_order> FEM;

        FEM fem(gv);

        typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::none,1> Scalar_VectorBackend;

        typedef Dune::PDELab::GridFunctionSpace<GV, FEM, CON, Scalar_VectorBackend> SCALAR_GFS;
        SCALAR_GFS dispU1(gv,fem,con); dispU1.name("U1");
        SCALAR_GFS dispU2(gv,fem,con); dispU2.name("U2");
        SCALAR_GFS dispU3(gv,fem,con); dispU3.name("U3");
        typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::fixed,3> VectorBackend;  //Vectors are blocked
        typedef Dune::PDELab::CompositeGridFunctionSpace <VectorBackend,Dune::PDELab::EntityBlockedOrderingTag, SCALAR_GFS, SCALAR_GFS, SCALAR_GFS> GFS;
        const GFS gfs(dispU1, dispU2, dispU3);

        //file << "Time to set up FE space " << timer.elapsed() << "\n";
        times[3] = timer.elapsed();
        timer.reset();

        //FEM driver
        driver<PROBLEM,modelGeometry,GV,GFS,GRID,InitialSolution,dofel,FEM,CON>(fem,gfs,problem,gv,grid,initial_solution,con,helper,model, nelem[0],times);
    }
    if(config.get<int>("elem_order",1) == 2){
        const int element_order = 2; // Element order 1 - linear, 2 - quadratic
        if(config.get<int>("serendipity",0) == 1){
            if(helper.rank() == 0){std::cout << "Piecewise quadratic serendipity elements" << std::endl; }
            const int dofel = 3*20;
            typedef Dune::PDELab::SerendipityLocalFiniteElementMap<GV,GV::Grid::ctype,double,element_order> FEM;
            FEM fem(gv);

            typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::none,1> Scalar_VectorBackend;

            typedef Dune::PDELab::GridFunctionSpace<GV, FEM, CON, Scalar_VectorBackend> SCALAR_GFS;
            SCALAR_GFS dispU1(gv,fem,con); dispU1.name("U1");
            SCALAR_GFS dispU2(gv,fem,con); dispU2.name("U2");
            SCALAR_GFS dispU3(gv,fem,con); dispU3.name("U3");
            typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::fixed,3> VectorBackend;  //Vectors are blocked
            typedef Dune::PDELab::CompositeGridFunctionSpace <VectorBackend,Dune::PDELab::EntityBlockedOrderingTag, SCALAR_GFS, SCALAR_GFS, SCALAR_GFS> GFS;
            const GFS gfs(dispU1, dispU2, dispU3);

            std::cout << "GridFunctionSpace" << std::endl;
            //file << "Time to set up FE space " << timer.elapsed() << "\n";
            times[3] = timer.elapsed();
            timer.reset();

            //FEM driver
            driver<PROBLEM,modelGeometry,GV,GFS,GRID,InitialSolution,dofel,FEM,CON>(fem,gfs,problem,gv,grid,initial_solution,con,helper,model, nelem[0], times);
        }
        else{
            if(helper.rank() == 0){std::cout << "Piecewise quadratic elements" << std::endl; }
            const int dofel = 3*27;
            typedef Dune::PDELab::QkLocalFiniteElementMap<GV,GV::Grid::ctype,double,element_order> FEM;

            FEM fem(gv);

            typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::none,1> Scalar_VectorBackend;

            typedef Dune::PDELab::GridFunctionSpace<GV, FEM, CON, Scalar_VectorBackend> SCALAR_GFS;
            SCALAR_GFS dispU1(gv,fem,con); dispU1.name("U1");
            SCALAR_GFS dispU2(gv,fem,con); dispU2.name("U2");
            SCALAR_GFS dispU3(gv,fem,con); dispU3.name("U3");
            typedef Dune::PDELab::istl::VectorBackend<Dune::PDELab::istl::Blocking::fixed,3> VectorBackend;  //Vectors are blocked
            typedef Dune::PDELab::CompositeGridFunctionSpace <VectorBackend,Dune::PDELab::EntityBlockedOrderingTag, SCALAR_GFS, SCALAR_GFS, SCALAR_GFS> GFS;
            const GFS gfs(dispU1, dispU2, dispU3);

            //file << "Time to set up FE space " << timer.elapsed() << "\n";
            times[3] = timer.elapsed();
            timer.reset();

            //FEM driver
            driver<PROBLEM,modelGeometry,GV,GFS,GRID,InitialSolution,dofel,FEM,CON>(fem,gfs,problem,gv,grid,initial_solution,con,helper,model, nelem[0], times);
        }
    }
    return times;

}//end YaspGrid setup
