#ifndef PARALLEL_PARTITION__HH
#define PARALLEL_PARTITION__HH

//Partition grid for parallelism
#include<array>
template<int d>
class YaspPartition : public Dune::YLoadBalance<d>
{
private:
  int numProc ; // number of processes
public:
  YaspPartition(int num_proc_) : numProc(num_proc_) {};
  typedef std::array<int, d> iTupel;
  virtual ~YaspPartition() {}

  virtual void loadbalance (const iTupel& size, int P, iTupel& dims) const
  {
      std::fill(dims.begin(),dims.end(), 1);
      dims[0] = config.get<int>("ProcX",1);
      dims[1] = config.get<int>("ProcY",1);
      dims[2] = config.get<int>("ProcZ",1);

      if(dims[0]*dims[1]*dims[2]!=numProc){
          std::cout << "Given number of processors do not match config file, default load balancing used." << std::endl;
          if(int(numProc/8)*8 == numProc){
              dims[0] = 8;
              dims[1] = int(numProc/8);
              dims[2] = 1;
          }
          else{
              if(config.get<int>("geneo",0)==0 && int(sqrt(numProc))*int(sqrt(numProc)) == numProc){
                  dims[0] = sqrt(numProc);
                  dims[1] = sqrt(numProc);
              }
              else{
                  dims[0] = numProc;
              }
          }
      }
  }
};
#endif
