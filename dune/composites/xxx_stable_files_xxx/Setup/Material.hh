#ifndef MATERIAL__HH
#define MATERIAL__HH

namespace Dune{

namespace Composites{

//!  Material class. 
/*!
  Class which stores information about a material e.g. AS4/8552
*/

class Material{

public:

	Material(){}

	int Type;
	std::vector<double> matprop;
	double density; 

	void inline setType(int t){
		Type = t;
		if (t == 0){ matprop.resize(2);	}
		else if(t == 1){ matprop.resize(9);}
		else{ matprop.resize(21); }
	}

	void inline setProp(int i, double val){
		assert(i < matprop.size());
		matprop[i] = val;
	}

	void inline setDensity(double val){density = val;}


private:


};

}
}

#endif
