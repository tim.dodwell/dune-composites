

void inline GeometryBuilder(std::vector<Regions>& R){
	

	// Builds Overall Geometry and Mesh from a set of regions


	Dune::FieldVector<double,3> origin(0.0); // Define Origin, the default of this will be set to [0.0,0.0,0.0] 


	// Compute number of elements in x and y. Since structures are current prismatic and tensor product grid y elements must be the same so we check this

	int numRegions = R.size();

	int nelx = 0;
	int nely = R[0].nel[1];

	for (int i = 0; i < numRegions;i++){

		nelx += R[i].nel[0];

		assert(R[i].nel[1] == nely, "Overall tensor product grid y must be equal across all regions, check geometry input file");

	}

	// === For first case lets think about uniform grids, without any grading

	std::vector<std::vector<double>> coords(3);

	coords[0].resize(nelx + 1);

	coords[0][0] = origin[0];

	int k = 1; // initialise counter

	for (int i = 0; i < numRegions; i++){

		double hx = R[i].L[0] / R[i].nel[0]; // This makes grid uniform across a region TO DO:: Update for non-uniform grids

		for (int j = 0; j < R[i].nel[0] + 1; j++){

			coords[0][k] = coords[0][k-1] + hx;

			k++; // Increment Counter in the x-direction

		}
	}

	// === Prismatic mesh in y

	coords[1].resize(nely + 1);

	coords[1][0] = origin[0];

	double hy = R[0].L[1] / R[0].nel[1];

	for (int j = 1; j < R[0].nel[1] + 1; j++){
		coords[1][j] = coords[1][j-1] + hy;
	}

	// Build Coordinates in z
	
	// For now let's assume uniform thickness (not necessary though)
	int nelz = 0;
	for (int i = 0; i < R[0].layers.size(); i++){
		// for each layer
		nelz += R[0].layers[i].getElements();
	}

	coords[2].resize(nelz + 1);

	coords[2][0] = origin[2];

	int e = 0;

	for (int i = 0; i < R[0].layers.size(); i++){
		double hz = R[0].layers[i].getThickness() / R[0].layers[i].getElements();
		for (int k = 0; k < R[0].layers[i].getElements(); k++){
			e++;
			coords[2][e] = coords[2][e-1] + hz;
		}
	}







}