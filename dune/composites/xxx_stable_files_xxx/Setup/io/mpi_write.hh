#ifndef DUNE_MPI_WRITE_HH
#define DUNE_MPI_WRITE_HH

float **alloc2d(const int n, const int m) {
    float *data;
    data = (float*)malloc(n*m*sizeof(float));
    float **array;
    array = (float**)malloc(n*sizeof(float *));
    for (int i=0; i<n; i++)
        array[i] = &(data[i*m]);
    return array;
}

void mpi_write(Dune::MPIHelper& helper, std::string filename, std::vector<double>& buf){
    int rank = helper.rank();
    int size = helper.size();
    MPI_File   file;
    MPI_Status status;

    const int nrows=size;
    const int ncols=buf.size()+1;
    int locnrows = nrows/size;
    //define format
    std::string str_fmt="%8.3f ";
    const char *fmt=str_fmt.c_str();
    std::string str_endfmt="%8.3f\n";
    const char *endfmt=str_endfmt.c_str();

    const int charspernum=9;
    int startrow = rank * locnrows;
    int endrow = startrow + locnrows - 1;

    if (rank == size-1) {
        endrow = nrows - 1;
        locnrows = endrow - startrow + 1;
    }

    // allocate local data
    float **data;
    data = alloc2d(locnrows, ncols);

    // fill local data
    for (int i=0; i<locnrows; i++) {
        data[i][0] = rank;
        for (int j=1; j<ncols; j++) {
            data[i][j] = buf[j-1];
        }
    }

    // each number is represented by charspernum chars
    MPI_Datatype num_as_string;
    MPI_Type_contiguous(charspernum, MPI_CHAR, &num_as_string); 
    MPI_Type_commit(&num_as_string); 

    // convert our data into txt
    char *data_as_txt;
    data_as_txt = (char*)malloc(locnrows*ncols*charspernum*sizeof(char));
    int count = 0;
    for (int i=0; i<locnrows; i++) {
        for (int j=0; j<ncols-1; j++) {
            sprintf(&data_as_txt[count*charspernum], fmt, data[i][j]);
            count++;
        }
        sprintf(&data_as_txt[count*charspernum], endfmt, data[i][ncols-1]);
        count++;
    }

    //printf("%d: %s\n", rank, data_as_txt);

    // create a type describing our piece of the array 
    int globalsizes[2] = {nrows, ncols};
    int localsizes [2] = {locnrows, ncols};
    int starts[2]      = {startrow, 0};
    int order          = MPI_ORDER_C;

    MPI_Datatype localarray;
    MPI_Type_create_subarray(2, globalsizes, localsizes, starts, order, num_as_string, &localarray);
    MPI_Type_commit(&localarray);

    // open the file, and set the view 
    MPI_File_open(helper.getCommunicator(), filename.c_str(), 
                  MPI_MODE_CREATE | MPI_MODE_WRONLY | MPI_MODE_APPEND,
                  MPI_INFO_NULL, &file);

    MPI_File_set_view(file, 0,  MPI_CHAR, localarray, 
                           "native", MPI_INFO_NULL);

    MPI_File_write_all(file, data_as_txt, locnrows*ncols, num_as_string, &status);
    MPI_File_close(&file);

    MPI_Type_free(&localarray);
    MPI_Type_free(&num_as_string);

    free(data[0]);
    free(data);
}

#endif
