#ifndef _Cijkl_hh
#define _Cijkl_hh

// Cijkl is a class which computes Elasticity Tensor on each element and contains all required functions

#include "../MathFunctions/transpose.hh"
#include "../Setup/material.hh"

class CalCijkl{
    /* The Class Compute Cijkl - Calculates the Elastic Tensor sij = Cijkl for each element */
    typedef typename Dune::FieldMatrix<double,6,6> M;

public:
    // Constructor - Computes Cijkl in material coordinates for each material type
    CalCijkl(std::vector<Material> matprop, int my_rank) : C(matprop.size()){
        // For each material
        for (int i = 0; i < matprop.size(); i++){
            //isotropic material
            if (matprop[i].getType() == 0){
                double E = matprop[i].getE()[0];
                double nu = matprop[i].getNu()[0];

                double tmp = E / ( (1.0 + nu) * ( 1.0 - 2.0 * nu ) );
                double G = E / (2.0 * (1 + nu));

                C[i] = { { ( 1.0 - nu ) * tmp,  nu * tmp,           nu * tmp, 0,0,0},
                         { nu * tmp,            ( 1.0 - nu ) * tmp, nu * tmp, 0,0,0},
                         { nu * tmp,            nu * tmp,           ( 1.0 - nu ) * tmp, 0,0,0},
                         { 0,0,0,G,0,0},
                         { 0,0,0,0,G,0},
                         { 0,0,0,0,0,G}};
                if(my_rank == 0){ std::cout << "Resin material tensor: " << std::endl << C[i] << std::endl; }
            }

            if (matprop[i].getType() == 1)
            {
                double E1 = matprop[i].getE()[0];
                double E2 = matprop[i].getE()[1];
                double E3 = matprop[i].getE()[2];
                double nu12 = matprop[i].getNu()[0];
                double nu21 = nu12 * (E2 / E1);
                double nu13 = matprop[i].getNu()[1];
                double nu31 = nu13 * (E3 / E1);
                double nu23 = matprop[i].getNu()[2];
                double nu32 = nu23 * (E3 / E2);
                double G12 = matprop[i].getG()[0];
                double G13 = matprop[i].getG()[1];
                double G23 = matprop[i].getG()[2];

                Dune::FieldMatrix<double,3,3> Sinv = {{1./E1,-nu21/E2,-nu31/E3},{-nu12/E1,1./E2,-nu32/E3},{-nu13/E1,-nu23/E2,1./E3}}; 
                Sinv.invert();

                C[i][0][0] = Sinv[0][0];
                C[i][1][1] = Sinv[1][1];
                C[i][2][2] = Sinv[2][2];
                C[i][0][1] = Sinv[0][1]; C[i][1][0] = Sinv[1][0];
                C[i][0][2] = Sinv[0][2]; C[i][2][0] = Sinv[2][0];
                C[i][1][2] = Sinv[1][2]; C[i][2][1] = Sinv[2][1];
                C[i][3][3] = G23;
                C[i][4][4] = G13;
                C[i][5][5] = G12;
                if(my_rank == 0){ std::cout << "Ply material tensor: " << std::endl << C[i] << std::endl; }
            }
        } // end for each material 
    }

    //Turn 3D rotation matrix into a tensor rotation
    M getMatrix(Dune::FieldMatrix<double,3,3> A) const{
	    M R;
        R[0][0] = A[0][0]*A[0][0]; R[0][1] = A[0][1]*A[0][1]; R[0][2] = A[0][2]*A[0][2];
	    R[1][0] = A[1][0]*A[1][0]; R[1][1] = A[1][1]*A[1][1]; R[1][2] = A[1][2]*A[1][2];
	    R[2][0] = A[2][0]*A[2][0]; R[2][1] = A[2][1]*A[2][1]; R[2][2] = A[2][2]*A[2][2];

	    R[0][3] = 2.0*A[0][1]*A[0][2]; R[0][4] = 2.0*A[0][0]*A[0][2]; R[0][5] = 2.0*A[0][0]*A[0][1];
	    R[1][3] = 2.0*A[1][1]*A[1][2]; R[1][4] = 2.0*A[1][0]*A[1][2]; R[1][5] = 2.0*A[1][0]*A[1][1];
	    R[2][3] = 2.0*A[2][1]*A[2][2]; R[2][4] = 2.0*A[2][0]*A[2][2]; R[2][5] = 2.0*A[2][0]*A[2][1];

	    R[3][0] = A[1][0]*A[2][0]; R[3][1] = A[1][1]*A[2][1]; R[3][2] = A[1][2]*A[2][2];
	    R[4][0] = A[0][0]*A[2][0]; R[4][1] = A[0][1]*A[2][1]; R[4][2] = A[0][2]*A[2][2];
	    R[5][0] = A[0][0]*A[1][0]; R[5][1] = A[0][1]*A[1][1]; R[5][2] = A[0][2]*A[1][2];
	
	    R[3][3] = A[1][1]*A[2][2]+A[1][2]*A[2][1]; R[3][4] = A[1][0]*A[2][2]+A[1][2]*A[2][0]; R[3][5] = A[1][0]*A[2][1]+A[1][1]*A[2][0];
	    R[4][3] = A[0][1]*A[2][2]+A[2][1]*A[0][2]; R[4][4] = A[0][0]*A[2][2]+A[2][0]*A[0][2]; R[4][5] = A[2][0]*A[0][1]+A[2][1]*A[0][0];
	    R[5][3] = A[0][1]*A[1][2]+A[0][2]*A[1][1]; R[5][4] = A[0][0]*A[1][2]+A[0][2]*A[1][0]; R[5][5] = A[0][0]*A[1][1]+A[0][1]*A[1][0];
        return R;
    }



    void rotate_left(M& C_local, const std::vector<double> theta, int i_dim) const{
	    M R2(0.0),R2T(0.0);

        //Set up 3D rotation matrix
        double c = cos(theta[i_dim]*M_PI/180.0);
	    double s = sin(theta[i_dim]*M_PI/180.0);
	    Dune::FieldMatrix<double,3,3> A;
        if(i_dim == 2) A = {{c,s,0},{-s,c,0},{0,0,1}};
        if(i_dim == 1) A = {{c,0,-s},{0,1,0},{s,0,c}};
        if(i_dim == 0) A = {{1,0,0},{0,c,s},{0,-s,c}};

        //Transform to tensor rotation
	    R2 = getMatrix(A);

        //rotate the input matrix
        C_local.leftmultiply(R2);
    }

    void rotate_right(M& C_local, const std::vector<double> theta, int i_dim) const{
	    M R2(0.0),R2T(0.0);

        //set up 3D rotation matrix
        double c = cos(theta[i_dim]*M_PI/180.0);
	    double s = sin(theta[i_dim]*M_PI/180.0);
	    Dune::FieldMatrix<double,3,3> A;
        if(i_dim == 2) A = {{c,s,0},{-s,c,0},{0,0,1}};
        if(i_dim == 1) A = {{c,0,-s},{0,1,0},{s,0,c}};

        //Transform to tensor rotation and transpose
	    R2 = getMatrix(A);
	    transpose<M,6>(R2,R2T);

        //rotate the input matrix
	    C_local.rightmultiply(R2T);
    }

    void rotate(M& C, const std::vector<double> theta, int i_dim) const
    {
	    M R2(0.0),R2T(0.0);

        //Set up 3D rotation matrix
	    double c = cos(theta[i_dim]*M_PI/180.0);
	    double s = sin(theta[i_dim]*M_PI/180.0);
	    Dune::FieldMatrix<double,3,3> A;
        if(i_dim == 2) A = {{c,s,0},{-s,c,0},{0,0,1}};
        if(i_dim == 1) A = {{c,0,-s},{0,1,0},{s,0,c}};
        if(i_dim == 0) A = {{1,0,0},{0,c,s},{0,-s,c}};

        //Transform to tensor rotation and transpose
	    R2 = getMatrix(A);
	    transpose<M,6>(R2,R2T);
	
        //rotate the input matrix
        C.leftmultiply(R2);
	    C.rightmultiply(R2T);
    } // end eval


public:
    std::vector<Dune::FieldMatrix<double,6,6>> C;
};  //end class


#endif
