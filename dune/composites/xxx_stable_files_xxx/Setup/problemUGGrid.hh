#ifndef _problem_definition_UG_hh
#define _problem_definition_UG_hh

//UG/Alugrid version
template<typename GV, typename RF, typename PGMap, typename MAT_PROP, typename MODEL>
class ElasticityProblemUG :
    public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,GV::dimension 
    * GV::dimension,Dune::FieldMatrix<RF,GV::dimension,GV::dimension> >, ElasticityProblemUG<GV,RF,PGMap,MAT_PROP, MODEL> >
{
    public :
        typedef RF RFType;
        typedef Dune::PDELab::GridFunctionTraits<GV,RF,
                GV::dimension*GV::dimension,Dune::FieldMatrix<RF,GV::dimension,GV::dimension> > Traits;
        typedef Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<GV,RF,
                GV::dimension*GV::dimension,Dune::FieldMatrix<RF,GV::dimension,GV::dimension> >,
                ElasticityProblemUG<GV,RF,PGMap,MAT_PROP, MODEL> > BaseT;
        // ================================================================================
        // 			Constructor for ElasticityTensor
        // ================================================================================
        ElasticityProblemUG(const GV& gv_, const PGMap& elemIndx2PG, MAT_PROP mat_prop, MODEL model_)
            : gv(gv_), is(gv.indexSet()), Cijkl_strain(is.size(0)),Cijkl(is.size(0)), //Cijkl_local(is.size(0)), 
            density(is.size(0)), model(model_), theta(is.size(0)), thetaFct(is.size(0)), lay_id(is.size(0)),
            materialType(is.size(0)), geometryType(is.size(0))
    {
        typedef typename GV::Traits::template Codim<0>::Iterator ElementIterator;
        // ================================================================================
        // 	<< a >>		Compute Elasticity Tensors for Isotropic & Orthropic Cases
        // ================================================================================
        auto my_rank = gv.comm().rank();
        CalCijkl<MAT_PROP> field(mat_prop, my_rank);
        // ================================================================================
        // 	<< b >>		Calculate Cijkl[id] in each element
        // ================================================================================
        for (ElementIterator it = gv.template begin<0>(); it!=gv.template end<0>(); ++it)
        { // loop through each element
            int id = is.index(*it); // Get element id
            int pg = elemIndx2PG[id]; // Map from element to physical group
            if(pg == 0) pg = 100;
            // Physical Groups are (at least) 3 digit numbers which define both region and layer id
            // abc - a is the region number and bc - is the layer id
            // Currently there is no maximum on the number regions (i.e 12323 defines region 123 and layer 23)
            // There is at the moment a maximum number of layers = 100
            int region_id = floor(pg / 100) - 1;
            int layer_id  = pg - (1 + region_id) * 100;

            int mat_id = mat_prop.lay_mat[region_id][layer_id];

            auto x = it->geometry().center();
            double dt2 = mat_prop.region_data[region_id][1];
            if(mat_prop.region_info[region_id] == 2 ){
                double a0 = mat_prop.region_data[region_id][4];
                double a2 = mat_prop.region_data[region_id][5];
                dt2 += 180./M_PI*atan2( x[0] - a0, a2 - x[2]);  //calculate angle
            }
            theta[id] = {mat_prop.region_data[region_id][0], dt2, mat_prop.region_data[region_id][2] + mat_prop.lay_rot[region_id][layer_id]};
            thetaFct[id] = [=](Dune::FieldVector<double,3> xh){return dt2;};
            lay_id[id] = layer_id;
            geometryType[id] = mat_prop.region_info[region_id];

            materialType[id] = mat_prop.lay_mat[region_id][layer_id];

            field.eval(Cijkl[id],theta[id],mat_id,mat_prop.materialType[mat_id],region_id);
            //field.eval_local(Cijkl_local[id],theta[id],mat_id,mat_prop.materialType[mat_id],region_id);
            field.eval_strain(Cijkl_strain[id],theta[id],mat_id,mat_prop.materialType[mat_id],region_id);

            density[id] = mat_prop.matprop[mat_id][21];
        } // End for-loop of each element
    }; // End Constructor

        inline void evaluate(const typename Traits::ElementType& e,
                         Dune::FieldMatrix<double,6,6>& y) const{
        y = Cijkl[is.index(e)];
    }

    inline void evaluate(int id, Dune::FieldMatrix<double,6,6>& y) const{
        y = Cijkl[id];
    }
    /*inline void evaluate_local(int id, Dune::FieldMatrix<double,6,6>& y) const{
      y = Cijkl_local[id];
    }
    inline void evaluate_local(const typename Traits::ElementType& e,
                         Dune::FieldMatrix<double,6,6>& y) const{
        y = Cijkl_local[is.index(e)];
    }    */
    inline void evaluate_strain(int id, Dune::FieldMatrix<double,6,6>& y) const{
      y = Cijkl_strain[id];
    }
    inline void evaluate_strain(const typename Traits::ElementType& e,
                         Dune::FieldMatrix<double,6,6>& y) const{
        y = Cijkl_strain[is.index(e)];
    }

    void setDofel(int number){
      dofel = number;    
    }
    inline int getDofel(){
      return dofel;      
    }

    void setIntOrder(int order){
      intorder = order;   
    }    
    inline const int getIntOrder() const{
      return intorder;
    }

    inline  std::vector<double> getTheta(int id) const{
      return theta[id];
    }
    inline  std::vector<double> getTheta(const typename Traits::ElementType& e) const{
      return theta[is.index(e)];    
    }
    inline  std::vector<double> getThetaFct(const typename Traits::ElementType& e, Dune::FieldVector<double,3> x) const{
        std::vector<double> angle = theta[is.index(e)];
        //double dt = thetaFct[is.index(e)](x);
        //dt = 180.0/M_PI*dt;
        //angle[1] = dt;
        return angle;
    }

    inline  int getLayID(int id) const{
      return lay_id[id];    
    }
    inline  int getLayID(const typename Traits::ElementType& e) const{
      return lay_id[is.index(e)];    
    }
    inline  int getMatID(int id) const{
      return geometryType[id];    
    }
    inline  int getMatID(const typename Traits::ElementType& e) const{
      return geometryType[is.index(e)];
    }
        inline  int getMaterialType(int id) const{
      return materialType[id];    
    }
    inline  int getMaterialType(const typename Traits::ElementType& e) const{
      return materialType[is.index(e)];
    }

    void setSolver(int solver){
        solver_type = solver;
    }
    inline int getSolver(){
        return solver_type;
    }

    void setParallel(bool answer) {
        parallel = answer;
    }

    inline bool isParallel() const{
        return parallel;
    }

    void setMaxIt(int N){
        maxIt = N;
    }

    inline int getMaxIt(){
        return maxIt;
    }

    void setVerbosity(int N){
        verbosity = N;
    }

    inline int getVerbosity(){
        return verbosity;
    }

    void setTol(double tol)
    {
        tolerance = tol;
    }

    inline double getTol(){
        return tolerance;
    }

    inline void evaluateDensity(const typename Traits::ElementType& e, Dune::FieldVector<double,3>& f) const{
        f[0] = 0.0;
        f[1] = 0.0;
        f[2] = 0.0;
        if(config.get<int>("geneo",0)==1)
            f[2] = -10.0; // 9.80665 * density[is.index(e)];
    }

    inline void evaluateHeat(Dune::FieldVector<double,6>& f,bool resin) const{
        f = 0.0;
        if(config.get<bool>("residualHeat",false)){
            double deltaT = -160.; //temperature difference
            double alpha11 = config.get<double>("alpha11", -.342)*1e-6;
            double alpha22 = config.get<double>("alpha22", 25.8)*1e-6;
            double alpha33 = config.get<double>("alpha33", 25.8)*1e-6;
            double alpharesin = config.get<double>("alpharesin",25.8)*1e-6;
            if(resin){
                f[0] = alpharesin*deltaT;
                f[1] = alpharesin*deltaT;
                f[2] = alpharesin*deltaT;
            }
            else{
                f[0] = alpha11*deltaT;
                f[1] = alpha22*deltaT;
                f[2] = alpha33*deltaT;
            }
        }
    }

    inline void evaluateNeumann(const Dune::FieldVector<double,3> &x,Dune::FieldVector<double,3>& h,const  Dune::FieldVector<double,3>& normal) const{
        // s_ij n_j = h_i
        double p_m = config.get<double>("moment",1); // Initialise pressure & traction values
        double p_p = config.get<double>("pressure",1); // Initialise pressure & traction values
        h = 0;

        //handle special cases
        if(config.get<int>("geneo",0)==1) p_m = 0.;
        double xval = model.radius;
        if(model.num_limbs == 2) xval += model.limb;

        double resin_treatment = 0;
        if(config.get<int>("edge_treatment",0) == 1) resin_treatment = config.get<double>("edge_width");

        //If model of a cube, apply a pressure to the top face
        if(model.radius == 0){
            if(x[0] > xval && x[2] > model.thickness-1e-06){
                for (int i = 0; i < 3; i++)
                    h[i] = p_p * normal[i];
            }
            return;
        }
        //lshape
        //apply a pressure if there is no multi point constraint on the limb
        if(config.get<int>("mpc",0)==0){
            if( fabs(x[0] - xval) < 1e-06 && x[2] > model.radius+model.thickness - 1e-06){
                for (int i = 0; i < 3; i++)
                    h[i] = p_p * normal[i];
            }
            return;
        }
        //or otherwise apply a moment to the limb
        if(x[2] > model.radius+model.thickness+model.limb - 1e-06){

            if(x[1] > resin_treatment + 1e-06  && x[1] < model.Y-resin_treatment-1e-06){
                if(fabs(x[0] - (xval) ) < model.thickness*0.1){
                    for(int i = 0; i < 3; i++)
                        h[i] = -p_m*normal[i];
                }
                if(fabs(x[0] - (xval+model.thickness)) < model.thickness*0.1){
                    for(int i = 0; i < 3; i++)
                        h[i] = p_m*normal[i];
                }
            }
        }
    }

    private :

    const GV& gv;
    const typename GV::IndexSet& is;
    std::vector<Dune::FieldMatrix<RF,6,6>> Cijkl;
    std::vector<Dune::FieldMatrix<RF,6,6>> Cijkl_strain;
    //std::vector<Dune::FieldMatrix<RF,6,6>> Cijkl_local;
    std::vector<RF> density;
    MODEL model;
    std::vector<std::vector<double>> theta;
    std::vector<std::function<double(Dune::FieldVector<double,3>)>> thetaFct;
    std::vector<int> lay_id;
    std::vector<int> geometryType;
    std::vector<int> materialType;
    std::vector<Dune::FieldVector<RF,6>> eps;
    int solver_type;
    int maxIt;
    int verbosity;
    int dofel;
    int intorder;
    bool parallel;
    double tolerance;

};

#endif
