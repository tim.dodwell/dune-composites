

class Layer{
	
	public:

		Layer(){};

		void inline setElements(int nel_){ nel = nel_; }

		void inline setMaterial(int material_){ material = material_;}

		void inline setThickness(double thickness_){ thickness = thickness_;}

		void inline setOrientation(double theta_){ theta = theta_; }

		double inline getOrientation(){ return theta; }

		int inline getElements(){ return nel; }

		double inline getThickness(){ return thickness; }

		int inline getMaterial(){ return material; }



	private:

		double thickness;
		int id;
		int material;
		int nel;
		double theta;


};