#ifndef REGION__HH
#define REGION__HH

#include "Layer.hh"

namespace Dune{

namespace Composites{

//!  Material class. 
/*!
  Class which stores information about a material e.g. AS4/8552
*/

class Region{

public:

	int Type;

	std::vector<int> nel;
	std::vector<double> L;
	std::vector<double> theta;

	std::vector<Layer> layers;

	int numParameters;

	int numLayers;

	Region(){

		nel.resize(3);
		L.resize(3);
		theta.resize(3);

	}

	void inline setNumLayers(int nl){
		numLayers = nl;
		layers.resize(numLayers);
	}

	

	void inline setType(int t){
		Type = t;
		numParameters = 8;
		
	}

	void inline setParameter(int i, double value){

	if (Type == 0 ){ // Flat plate

			switch(i)
			{
				case 0:
					L[0] = value;
					break;
				case 1:
					L[1] = value;
					break;
				case 2:
					nel[0] = value;
					break;
				case 3:
					nel[1] = value;
					break;
				case 4:
					theta[0] = value;
					break;
				case 5:
					theta[1] = value;
					break;
				case 6:
					theta[2] = value;
					break;
				default:
					assert(i > numParameters); 
				

			}

	} // end if Type == 0

	}



	private:


};

}
}

#endif

