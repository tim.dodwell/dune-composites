// Define Scalar Dirichlet Boundary Conditions
template<typename GV, typename MODEL, typename RF>
class Scalar_BC :
    public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
    Scalar_BC<GV,MODEL,RF> >,
    public Dune::PDELab::InstationaryFunctionDefaults
{
    public:

        typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
        typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, Scalar_BC<GV,MODEL,RF>> BaseT;

        typedef typename Traits::DomainType DomainType;
        typedef typename Traits::RangeType RangeType;

        // Constructor
        Scalar_BC(const GV & gv, MODEL& model_, int i_dim_) :
            BaseT(gv), model(model_), i_dim(i_dim_){ }

        template<typename I>
            bool isDirichlet(const I & ig, const typename Dune::FieldVector<typename I::ctype, I::dimension-1> & x) const
            {
                Dune::FieldVector<double,3> xg = ig.geometry().global( x );
                if(xg[0] < 1e-6 || xg[0] > model.limb-1e-6)
                    return true;

                return false;
            }

        inline void evaluateGlobal(const DomainType & x, RangeType & u) const
        {
            u = 0.0;
            if(x[0] > model.limb-1e-6 && i_dim==0)
                u = .06;

        } // end inline function evaluateGlobal

        void setDof(int degree_of_freedom){
            dof = degree_of_freedom;
        }

    private:
        int dof;
        MODEL& model;
        int i_dim;
};

