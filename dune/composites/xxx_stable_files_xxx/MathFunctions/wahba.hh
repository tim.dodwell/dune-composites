#include "transpose.hh"

template<typename COORDS>
void wahba_prob(COORDS& w,COORDS& v,COORDS& Rot)
{
/*============================================================================
 * 							Wabhda Problem 
 * ============================================================================
 * 	Given two coordinate systems defined by matrices w and v where each columns
 *  is a vector
 * 
 *  This algorith returns the rotation matrix R which minimises the Lagrangian 
 * 
 *  J(R) = 0.5 * \sum_i=1^N || w_i - R * v_i ||^2
 * 
 *  i.e. the rotation which makes the two coordinates best align in the least 
 * 	squared sense
 * 
 *  Dr. T. Dodwell - University of Exeter - t.dodwell@exeter.ac.uk
 *  2 / 10 / 15
 * ============================================================================
 */

	int K = w.cols();
	int n = w.rows();

	Dune::FieldMatrix<double,3,3> B(0.0), V(0.0), VT(0.0), S(0.0), U(0.0);
	
	for (int k = 0; k < K; k++){
		for (int i = 0; i < n; i++){
			for (int j = 0; j < n; j++){
				B[i][j] += w[i][k] * v[j][k];
			}
		}
	}
	
	
	// Compute single value decomposition using Jacobi Method in svd.hh
	
	svd<Dune::FieldMatrix<double,3,3>>(B,U,S,V);

	// Construct the Rotation Matrix R = U * M * V'
	// Where M = diag[1,1,det(U) * det(V)];
	
    transpose<Dune::FieldMatrix<double,3,3>,3>(V,VT); // transpose V
    Dune::FieldMatrix<double,3,3> M(0.0); // Initialise M
	for (int i = 0; i < n; i++){
		M[i][i] = 1.0;
	}
	M[2][2] = V.determinant() * U.determinant();
	
	Rot = M.leftmultiply(U);
	Rot = Rot.rightmultiply(VT);

}
