#ifndef CREATE_TP_GRID__HH
#define CREATE_TP_GRID__HH

std::vector<double> meshRatio(int N, double k){
    if(k == 1){
        std::vector<double> h(N);
        for(int i = 0; i < N; i++)
            h[i] = 1./N;
        return h;
    }
    else{
        if((N/2)*2 != N){ N += 1; } //ensure this number is even
        if(N == 2){
            return {0.5, 0.5};
        }
        int M = N/2;
        std::vector<double> h(N), a(M);
        int k2 = k;
        if(k<1) k2 = 1.0/k;
        for(int i = 0; i < M; i++){
            a[i] = float(i)*(k2-1)/(M-1)+1.;
        }
        double h0 = 1./M*1./(1.+(k2-1)/2.);
        for(int i = 0; i < M; i++){
            h[i] = a[i]*h0/2.;
            h[N-1 - i] = a[i]*h0/2.;
        }
        if(k<1){
            auto h2 = h;
            for(int i = 0; i< M; i++){
                h2[i]   = h[M-1-i];
                h2[M+i] = h[N-1-i];
            }
            return h2;
        }
        return h;
    }
}


template<typename MODEL>
std::array< std::vector<double>, 3> createCoords(const MODEL& model, std::vector<int> nelem){
    double ratioX = config.get<double>("ratioX",1);
    double grading_loc = model.limb+model.radius/2;
    double defect_size = config.get<double>("defect_size",0);
    if(defect_size>0 && ratioX!=1){
        grading_loc = config.get<double>("defect_locationX",model.limb+model.radius/2);
        if(model.num_limbs == 2 && (grading_loc<model.limb || grading_loc>model.limb+model.radius)){
                std::cout << "Invalid defect location, check to ensure it is in the curved part of the lshape" << std::endl;
                exit(EXIT_FAILURE);
        }
    }
    double ratioY = config.get<double>("ratioY",1);
    double ratioZ = config.get<double>("ratioZ",1);;

	std::array<std::vector<double>,3>  coords;

	if(model.radius < 1e-06){
        auto hx = meshRatio(nelem[0],ratioX);
		std::vector<double> ch0(nelem[0]+1);
        ch0[0] = 0;
		for(unsigned int i = 1; i < ch0.size(); i++)
			ch0[i] = ch0[i-1] + model.limb*model.num_limbs*hx[i-1];
		coords[0] = ch0;
	}
	else{
        if(model.limb_a>0){//wingbox
            double  h_limb_b = model.limb_b/nelem[0];
            int n_limb_b = nelem[0];
            int n_limb_a = std::ceil(model.limb_a/h_limb_b);
            double  h_limb_a = model.limb_a/n_limb_a;

            auto meshgrad_a = meshRatio(n_limb_a, ratioX);
            auto meshgrad_b = meshRatio(n_limb_b, ratioX);

            std::vector<double> ch0(2*n_limb_a+2*n_limb_b+4*nelem[1]+1);
            ch0[0] = 0.0;

            for(int i = 1; i <= n_limb_a; i++)
                ch0[i] = ch0[i-1] + meshgrad_a[i-1] * model.limb_a;
            int cnt = n_limb_a+1;
            for(int i = cnt; i < cnt+nelem[1]; i++)
                ch0[i] = ch0[i-1] + model.radius/nelem[1];
            cnt += nelem[1];
            for(int i = cnt; i < cnt+n_limb_b; i++)
                ch0[i] = ch0[i-1] + meshgrad_b[i-cnt] * model.limb_b;
            cnt += n_limb_b;
            for(int i = cnt; i < cnt+nelem[1]; i++)
                ch0[i] = ch0[i-1] + model.radius/nelem[1];
            cnt += nelem[1];
            for(int i = cnt; i < cnt+n_limb_a; i++)
                ch0[i] = ch0[i-1] + meshgrad_a[i-cnt] * model.limb_a;
            cnt += n_limb_a;
            for(int i = cnt; i < cnt+nelem[1]; i++)
                ch0[i] = ch0[i-1] + model.radius/nelem[1];
            cnt += nelem[1];
            for(int i = cnt; i < cnt+n_limb_b; i++)
                ch0[i] = ch0[i-1] + meshgrad_b[i-cnt] * model.limb_b;
            cnt += n_limb_b;
            for(int i = cnt; i < cnt+nelem[1]; i++)
                ch0[i] = ch0[i-1] + model.radius/nelem[1];
            coords[0] = ch0;

        }
        else{
        if(model.num_limbs == 2){
            std::vector<double> ch0(2*nelem[0]+nelem[1]+1);
            ch0[0] = 0.0;
            for(int i = 1; i <= nelem[0]; i++)
                ch0[i] = i*model.limb/nelem[0];
            auto h = meshRatio(nelem[1],ratioX);
            int M = nelem[1]/2;
            for(int i = 1; i < M; i++)
                ch0[i+nelem[0]] = ch0[nelem[0]+i-1] + (h[i-1]*2)*(grading_loc-model.limb);
            for(int i = M; i < nelem[1]+1; i++)
                ch0[i+nelem[0]] = ch0[nelem[0]+i-1] + (h[i-1]*2)*(model.radius-grading_loc+model.limb);
            for(int i = 1; i <= nelem[0]; i++)
                ch0[i+nelem[0]+nelem[1]] = model.limb+model.radius + i*model.limb/nelem[0];
            coords[0] = ch0;
        }
        else{
            std::vector<double> ch0(nelem[0]+nelem[1]+1);
            ch0[0] = 0.0;
            auto h = meshRatio(nelem[1],ratioX);
            for(int i = 1; i <= nelem[1]; i++)
                ch0[i] = ch0[i-1]+model.radius*h[i-1];

            for(int i = 1; i <= nelem[0]; i++)
                ch0[i+nelem[1]] = model.radius + i*model.limb/nelem[0];
            coords[0] = ch0;
        }
        }
    }

    if(config.get<int>("edge_treatment",0)==0){
        auto h = meshRatio(nelem[2],ratioY);
        std::vector<double> ch1(h.size()+1);
        ch1[0] = 0.;
        for(unsigned int i = 1; i < ch1.size(); i++){
            ch1[i] = ch1[i-1] + model.Y * h[i-1];
        }
        coords[1] = ch1;
        //for(int i = 0; i < ch1.size(); i++){ std::cout << "ch1 "<<ch1[i] << std::endl;}
    }
    else{
        int edge_n = config.get<int>("edge_n",0);
        assert(edge_n > 0);
        auto hedge = meshRatio(edge_n, config.get<double>("edge_bias"));
        auto h = meshRatio(nelem[2],ratioY);
        std::vector<double> ch1(h.size()+2*edge_n+1);
        ch1[0] = 0.;
        for(unsigned int i = 1; i <= edge_n; i++)
            ch1[i] = ch1[i-1] + config.get<double>("edge_width")*hedge[i-1];
        int idx = edge_n+1;
        for(unsigned int i = 0; i < h.size(); i++)
            ch1[i+idx] = ch1[i+idx-1] + (model.Y-config.get<double>("edge_width")*2) * h[i];
        idx = idx+h.size();
        for(unsigned int i = 0; i < edge_n; i++)
            ch1[i+idx] = ch1[i+idx-1] + config.get<double>("edge_width")*hedge[i];
        coords[1] = ch1;
        //for(int i = 0; i < ch1.size(); i++){ std::cout << "ch1 "<<ch1[i] << std::endl;}
    }


  auto h1 = meshRatio(nelem[4],ratioZ); //resin
  auto h2 = meshRatio(nelem[3],ratioZ); //composite

  int n = h1.size();
  int m = h2.size();
  int size2;
  if((model.num_layers/2)*2==model.num_layers){ //even
     size2 = (model.num_layers/2*n+ (model.num_layers/2)*m+1);
  }
  else{
     size2 = (model.num_layers/2*n+ (model.num_layers/2+1)*m+1);
  }
  std::vector<double> ch2(size2);
  ch2[0] = 0;
  int layer_switch  = 1;
  int cnt = 1;
  for (int i = 0; i < model.num_layers; i++){
    for (int j = 1; j <= m*layer_switch+n*(1-layer_switch); j++){
      if(layer_switch == 1){
          ch2[cnt] = ch2[cnt-1] + h2[j-1] * model.layer_com_h;
      }
      if(layer_switch == 0){
          ch2[cnt] = ch2[cnt-1] + h1[j-1] * model.layer_res_h;
      }
      cnt  = cnt+1;
    }
    layer_switch = (i+1)/2 - i/2;
  }
  coords[2] = ch2;
  return coords;

}

#endif
