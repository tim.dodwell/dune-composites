#include "RandomFields/integrator.hh"
#include "RandomFields/defectGenerator.hh"

#ifndef GRIDTRANSFORMATION__HH
#define GRIDTRANSFORMATION__HH

template<typename DomainVector, typename RangeVector,class MODEL>
class GeometryFct{
    public:
        GeometryFct(const MODEL& model_, COEFF myDefect_) : model(model_),  myDefect(myDefect_)
        {
        }

        RangeVector returnY(const DomainVector& x) const{
            double theta;
            return returnY(x,theta);
        }

        RangeVector returnY(const DomainVector& x, double& theta) const{
            RangeVector y = x;
            RangeVector ydef = defect(x);

            //Transformation for wingbox
            //no transformation needed for first limb section
            double offset =  model.radius+model.thickness;
            y[0] = x[0] + offset;
            //transform first radial section
            if(x[0] >= model.limb_a && x[0] <= model.limb_a+model.radius){
                //Angle in radians
                theta = M_PI/2*(ydef[0]-model.limb_a)/model.radius;
                //Radius
                double r = model.radius + model.thickness - ydef[2];
                y[0] = offset + model.limb_a + r*sin(theta);
                y[1] = ydef[1];
                y[2] = model.radius + model.thickness - r*cos(theta);
            }
            //Model the second limb
            if(x[0] > model.limb_a+model.radius && x[0]<=model.limb_a+model.limb_b+model.radius){
                theta = M_PI/2;
                double y0 = ydef[0] - model.limb_a - model.radius;
                double y2 = ydef[2];
                y[0] = offset + model.limb_a + model.radius + model.thickness - y2;
                y[1] = ydef[1];
                y[2] = model.radius + model.thickness + y0;
            }
            //model second radial section
            if(x[0] >= model.limb_a+model.limb_b+model.radius && x[0] <= model.limb_a+model.limb_b+2*model.radius){
                //Angle in radians
                theta = M_PI/2*(ydef[0]-model.limb_a-model.limb_b-model.radius)/model.radius + M_PI/2;
                //Radius
                double r = model.radius + model.thickness - ydef[2];
                y[0] = offset + model.limb_a + r*sin(theta);
                y[1] = ydef[1];
                y[2] = model.radius + model.thickness + model.limb_b - r*cos(theta);
            }
            //Model the third limb
            if(x[0] > model.limb_a + model.limb_b + 2*model.radius && x[0]<=2*model.limb_a + model.limb_b + 2*model.radius){
                theta = M_PI;
                double y0 = ydef[0] - model.limb_a -model.limb_b - 2*model.radius;
                double y2 = ydef[2];
                y[0] = offset + model.limb_a + cos(theta)*y0 - sin(theta)*y2;
                y[1] = ydef[1];
                y[2] = 2*model.radius + 2*model.thickness +model.limb_b + sin(theta)*y0 + cos(theta)*y2;
            }
            //Model the third radial section
            if( x[0] > 2*model.limb_a + model.limb_b + 2*model.radius && x[0] <= 2*model.limb_a + model.limb_b + 3*model.radius){
                //Angle in radians
                theta = M_PI/2*(ydef[0]-2*model.limb_a-model.limb_b-2*model.radius)/model.radius + M_PI;
                //Radius
                double r = model.radius + model.thickness - ydef[2];
                y[0] = offset + r*sin(theta);
                y[1] = ydef[1];
                y[2] = model.radius + model.thickness + model.limb_b - r*cos(theta);
            }
            //Model the fourth limb
            if(x[0] > 2*model.limb_a + model.limb_b + 3*model.radius && x[0]<=2*model.limb_a + 2*model.limb_b + 3*model.radius){
                theta = 3*M_PI/2;
                double y0 = ydef[0] - 2*model.limb_a - model.limb_b - 3*model.radius;
                double y2 = ydef[2];
                y[0] = offset - model.radius - model.thickness + cos(theta)*y0 - sin(theta)*y2;
                y[1] = ydef[1];
                y[2] = model.radius + model.thickness + model.limb_b + sin(theta)*y0 + cos(theta)*y2;
            }
            //Model final radial section
            if( x[0] > 2*model.limb_a + 2*model.limb_b + 3*model.radius ){
                //Angle in radians
                theta = M_PI/2*(ydef[0]-2*model.limb_a-2*model.limb_b-3*model.radius)/model.radius + 3*M_PI/2;
                //Radius
                double r = model.radius + model.thickness - ydef[2];
                y[0] = offset + r*sin(theta);
                y[1] = ydef[1];
                y[2] = model.radius + model.thickness - r*cos(theta);
            }
            return y;
        }
        double returnTheta(const DomainVector& x) const{
            RangeVector y,yph;
            double theta,thetaph;
            double h = 1e-10;
            RangeVector xph = x; xph[0] += h/2.;
            RangeVector xmh = x; xmh[0] -= h/2.;
            y = returnY(xmh,theta);
            yph = returnY(xph,thetaph);
            auto angle = atan2((yph[2]-y[2])/h,(yph[0]-y[0])/h);
            return angle;
        }


    private:

        RangeVector defect(const DomainVector &x) const{
            //Get defect data from ini file
            double defect_size = config.get<double>("defect_size",0);
            RangeVector y = x;
            if(defect_size>0){
                //int wavelength = config.get<int>("wavelength",7);
                Dune::FieldVector<double,3> damping = {config.get<double>("dampingX",1),config.get<double>("dampingY",1),config.get<double>("dampingZ",1)};
                Dune::FieldVector<double,3> defect_location = {config.get<double>("defect_locationX",model.radius/2.+model.limb_a),
                    config.get<double>("defect_locationY",model.Y/2.),
                    config.get<double>("defect_locationZ",model.thickness/2)};
                //Define defect function
                if(x[0] > model.limb_a && x[0] < model.limb_a+model.radius){   //Place defect only in the curved section
                    if(config.get<std::string>("defect_type","simple") == "simple"){
                            double dampingZ = damping[2];
                            if(x[2] > defect_location[2]) dampingZ *= 4;
                            y[2] += defect_size
                                * 1./pow(cosh(damping[0] * M_PI * (defect_location[0] - x[0])/model.radius),2.)
                                * 1./pow(cosh(damping[1] * M_PI * (defect_location[1] - x[1])/model.Y),2.)
                                * 1./pow(cosh(dampingZ   * M_PI * (defect_location[2] - x[2])/model.thickness),2.);
                    }
		    if(config.get<std::string>("defect_type","simple") == "MC"){
		      // In curvy bit
		      double xval = (x[0]-model.limb_a)/model.radius * config.get<double>("LengthX"); // xval goes from 0 to 346

		      double pix = 5.0/58.0;
		      xval = xval*pix; // xval converted to mm

		      double nexp = 4.0; // exponential for decay function in x. Must always be even.
		
		      auto xd = std::log(1.0/98); // because the processed image is 98 pixels tall
		      auto xdamp = std::exp(damping[0] * pow((x[0] - defect_location[0]),nexp) / (pow((model.radius/2.0),nexp) / xd ));

		      auto zd = std::log(1.0/98.0);
		      auto zcentre = model.thickness - defect_location[2]; // roughly the depth of the peak in the z-direction i.e. centre of the focussed part of scan	     
		      double zdamp; //asymmetrical decay function to ensure 0 deflection at tool face

		      if(x[2] < zcentre)
			zdamp = std::exp(damping[2] * pow((x[2] - zcentre),2) / (pow(zcentre,2) / zd)); // this one is different because the wrinkle is located at the inner radius.
		      else
			zdamp = std::exp(damping[2] * pow((x[2] - zcentre),2) / (pow((model.thickness - zcentre),2) / zd)); // this one is different because the wrinkle is located at the inner radius.

		      auto yd = std::log(1.0 /98.0);
		      auto ydamp = std::exp(damping[1] * pow((x[1] - defect_location[1]),2) / (pow((model.Y/2.0),2) ));

		      for (int i = 0; i < myDefect.getN(); i++){

			y[2] += defect_size * myDefect.evalPhi(xval,i) * myDefect.getxi(i) * xdamp * ydamp * zdamp;

		      }
		    }
                    if(config.get<std::string>("defect_type","simple")  == "cosdefect" ){
                        double theta = 2*((x[0]-model.limb)/model.radius - 1./2.);
                        double l = x[1];
                        double r = model.radius + (model.thickness - x[2]);

                        auto Amax = config.get<double>("Amax");

                        auto rmin = config.get<double>("rmin");
                        auto rAmax = config.get<double>("rAmax");
                        auto rmax = config.get<double>("rmax");

                        auto rthetamin = config.get<double>("rthetamin");
                        auto rthetamax = config.get<double>("rthetamax");
                        auto rlmin = config.get<double>("rlmin");
                        auto rlmax = config.get<double>("rlmax");

                        auto thetamin = config.get<double>("thetamin");
                        auto thetamax = config.get<double>("thetamax");
                        auto omega1 = config.get<double>("omega1");
                        auto omega2 = config.get<double>("omega2");

                        auto lmin = config.get<double>("lmin");
                        auto lmax = config.get<double>("lmax");
                        auto psi1 = config.get<double>("psi1");
                        auto psi2 = config.get<double>("psi2");

                        double A = 0;
                        if(r > rmin  && r < rAmax+1e-03) A = Amax*(r - rmin)/(rAmax-rmin);
                        if(r >= rAmax && r < rmax)  A = Amax*(rmax-r)/(rmax-rAmax);

                        double theta1 = thetamin + omega1*(r - rthetamin);
                        double theta2 = thetamax + omega2*(r - rthetamax);

                        double l1 = lmin + psi1*(r - rlmin);
                        double l2 = lmax + psi2*(r - rlmax);

                        if( fabs(A) >1e-6){
                            if( l<l2 && l>l1 && theta>theta1 && theta<theta2){
                                double defect_size = A/4. * (1-cos(2*M_PI*(theta-theta1)/(theta2-theta1))) * (1-cos(2*M_PI*(l-l1)/(l2-l1)));
                                y[2] -= defect_size;
                            }
                        }
                    }
                }
            }

            return y;
        }

        const MODEL& model;
        COEFF myDefect;

};


//Grid Transformation
template <int dim, class MODEL>
class GridTransformation
: public Dune :: AnalyticalCoordFunction< double, dim, dim, GridTransformation <dim,MODEL> >
{
  typedef GridTransformation This;
  typedef Dune :: AnalyticalCoordFunction< double, dim, dim, This > Base;

public:
  typedef typename Base :: DomainVector DomainVector;
  typedef typename Base :: RangeVector RangeVector;

  GridTransformation(MODEL& model_, int my_rank_,COEFF myDefect_) : model(model_), giveOutput(1), my_rank(my_rank_), myDefect(myDefect_)
  {
  }

  void evaluate ( const DomainVector &x, RangeVector &y ) const
  {
      GeometryFct<DomainVector,RangeVector,MODEL> g(model,myDefect);
      y = g.returnY(x);
  }
private:
  COEFF myDefect;
  mutable int giveOutput;
  MODEL model;
  int my_rank;
};

#endif
