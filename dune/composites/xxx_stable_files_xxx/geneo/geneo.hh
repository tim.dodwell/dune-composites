#ifndef DUNE_GENEO_HH
#define DUNE_GENEO_HH


#include "two_level_schwarz.hh"

#include "subdomainprojectedcoarsespace.hh"

#include "partitionofunity.hh"
#include "localoperator_ovlp_region.hh"

#include "geneobasis.hh"
#include "liptonbabuskabasis.hh"
#include "partitionofunitybasis.hh"
#include "zembasis.hh"


#endif // DUNE_GENEO_HH
