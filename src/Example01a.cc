#include <dune/composites/composites.hh>
////#include <dune/composites/Tim/Geometry/GridTransformation_cube.hh>
//#include <dune/composites/Tim/PostProcessing/camanhoCriterion.hh>
//#include <dune/composites/Tim/Setup/initialiseSolveYaspGrid.hh>


#include "Example01/Example01a.hh"

//===============================================================
// Main program with grid initialiseSolve
//===============================================================

int main(int argc, char** argv){

	const int DIM = 3;

	// Maybe initialize Mpi
    
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);

    // Read ini file
    Dune::ParameterTreeParser parser;
    parser.readINITree("config_Example01a.ini",config);
    //read in further options from command line
    parser.readOptions(argc,argv,config);


    std::cout << "Hello World!" << std::endl;

    typedef Example01a<3> MODEL;

    MODEL myModel(helper);

    myModel.apply();




    //initialiseGridView<MODEL>(helper,myModel);

    std::cout << "Solution of my Problem = " << myModel.Q << std::endl;




}
