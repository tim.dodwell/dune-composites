add_executable(Example01a Example01a.cc)
target_link_dune_default_libraries(Example01a)
dune_symlink_to_source_files(FILES config)
dune_symlink_to_source_files(FILES geneo/config.ini)
dune_symlink_to_source_files(FILES stackingSequences)
dune_symlink_to_source_files(FILES grids)
dune_symlink_to_source_files(FILES RandomFieldCoefficient)
