

#include<dune/composites/Setup/baseStructuredGridModel.hh>
#include <dune/composites/Driver/linearStaticDriver.hh>

template<int DIM>
class Example01a : public Dune::Composites::baseStructuredGridModel{

    public:

        double Q;

        int overlap;
        bool verbosity;

        Dune::Composites::linearStaticDriver<Example01a<DIM>> myDriver;

        Example01a(Dune::MPIHelper& helper_) : baseStructuredGridModel(helper_), myDriver(helper_){
        	
            overlap = 2;
            
            verbosity = true;

            //
            /*solverParameters.solver = "CG";
            solverParameters.preconditioner = "GenEO";
            solverParameters.subdomainSolver = "UMFPack";
            solverParameters.overlap = overlap;
            solverParameters.coarseSpaceActive = true;
            solverParameters.nev = 10;
            solverParameters.nev_arpack = 10; 
            solverParameters.verb = 10;
            solverParameters.eigenvalue_threshold = 0.2;
            solverParameters.widlund_part_unity = true;
            solverParameters.eigen_shift = 0.001;*/

            
            solverParameters.solver = "CG";
            solverParameters.preconditioner = "OneLevel_AdditiveSchwarz";
            solverParameters.subdomainSolver = "UMFPack";


        };

    void inline apply(){    myDriver.apply(*this);  }


    bool inline isDirichlet(Dune::FieldVector<double,3> & x, int dof){
        // Default isDirichlet function, returning homogeneous Dirichlet boundary conditioners
        bool answer = false;
        if (x[0] < 1e-6){   answer = true;  }
        return answer;
    }

    inline void evaluateWeight(Dune::FieldVector<double,3>& f, int id) const{
        f = 0.0;
    }

    inline void evaluateNeumann(const Dune::FieldVector<double,3> &x,Dune::FieldVector<double,3>& h,const  Dune::FieldVector<double,3>& normal) const{
        h = 0.0; // initialise to zero
        double T = 2.98; // Thickness
        double q = 1e-2; // 50 Atmos spheres in MPa;
        if (x[2] > T - 1e-6){ // If element on top face of laminate, apply neumann boundary condition
            h[2] += q;
        }
    }

    //! \fn A member taking which constructs the base layered laminate in untransformed coordinates
    void inline LayerCake(){
        std::string example01a_Geometry = "stackingSequences/example1.csv";
        LayerCakeFromFile(example01a_Geometry);
        GeometryBuilder();
    }

    template<class GO, class U, class GFS, class C,class MBE, class GV>
    void inline postprocess(const GO& go, U& u, const GFS& gfs, const C& cg, const GV gv, MBE& mbe){
        using Dune::PDELab::Backend::native;
        double local_u3_max = 0.0;
        for (int i = 0; i < native(u).size(); i++){ // Loop over each vertex
            double u3 = std::abs(native(u)[i][2]);
            if (local_u3_max < u3){ local_u3_max = u3; }
        }
        MPI_Allreduce(&local_u3_max, &Q, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    }

    private:


    
};